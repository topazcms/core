<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => config('topaz.admin_root'), 'as' => 'admin.'], function() {

    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function() {
        \Topaz\Core\Controllers\AdminAuthController::routes();
    });

    Route::group(['middleware' => 'logged:AdminUser', 'as' => ''], function() {
        \Topaz\Core\Controllers\AdminController::routes();

        \Topaz\Core\Controllers\PagesController::routes();

        Route::group(['prefix' => 'medias', 'as' => 'medias.'], function() {
            \Topaz\Core\Controllers\MediasController::routes();
        });

        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function() {
            \Topaz\Core\Controllers\SettingsController::routes();
        });

        \Topaz\Core\Controllers\SitesController::routes();

        \Topaz\Core\Controllers\UsersController::routes();
    });

});

Route::group(['prefix' => 'medias', 'as' => 'medias.'], function() {
    \Topaz\Core\Controllers\GlobalMediaController::routes();
});

get('sitemap.xml', 'Topaz\Core\Controllers\EntrypointController@sitemap');