@if (Session::has('success'))
    <div class="alert alert-border-left alert-success dark">
        <i class="fa fa-check-circle"></i> {!! Session::get('success') !!}
    </div>
@endif
@if (Session::has('error'))
    <div class="alert alert-border-left alert-danger">
        <i class="fa fa-times-circle"></i> {!!Session::get('error') !!}
    </div>
@endif
@if (Session::has('warning'))
    <div class="alert alert-border-left alert-warning dark">
        <i class="fa fa-exclamation-triangle"></i> {!!Session::get('warning') !!}
    </div>
@endif
@if (Session::has('info'))
    <div class="alert alert-border-left alert-primary pastel">
        <i class="fa fa-info-circle"></i> {!!Session::get('info') !!}
    </div>
@endif

@section('flashbag_errors')
        <div class="alert alert-border-left alert-danger">
            <i class="fa fa-exclamation-triangle"></i>
            <b class="alert-title">Veuillez résoudre les problèmes suivants pour pouvoir poursuivre : </b>
            @foreach ($errors->all() as $error)
                <p class="ml10"><i class="fa fa-caret-right"></i> {!!$error !!}</p>
            @endforeach
        </div>
@endsection

@if ($errors->has())
    @yield('flashbag_errors')
@endif