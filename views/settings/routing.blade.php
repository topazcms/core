@extends('topaz::app')

@section('title', 'Routeur')
@section('page_title', 'Routeur')

@section('breadcrumb_tail', 'Routeur')

@inject('topaz', 'topaz')

@section('body')
    <div class="panel panel-warning panel-border top">
        <div class="panel-menu">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a class="btn btn-primary text-bold openModal" href="#">
                            <i class="fa fa-plus"></i> Nouvelle Route
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">

        <div class="table-responsive">
            <table class="table table-hovered">
                <thead>
                    <tr>
                        <th>Route</th>
                        <th>Type</th>
                        <th>Paramètres</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($routes as $route)
                        <?php $type = $topaz->getRouteType($route->type); ?>
                        <tr>
                            <td>
                                <span class="text-{{ $route->active ? 'success' : 'danger' }}">
                                    <i class="fa fa-circle"></i>
                                </span>
                                <code>
                                    @if ($route->has_parameters)
                                        <span class="text-primary">
                                            {!! $route->stylized_route !!}
                                        </span>
                                    @else
                                        {{ $route->route }}
                                    @endif
                                </code>
                                @if ($route->route == '/')
                                    <span class="label label-primary mr5">
                                        <span class="glyphicon glyphicon-home"></span> Racine
                                    </span>
                                @endif
                                @if ($route->password !== null)
                                    <span class="label label-warning mr5" rel="tooltip" data-placement="right" title="Page protégée par un mot de passe">
                                        <span class="fa fa-lock"></span>
                                    </span>
                                @endif
                                @if (in_array($route->route, $duplicates))
                                    <span class="label label-danger mr5">
                                        <span class="fa fa-exclamation-circle"></span> Doublon
                                    </span>
                                @endif
                            </td>
                            <td>
                                <i class="fa fa-{{ $type['icon'] }}"></i> {{ $type['title'] }}
                            </td>
                            <td>
                                @if ($type['name'] == 'resource')
                                    <i class="fa fa-{{ $route->resource->getResourceIcon() }}"></i> {{ $route->resource->getResourceTitle() }}
                                    /
                                    <a href="{{ $route->resource->editRoute }}" class="text-bold"><span class="text-primary">{{ $route->resource }}</span></a>
                                @else
                                    {{ $route->parameter }}
                                @endif
                            </td>
                            <td style="width: 230px;" class="text-right">
                                <div class="">
                                    <a href="{{ route('admin.settings.router.active', $route) }}" class="btn btn-{{ $route->active ? 'danger' : 'success' }} dark btn-sm" rel="tooltip" data-placement="left" title="{{ $route->active ? 'Rendre inactif' : 'Rendre actif' }}">
                                        <i class="fa fa-{{ $route->active ? 'ban' : 'check-circle' }}"></i>
                                    </a>
                                    <a class="btn btn-primary dark btn-sm openModal" data-id="{{ $route->id }}">
                                        <i class="fa fa-pencil"></i> Modifier
                                    </a>
                                    <a class="btn btn-default dark btn-sm" href="{{ $route->url }}" target="_blank">
                                        <i class="fa fa-external-link"></i> Ouvrir
                                    </a>
                                </div>
                            </td>

                        </tr>
                    @empty
                        <tr>
                            <td colspan="10" class=" text-center">
                                <div class="lead text-muted mt15">
                                    Aucune route
                                </div>
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="text-center">
            {!! $routes->render() !!}
        </div>

        </div>
    </div>

    <!-- Admin Form Popup -->
            <div id="modal" class="popup-basic horizontal-form mfp-with-anim mfp-hide">
              <div class="panel">
                <div class="panel-heading pb0">
                  <span class="panel-title">

                  </span>
                </div>
                <!-- end .panel-heading section -->

                <form method="post" action="/" id="modal-form">
                <input type="hidden" name="obj_id" value="new" />
                  <div class="panel-body p25">

                    <div class="form-group">
                        <label for="route"><i class="fa fa-code-fork ml5 mr5"></i> Route</label>
                        <div class="bs-component">
                            <input id="route" name="route" placeholder="Route" class="form-control" type="text" value="{{ Session::get('_old_input.route', '') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="type"><i class="fa fa-cube ml5 mr5"></i> Type de destination</label>
                        <div class="bs-component">
                            {!! Form::select('type',
                                $topaz->getRoutesTypesForList(),
                                '',
                                ['class' => 'select2-types form-control', 'style' => 'width: 100%']
                            ) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="type"><i class="fa fa-cog ml5 mr5"></i> Paramètre de la destination</label>
                        <div class="bs-component">
                            <div id="form_param_resource">
                                {!! Form::select('resource',
                                    $topaz->getResourcesTypesForList(),
                                    '',
                                    ['class' => 'select2-types form-control', 'style' => 'width: 100%']
                                ) !!}
                            </div>
                            <div id="form_param_input">
                                <input id="parameter" name="parameter" placeholder="Paramètre de la destination" class="form-control" type="text" value="{{ Session::get('_old_input.parameter', '') }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password"><i class="fa fa-lock ml5 mr5"></i> Protection par mot de passe</label>
                        <div class="bs-component">
                            <input id="password" name="password" placeholder="Laisser vide pour ne pas restreindre" class="form-control" type="password" value="{{ old('route') }}">
                        </div>
                    </div>

                  </div>
                  <!-- end .form-body section -->

                  <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">

                            </button>
                        </div>
                        <div class="col-md-8 text-right">
                            <div id="delete_step1">
                                <button type="button" class="btn btn-danger btn-rounded btn-sm mt5 toggle_delete">
                                    <i class="fa fa-trash-o"></i> Supprimer
                                </button>
                            </div>
                            <div id="delete_step2" style="display: none">
                                <button type="button" class="btn btn-danger btn-rounded btn-sm mt5" id="delete_obj">
                                    <b><i class="fa fa-trash-o"></i> Confirmer</b>
                                </button>
                                <button type="button" class="btn btn-default btn-rounded btn-sm mt5 toggle_delete">
                                    <i class="fa fa-undo"></i> Annuler
                                </button>
                            </div>
                        </div>
                    </div>
                  </div>
                  <!-- end .form-footer section -->
                </form>
              </div>
              <!-- end: .panel -->
            </div>
            <!-- end: .admin-form -->
@stop

@section('stylesheets')
    @include('topaz::form_css')
    <link rel="stylesheet" href="{{ asset('topaz/admin_assets/vendor/plugins/magnific/magnific-popup.css') }}"/>
@endsection

@section('javascripts')
    @include('topaz::form_js')
    <script src="{{ asset('topaz/admin_assets/vendor/plugins/magnific/jquery.magnific-popup.js') }}"></script>
    <script>

        $(document).ready(function() {

            function formatState(state) {
                if (state.element && state.element.localName == "optgroup") {
                    var parts = state.text.split("|");
                    return $("<b>").html("<i class='fa fa-" + parts[0] + "'></i> " + parts[1]);
                }
                if (!state.id) return state.text;
                var parts = state.text.split("|");
                var $state = $("<span><i class='fa fa-" + parts[0] + "'></i> " + parts[1] + "</span>");
                return $state;
            }

            $(".select2-types").select2({
                templateResult: formatState,
                templateSelection: formatState
            });

            function updateModalStates()
            {
                if ($('select[name=type]').val() == 'resource') {
                    $('#form_param_resource').show();
                    $('#form_param_input').hide();
                } else {
                    $('#form_param_resource').hide();
                    $('#form_param_input').show();
                }
            }

            $('select[name=type]').on('change', updateModalStates);

            $('#modal-form').submit(function(e) {
                var form = $(this);
                var params = {
                    obj_id: $('input[name=obj_id]').val(),
                    route: $('input[name=route]').val(),
                    type: $('select[name=type]').val(),
                    resource: $('select[name=resource]').val(),
                    parameter: $('input[name=parameter]').val(),
                    password: $('input[name=password]').val()
                };
                var submitBtn = form.find('button[type=submit]');
                submitBtn.addClass('disabled').html("<i class='fa fa-spinner fa-pulse'></i> Chargement...");

                $.ajax({
                    type: 'POST',
                    url: '{{ route('admin.settings.router.form') }}',
                    dataType: 'json',
                    data: params
                })
                .success(function(data) {
                      if (data.success) {
                          document.location.reload();
                      } else {
                          alert(data.errors);
                      }
                  })
                  .error(function(xhr, ajaxOptions, thrownError) {
//                    document.write(xhr.responseText);
                  });

                e.preventDefault();
                return false;
            });

            $('.toggle_delete').on('click', function() {
                $('#delete_step1').toggle();
                $('#delete_step2').toggle();
            });

            $('#delete_obj').on('click', function() {
                $(this).addClass('disabled').html("<i class='fa fa-spinner fa-pulse'></i> Chargement...");
                $.post('{{ route('admin.settings.router.delete') }}', {id: $(this).attr('data-id')}, function(data) {
                    location.reload();
                });
            });

            var openModal = $('.openModal');
            openModal.attr('data-effect', "mfp-zoomOut");

            openModal.on('click', function() {
                var btn = $(this);
                var modal = $('#modal');

                 if (!btn.is('[data-id]')) {
                    modal.find('.panel-title').html('<i class="fa fa-plus"></i> Nouvelle Route');
                    modal.find('button[type=submit]').html('<i class="fa fa-plus"></i> Créer');
                    $('input[name=obj_id]').val('new');

                    $('input[name=route]').val('');
                    $('select[name=type]').val('resource').trigger('change');
                    $('input[name=parameter]').val('');
                    $('input[name=password]').val('');
                    $('#delete_step1').hide();
                 } else {
                    modal.find('.panel-title').html('<i class="fa fa-pencil"></i> Modifier une Route');
                    modal.find('button[type=submit]').html('<i class="fa fa-save"></i> Enregistrer');
                    $('input[name=obj_id]').val(btn.attr('data-id'));
                    $.post('{{ route('admin.settings.router.route') }}', {id: btn.attr('data-id')}, function(data) {
                        $('input[name=route]').val(data.route);
                        $('select[name=type]').val(data.type).trigger('change');
                        $('select[name=resource]').val(data.resource_type + '|' + data.resource_id).trigger('change');
                        $('input[name=parameter]').val(data.parameter);
                        $('#delete_step1').show().attr('data-id', btn.attr('data-id'));
                        $('button#delete_obj').attr('data-id', btn.attr('data-id'));
                    }, 'json');

                 }

                 updateModalStates();

                 $.magnificPopup.open({
                    removalDelay: 500, //delay removal by X to allow out-animation,
                    items: {
                        src: '#modal'
                    },
                    // overflowY: 'hidden', //
                    callbacks: {
                        beforeOpen: function(e) {
                            var Animation = btn.attr('data-effect');
                            this.st.mainClass = Animation;
                        }
                    },
                    midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
                });
            });

        });
    </script>
@endsection