<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topaz_sites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('prefix');
            $table->string('theme')->nullable();
            $table->boolean('default')->default(false);
            $table->timestamps();
        });

        Schema::table('topaz_settings', function (Blueprint $table) {
            $table->integer('site_id')->index()->default(1);
        });

        Schema::table('topaz_pages', function (Blueprint $table) {
            $table->integer('site_id')->index()->default(1);
        });

        Schema::table('topaz_router', function (Blueprint $table) {
            $table->integer('site_id')->index()->default(1);
        });

        Schema::table('topaz_menus', function (Blueprint $table) {
            $table->integer('site_id')->index()->default(1);
        });

        Schema::table('topaz_medias', function (Blueprint $table) {
            $table->integer('site_id')->index()->default(1);
        });

        Schema::table('topaz_users', function (Blueprint $table) {
            $table->integer('site_id')->index()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('topaz_sites');

        Schema::table('topaz_settings', function (Blueprint $table) {
            $table->dropColumn('site_id');
        });

        Schema::table('topaz_pages', function (Blueprint $table) {
            $table->dropColumn('site_id');
        });

        Schema::table('topaz_router', function (Blueprint $table) {
            $table->dropColumn('site_id');
        });

        Schema::table('topaz_menus', function (Blueprint $table) {
            $table->dropColumn('site_id');
        });

        Schema::table('topaz_medias', function (Blueprint $table) {
            $table->dropColumn('site_id');
        });

        Schema::table('topaz_users', function (Blueprint $table) {
            $table->dropColumn('site_id');
        });
    }
}
