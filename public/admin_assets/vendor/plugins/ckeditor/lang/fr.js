﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * @fileOverview Defines the {@link CKEDITOR.lang} object, for the
 * French language.
 */

/**#@+
   @type String
   @example
*/

/**
 * Contains the dictionary of language entries.
 * @namespace
 */
CKEDITOR.lang[ 'fr' ] = {
	// ARIA description.
	editor: 'Éditeur de Texte Enrichi',
	editorPanel: 'Tableau de bord de l\'éditeur de texte enrichi',

	// Common messages and labels.
	common: {
		// Screenreader titles. Please note that screenreaders are not always capable
		// of reading non-English words. So be careful while translating it.
		editorHelp: 'Appuyez sur ALT-0 pour l\'aide',

		browseServer: 'Explorer le serveur',
		url: 'URL',
		protocol: 'Protocole',
		upload: 'Envoyer',
		uploadSubmit: 'Envoyer sur le serveur',
		image: 'Image',
		flash: 'Flash',
		form: 'Formulaire',
		checkbox: 'Case à cocher',
		radio: 'Bouton Radio',
		textField: 'Champ texte',
		textarea: 'Zone de texte',
		hiddenField: 'Champ caché',
		button: 'Bouton',
		select: 'Liste déroulante',
		imageButton: 'Bouton image',
		notSet: '<non défini>',
		id: 'Id',
		name: 'Nom',
		langDir: 'Sens d\'écriture',
		langDirLtr: 'Gauche à droite (LTR)',
		langDirRtl: 'Droite à gauche (RTL)',
		langCode: 'Code de langue',
		longDescr: 'URL de description longue (longdesc => malvoyant)',
		cssClass: 'Classe CSS',
		advisoryTitle: 'Description (title)',
		cssStyle: 'Style',
		ok: 'OK',
		cancel: 'Annuler',
		close: 'Fermer',
		preview: 'Aperçu',
		resize: 'Déplacer pour modifier la taille',
		generalTab: 'Général',
		advancedTab: 'Avancé',
		validateNumberFailed: 'Cette valeur n\'est pas un nombre.',
		confirmNewPage: 'Les changements non sauvegardés seront perdus. Êtes-vous sûr de vouloir charger une nouvelle page?',
		confirmCancel: 'Certaines options ont été modifiées. Êtes-vous sûr de vouloir fermer?',
		options: 'Options',
		target: 'Cible (Target)',
		targetNew: 'Nouvelle fenêtre (_blank)',
		targetTop: 'Fenêtre supérieure (_top)',
		targetSelf: 'Même fenêtre (_self)',
		targetParent: 'Fenêtre parent (_parent)',
		langDirLTR: 'Gauche à Droite (LTR)',
		langDirRTL: 'Droite à Gauche (RTL)',
		styles: 'Style',
		cssClasses: 'Classes de style',
		width: 'Largeur',
		height: 'Hauteur',
		align: 'Alignement',
		alignLeft: 'Gauche',
		alignRight: 'Droite',
		alignCenter: 'Centré',
		alignJustify: 'Justifier',
		alignTop: 'Haut',
		alignMiddle: 'Milieu',
		alignBottom: 'Bas',
		alignNone: 'Aucun',
		invalidValue	: 'Valeur incorrecte.',
		invalidHeight: 'La hauteur doit être un nombre.',
		invalidWidth: 'La largeur doit être un nombre.',
		invalidCssLength: 'La valeur spécifiée pour le champ "%1" doit être un nombre positif avec ou sans unité de mesure CSS valide (px, %, in, cm, mm, em, ex, pt, ou pc).',
		invalidHtmlLength: 'La valeur spécifiée pour le champ "%1" doit être un nombre positif avec ou sans unité de mesure HTML valide (px ou %).',
		invalidInlineStyle: 'La valeur spécifiée pour le style inline doit être composée d\'un ou plusieurs couples de valeur au format "nom : valeur", separés par des points-virgules.',
		cssLengthTooltip: 'Entrer un nombre pour une valeur en pixels ou un nombre avec une unité de mesure CSS valide (px, %, in, cm, mm, em, ex, pt, ou pc).',


		// Put the voice-only part of the label in the span.
		unavailable: '%1<span class="cke_accessibility">, Indisponible</span>'
	},
		"about": {
			"copy": "Copyright &copy; $1. All rights reserved.",
					"dlgTitle": "About CKEditor",
					"help": "Check $1 for help.",
					"moreInfo": "For licensing information please visit our web site:",
					"title": "About CKEditor",
					"userGuide": "CKEditor User's Guide"
		},
		"basicstyles": {
			"bold": "Bold",
					"italic": "Italic",
					"strike": "Strikethrough",
					"subscript": "Subscript",
					"superscript": "Superscript",
					"underline": "Underline"
		},
		"blockquote": {
			"toolbar": "Block Quote"
		},
		"clipboard": {
			"copy": "Copy",
					"copyError": "Your browser security settings don't permit the editor to automatically execute copying operations. Please use the keyboard for that (Ctrl/Cmd+C).",
					"cut": "Cut",
					"cutError": "Your browser security settings don't permit the editor to automatically execute cutting operations. Please use the keyboard for that (Ctrl/Cmd+X).",
					"paste": "Paste",
					"pasteArea": "Paste Area",
					"pasteMsg": "Please paste inside the following box using the keyboard (<strong>Ctrl/Cmd+V</strong>) and hit OK",
					"securityMsg": "Because of your browser security settings, the editor is not able to access your clipboard data directly. You are required to paste it again in this window.",
					"title": "Paste"
		},
		"contextmenu": {
			"options": "Context Menu Options"
		},
		"button": {
			"selectedLabel": "%1 (Selected)"
		},
		"toolbar": {
			"toolbarCollapse": "Collapse Toolbar",
					"toolbarExpand": "Expand Toolbar",
					"toolbarGroups": {
				"document": "Document",
						"clipboard": "Clipboard/Undo",
						"editing": "Editing",
						"forms": "Forms",
						"basicstyles": "Basic Styles",
						"paragraph": "Paragraph",
						"links": "Links",
						"insert": "Insert",
						"styles": "Styles",
						"colors": "Colors",
						"tools": "Tools"
			},
			"toolbars": "Editor toolbars"
		},
		"elementspath": {
			"eleLabel": "Elements path",
					"eleTitle": "%1 element"
		},
		"format": {
			"label": "Format",
					"panelTitle": "Paragraph Format",
					"tag_address": "Address",
					"tag_div": "Normal (DIV)",
					"tag_h1": "Heading 1",
					"tag_h2": "Heading 2",
					"tag_h3": "Heading 3",
					"tag_h4": "Heading 4",
					"tag_h5": "Heading 5",
					"tag_h6": "Heading 6",
					"tag_p": "Normal",
					"tag_pre": "Formatted"
		},
		"horizontalrule": {
			"toolbar": "Insert Horizontal Line"
		},
		"image": {
			"alertUrl": "Please type the image URL",
					"alt": "Alternative Text",
					"border": "Border",
					"btnUpload": "Send it to the Server",
					"button2Img": "Do you want to transform the selected image button on a simple image?",
					"hSpace": "HSpace",
					"img2Button": "Do you want to transform the selected image on a image button?",
					"infoTab": "Image Info",
					"linkTab": "Link",
					"lockRatio": "Lock Ratio",
					"menu": "Image Properties",
					"resetSize": "Reset Size",
					"title": "Image Properties",
					"titleButton": "Image Button Properties",
					"upload": "Upload",
					"urlMissing": "Image source URL is missing.",
					"vSpace": "VSpace",
					"validateBorder": "Border must be a whole number.",
					"validateHSpace": "HSpace must be a whole number.",
					"validateVSpace": "VSpace must be a whole number."
		},
		"indent": {
			"indent": "Increase Indent",
					"outdent": "Decrease Indent"
		},
		"fakeobjects": {
			"anchor": "Anchor",
					"flash": "Flash Animation",
					"hiddenfield": "Hidden Field",
					"iframe": "IFrame",
					"unknown": "Unknown Object"
		},
		"link": {
			"acccessKey": "Access Key",
					"advanced": "Advanced",
					"advisoryContentType": "Advisory Content Type",
					"advisoryTitle": "Advisory Title",
					"anchor": {
				"toolbar": "Anchor",
						"menu": "Edit Anchor",
						"title": "Anchor Properties",
						"name": "Anchor Name",
						"errorName": "Please type the anchor name",
						"remove": "Remove Anchor"
			},
			"anchorId": "By Element Id",
					"anchorName": "By Anchor Name",
					"charset": "Linked Resource Charset",
					"cssClasses": "Stylesheet Classes",
					"emailAddress": "E-Mail Address",
					"emailBody": "Message Body",
					"emailSubject": "Message Subject",
					"id": "Id",
					"info": "Link Info",
					"langCode": "Language Code",
					"langDir": "Language Direction",
					"langDirLTR": "Left to Right (LTR)",
					"langDirRTL": "Right to Left (RTL)",
					"menu": "Edit Link",
					"name": "Name",
					"noAnchors": "(No anchors available in the document)",
					"noEmail": "Please type the e-mail address",
					"noUrl": "Please type the link URL",
					"other": "<other>",
					"popupDependent": "Dependent (Netscape)",
					"popupFeatures": "Popup Window Features",
					"popupFullScreen": "Full Screen (IE)",
					"popupLeft": "Left Position",
					"popupLocationBar": "Location Bar",
					"popupMenuBar": "Menu Bar",
					"popupResizable": "Resizable",
					"popupScrollBars": "Scroll Bars",
					"popupStatusBar": "Status Bar",
					"popupToolbar": "Toolbar",
					"popupTop": "Top Position",
					"rel": "Relationship",
					"selectAnchor": "Select an Anchor",
					"styles": "Style",
					"tabIndex": "Tab Index",
					"target": "Target",
					"targetFrame": "<frame>",
					"targetFrameName": "Target Frame Name",
					"targetPopup": "<popup window>",
					"targetPopupName": "Popup Window Name",
					"title": "Link",
					"toAnchor": "Link to anchor in the text",
					"toEmail": "E-mail",
					"toUrl": "URL",
					"toolbar": "Link",
					"type": "Link Type",
					"unlink": "Unlink",
					"upload": "Upload"
		},
		"list": {
			"bulletedlist": "Insert/Remove Bulleted List",
					"numberedlist": "Insert/Remove Numbered List"
		},
		"magicline": {
			"title": "Insert paragraph here"
		},
		"maximize": {
			"maximize": "Maximize",
					"minimize": "Minimize"
		},
		"pastetext": {
			"button": "Paste as plain text",
					"title": "Paste as Plain Text"
		},
		"pastefromword": {
			"confirmCleanup": "The text you want to paste seems to be copied from Word. Do you want to clean it before pasting?",
					"error": "It was not possible to clean up the pasted data due to an internal error",
					"title": "Paste from Word",
					"toolbar": "Paste from Word"
		},
		"removeformat": {
			"toolbar": "Remove Format"
		},
		"sourcearea": {
			"toolbar": "Source"
		},
		"specialchar": {
			"options": "Special Character Options",
					"title": "Select Special Character",
					"toolbar": "Insert Special Character"
		},
		"scayt": {
			"btn_about": "About SCAYT",
					"btn_dictionaries": "Dictionaries",
					"btn_disable": "Disable SCAYT",
					"btn_enable": "Enable SCAYT",
					"btn_langs": "Languages",
					"btn_options": "Options",
					"text_title": "Spell Check As You Type"
		},
		"stylescombo": {
			"label": "Styles",
					"panelTitle": "Formatting Styles",
					"panelTitle1": "Block Styles",
					"panelTitle2": "Inline Styles",
					"panelTitle3": "Object Styles"
		},
		"table": {
			"border": "Border size",
					"caption": "Caption",
					"cell": {
				"menu": "Cell",
						"insertBefore": "Insert Cell Before",
						"insertAfter": "Insert Cell After",
						"deleteCell": "Delete Cells",
						"merge": "Merge Cells",
						"mergeRight": "Merge Right",
						"mergeDown": "Merge Down",
						"splitHorizontal": "Split Cell Horizontally",
						"splitVertical": "Split Cell Vertically",
						"title": "Cell Properties",
						"cellType": "Cell Type",
						"rowSpan": "Rows Span",
						"colSpan": "Columns Span",
						"wordWrap": "Word Wrap",
						"hAlign": "Horizontal Alignment",
						"vAlign": "Vertical Alignment",
						"alignBaseline": "Baseline",
						"bgColor": "Background Color",
						"borderColor": "Border Color",
						"data": "Data",
						"header": "Header",
						"yes": "Yes",
						"no": "No",
						"invalidWidth": "Cell width must be a number.",
						"invalidHeight": "Cell height must be a number.",
						"invalidRowSpan": "Rows span must be a whole number.",
						"invalidColSpan": "Columns span must be a whole number.",
						"chooseColor": "Choose"
			},
			"cellPad": "Cell padding",
					"cellSpace": "Cell spacing",
					"column": {
				"menu": "Column",
						"insertBefore": "Insert Column Before",
						"insertAfter": "Insert Column After",
						"deleteColumn": "Delete Columns"
			},
			"columns": "Columns",
					"deleteTable": "Delete Table",
					"headers": "Headers",
					"headersBoth": "Both",
					"headersColumn": "First column",
					"headersNone": "None",
					"headersRow": "First Row",
					"invalidBorder": "Border size must be a number.",
					"invalidCellPadding": "Cell padding must be a positive number.",
					"invalidCellSpacing": "Cell spacing must be a positive number.",
					"invalidCols": "Number of columns must be a number greater than 0.",
					"invalidHeight": "Table height must be a number.",
					"invalidRows": "Number of rows must be a number greater than 0.",
					"invalidWidth": "Table width must be a number.",
					"menu": "Table Properties",
					"row": {
				"menu": "Row",
						"insertBefore": "Insert Row Before",
						"insertAfter": "Insert Row After",
						"deleteRow": "Delete Rows"
			},
			"rows": "Rows",
					"summary": "Summary",
					"title": "Table Properties",
					"toolbar": "Table",
					"widthPc": "percent",
					"widthPx": "pixels",
					"widthUnit": "width unit"
		},
		"undo": {
			"redo": "Redo",
					"undo": "Undo"
		},
		"wsc": {
			"btnIgnore": "Ignore",
					"btnIgnoreAll": "Ignore All",
					"btnReplace": "Replace",
					"btnReplaceAll": "Replace All",
					"btnUndo": "Undo",
					"changeTo": "Change to",
					"errorLoading": "Error loading application service host: %s.",
					"ieSpellDownload": "Spell checker not installed. Do you want to download it now?",
					"manyChanges": "Spell check complete: %1 words changed",
					"noChanges": "Spell check complete: No words changed",
					"noMispell": "Spell check complete: No misspellings found",
					"noSuggestions": "- No suggestions -",
					"notAvailable": "Sorry, but service is unavailable now.",
					"notInDic": "Not in dictionary",
					"oneChange": "Spell check complete: One word changed",
					"progress": "Spell check in progress...",
					"title": "Spell Checker",
					"toolbar": "Check Spelling"
		}
};
