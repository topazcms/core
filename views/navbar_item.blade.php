<?php if (!isset($params)) $params = []; ?>
@if (isset($items) && is_array($items))
    <?php
    $is_active = false;
    foreach ($items as $item) {
        if (Request::is(config('topaz.admin_root') . '/' . $item['active'])) {
            $is_active = true;
            break;
        }
    } ?>
    <li {{ $is_active ? 'class=active' : '' }}>
        <a class="accordion-toggle {{ $is_active ? 'menu-open' : '' }}" href="{{ route($route, $params) }}">
            <span class="{{ $icontype or 'fa' }} {{ $icontype or 'fa' }}-{{ $icon }}"></span>
            <span class="sidebar-title">{{ $title }}</span>
            <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            @foreach ($items as $item)
                <?php if (!isset($item['params'])) $item['params'] = []; ?>
                <li {{ Request::is(config('topaz.admin_root') . '/' . $item['active']) ? "class=active" : '' }}>
                    <a href="{{ route($item['route'], $item['params']) }}">
                        <span class="{{ $item['icontype'] or 'fa' }} {{ $item['icontype'] or 'fa' }}-{{ $item['icon'] }}"></span>
                        {{ $item['title'] }}
                    </a>
                </li>
            @endforeach
        </ul>
    </li>
@else
    <li {{ Request::is(config('topaz.admin_root') . '/' . $active) ? "class=active" : '' }}>
        <a href="{{ route($route, $params) }}">
            <span class="{{ $icontype or 'fa' }} {{ $icontype or 'fa' }}-{{ $icon }}"></span>
            @if (!empty($title))
                <span class="sidebar-title">{{ $title }}</span>
            @endif
        </a>
    </li>
@endif