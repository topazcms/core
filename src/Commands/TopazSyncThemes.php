<?php

namespace Topaz\Core\Commands;

use Artesaos\Defender\Defender;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Topaz\Core\Models\User;
use Topaz\Core\Services\SitesManager;
use Topaz\Core\TopazServiceProvider;

class TopazSyncThemes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'topaz:themes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync themes files';

    /** @var SitesManager */
    protected $sites;

    /** @var  ProgressBar */
    protected $bar;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->sites = app('topaz.sites');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $themes = config('topaz.themes');
        $this->bar = $this->output->createProgressBar(count($themes));
        $this->bar->setBarCharacter('<fg=white;bg=green> </>');
        $this->bar->setEmptyBarCharacter(' ');
        $this->bar->setProgressCharacter('<fg=white;bg=yellow> </>');
        $this->bar->setFormat(" %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%");

        $this->comment("Syncing themes files...");

        foreach ($themes as $id => $name) {
            $this->bar->advance();
            $this->bar->setMessage("Syncing $id theme...");
            $this->sites->callThemeCreationScripts($id, $name);
        }

        $this->info("");
        $this->info("Sync finished !");
    }
}
