<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>@yield('title', 'Administration') | Topaz {{ topaz_version() }}</title>
    <meta name='robots' content='noindex,nofollow' />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
    <link rel="stylesheet" type="text/css" href="{{ asset('topaz/admin_assets/admin.css') }}">
    @yield('skeleton_stylesheets')
    @yield('stylesheets')
</head>
<body class="@yield('body_class')">
    @yield('skeleton_body')
    <script src="{{ asset('topaz/admin_assets/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('topaz/admin_assets/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('topaz/admin_assets/js/utility/utility.js') }}"></script>
    <script src="{{ asset('topaz/admin_assets/js/main.js') }}"></script>
    @yield('skeleton_javascripts')
    <script type="text/javascript">
        jQuery(document).ready(function() {
            "use strict";
            Core.init();
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
            });
            $('*[rel=tooltip]').tooltip();
            @yield('js_inits')
        });
    </script>
    @yield('javascripts')
</body>
</html>