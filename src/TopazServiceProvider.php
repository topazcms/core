<?php

namespace Topaz\Core;

use Artesaos\Defender\Defender;
use Carbon\Carbon;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Topaz\Core\Commands\TopazGenerateSuperadmin;
use Topaz\Core\Commands\TopazPublish;
use Topaz\Core\Commands\TopazSetup;
use Topaz\Core\Commands\TopazSyncThemes;
use Topaz\Core\Middleware\TopazInitSites;
use Topaz\Core\Middleware\TopazLogged;
use Topaz\Core\Middleware\TopazMinify;
use Topaz\Core\Middleware\TopazPermission;
use Topaz\Core\Models\Media;
use Topaz\Core\Models\Menu;
use Topaz\Core\Models\Page;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Topaz\Core\Models\Resource;
use Topaz\Core\Models\Settings;
use Topaz\Core\Models\Site;
use Topaz\Core\Services\SitesManager;
use Topaz\Core\Services\TopazBreadcrumb;
use Topaz\Core\Services\TopazPermissions;
use Topaz\Core\Services\TopazRoutingService;
use Topaz\Core\Services\TopazService;

class TopazServiceProvider extends ServiceProvider
{
    use SEOToolsTrait;

    /**
     * @var TopazService $topaz
     */
    protected $topaz;

    /**
     * @var SitesManager
     */
    protected $sites;

    /**
     * @var TopazRoutingService
     */
    protected $routing;

    /**
     * @var TopazPermissions
     */
    protected $permissions;

    /**
     * @var TopazBreadcrumb
     */
    protected $breadcrumb;


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerService();
        $this->registerSitesManager();
        $this->registerPermissionsService();
        $this->registerBreadcrumbService();
        $this->registerRoutingService();
        $this->registerViews();
        $this->registerMiddlewares();
        $this->registerCommands();
        $this->registerBladeDirectives();
        $this->registerRolesAndPermissions();
        $this->registerPagesModule();
        $this->registerMediasModule();
        $this->registerSettings();
        $this->registerSeo();
        $this->registerAdmin();
        $this->registerHtmlFilters();

        $this->publishAssets();
        $this->publishConfigs();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRoutes();
    }

    protected function registerService()
    {
        $this->app->singleton(TopazService::class, function ($app) {
            return new TopazService();
        });

        $this->app->bind('topaz', function($app) {
            return $app[TopazService::class];
        });

        $this->topaz = $this->app->make(TopazService::class);

        $this->topaz->registerRouteType(null, null, 'resource', "Ressource", 'cube');
        $this->topaz->registerRouteType(null, null, 'redirection', "Redirection", 'code-fork');
        $this->topaz->registerRouteType(null, null, 'mirror', "Miroir", 'files-o');
        $this->topaz->registerNotificationType('topaz.test', 'diamond', 'primary');

        Menu::creating(function($menu) {
            $menu->order = Menu::select('order')->orderBy('order', 'desc')->where('menu', $menu->menu)->first()->order + 1;
        });

        Carbon::setLocale('fr');

    }

    protected function registerSitesManager()
    {
        $this->app->singleton(SitesManager::class, function ($app) {
            return new SitesManager();
        });

        $this->app->bind('topaz.sites', function($app) {
            return $app[SitesManager::class];
        });

        $this->sites = $this->app->make(SitesManager::class);

        $this->sites->onDuplicate(function($new_site, $source_site) {

            foreach (Media::ofSite($source_site)->get() as $item) {
                $this->sites->duplicateEntity($new_site, $item);
            }

            foreach (Menu::ofSite($source_site)->get() as $item) {
                $this->sites->duplicateEntity($new_site, $item);
            }

            // Duplicate Settings
            $default_settings = $this->sites->getSettings($source_site->id)->getAttributes();
            $settings = Settings::whereSiteId($new_site->id)->first();
            unset($default_settings['id']);
            unset($default_settings['site_id']);

            foreach ($default_settings as $field => $value) {
                $settings->$field = $value;
            }
            $settings->save();
        });

        $this->sites->onDelete(function($site) {

            foreach (Menu::ofSite($site)->get() as $item) {
                $item->delete();
            }

            foreach (Media::ofSite($site)->get() as $item) {
                $item->delete();
            }

            Settings::whereSiteId($site->id)->delete();
        });
    }


    protected function registerRoutingService()
    {
        $this->app->singleton(TopazRoutingService::class, function ($app) {
            return new TopazRoutingService();
        });

        $this->app->bind('topaz.routing', function($app) {
            return $app[TopazRoutingService::class];
        });

        $this->routing = $this->app->make(TopazRoutingService::class);

        $this->sites->onDuplicate(function($new_site, $source_site) {
            foreach (Resource::ofSite($source_site)->get() as $item) {
                $this->sites->duplicateEntity($new_site, $item);
            }
        });

        $this->sites->onDelete(function($site) {
            foreach (Resource::ofSite($site)->get() as $item) {
                $item->delete();
            }
        });
    }

    protected function registerPermissionsService()
    {
        $this->app->singleton(TopazPermissions::class, function() {
            return new TopazPermissions;
        });

        $this->app->bind('topaz.permissions', function($app) {
            return $app[TopazPermissions::class];
        });

        $this->permissions = $this->app->make(TopazPermissions::class);
    }

    protected function registerBreadcrumbService()
    {
        $this->app->singleton(TopazBreadcrumb::class, function() {
            return new TopazBreadcrumb;
        });

        $this->app->bind('topaz.breadcrumb', function($app) {
            return $app[TopazBreadcrumb::class];
        });

        $this->breadcrumb = $this->app->make(TopazBreadcrumb::class);
    }

    protected function registerRoutes()
    {
        if (!$this->app->routesAreCached()) {
            require __DIR__ . '/routes.php';
        }
    }

    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'topaz');

        view()->composer('topaz::*', function ($view) {
            $user =  app('auth')->admin()->get();
            $view->with('admin_user', $user);
        });
    }

    protected function registerMiddlewares()
    {
        $router = $this->app['router'];
        $router->middleware('logged', TopazLogged::class);
        $router->middleware('permission', TopazPermission::class);
    }

    protected function registerCommands()
    {
        $this->registerCommand('superadmin', TopazGenerateSuperadmin::class);
        $this->registerCommand('setup', TopazSetup::class);
        $this->registerCommand('publish', TopazPublish::class);
        $this->registerCommand('themes', TopazSyncThemes::class);
    }

    protected function registerCommand($id, $cmd)
    {
        $this->app->singleton('command.topaz.'.$id, function ($app) use ($cmd) {
            return $app[$cmd];
        });
        $this->commands('command.topaz.'.$id);
    }

    protected function publishAssets()
    {
        $this->publishes([
            __DIR__ . '/../migrations/' => base_path('/database/migrations')
        ], 'migrations');

        $this->publishes([
            __DIR__ . '/../public/' => public_path('topaz'),
        ], 'assets');
    }

    protected function publishConfigs()
    {
        $this->publishes([
            __DIR__ . '/../config/topaz.php' => config_path('topaz.php')
        ], 'config');

        $this->mergeConfigFrom( __DIR__ . '/../config/topaz.php', 'topaz');
        $this->mergeConfigFrom( __DIR__ . '/../config/topaz_languages.php', 'topaz_languages');
    }

    private function registerBladeDirectives()
    {
        Blade::directive('permission', function($perm) {
            return "<?php if (app('auth')->admin()->get()->can($perm)) : ?>";
        });
        Blade::directive('endpermission', function() {
            return "<?php endif; ?>";
        });
    }

    private function registerRolesAndPermissions()
    {
        $this->permissions->needPermission('settings.manage', "Gérer les réglages");
        $this->permissions->needPermission('settings.router', "Gérer le routage");
        $this->permissions->needPermission('settings.navigation', "Gérer la navigation");
        $this->permissions->needPermission('settings.users', "Gérer les utilisateurs");
        $this->permissions->needPermission('settings.sites', "Gérer les sites");
    }

    protected function registerPagesModule()
    {
        $this->topaz->registerAdminMenuSingleItem('file-o', 'Pages', 2, 'pages*', 'admin.pages.index');
        $this->topaz->registerResourceType(Page::class);

        $this->permissions->needRole('redactor');
        $this->permissions->needPermission('pages.manage', "Gérer les pages", ['redactor']);

        Page::creating(function($page) {
            $page->site_id = $this->sites->getCurrentSiteId();
        });

        Page::created(function($page) {
            $title = str_slug($page->title);

            $slug = config('topaz.pages.slug', '%TITLE%');
            $slug = str_replace('%TITLE%', $title, $slug);
            $page->addRoute($slug);
        });

        Page::deleting(function($page) {
            if ($page->trashed()) {
                $page->clearRoutes();
            }
        });

        $this->sites->onDuplicate(function($new_site, $source_site) {
            foreach (Page::ofSite($source_site)->get() as $page) {
                $this->sites->duplicateEntity($new_site, $page);
            }
        });

        $this->sites->onDelete(function($site) {
            foreach (Page::ofSite($site)->get() as $page) {
                $page->forceDelete();
            }
        });

        $this->sites->onThemeCreation(function($cp) {
            $layouts = config('topaz.pages.layouts');

            foreach ($layouts as $id => $name) {
                $cp(__DIR__ . '/../resources/views/pages.blade.php', $id . '.blade.php');
            }
        });
    }

    protected function registerMediasModule()
    {
        $this->topaz->registerAdminMenuSingleItem('picture-o', 'Bibliothèque', 80, 'medias*', 'admin.medias.index');
    }

    protected function registerSettings()
    {
        $this->topaz->registerAdminMenuItem([
            'icon' => 'cog',
            'title' => 'Réglages',
            'active' => 'settings*',
            'route' => 'admin.settings.edit',
            'order' => '100',
            'items' => [
                [
                    'icon' => 'wrench',
                    'title' => 'Paramètres du site',
                    'active' => 'settings/edit',
                    'route' => 'admin.settings.edit',
                    'icontype' => 'glyphicon'
                ],
                [
                    'icon' => 'random',
                    'title' => "Routeur",
                    'active' => 'settings/router',
                    'route' => 'admin.settings.routing',
                    'icontype' => 'glyphicon'
                ],
                [
                    'icon' => 'compass',
                    'title' => 'Navigation',
                    'active' => 'settings/navigation*',
                    'route' => 'admin.settings.navigation',
                ],
                [
                    'icon' => 'users',
                    'title' => 'Utilisateurs',
                    'active' => 'settings/users*',
                    'route' => 'admin.settings.users.index',
                ],
                [
                    'icon' => 'globe',
                    'title' => 'Sites',
                    'active' => 'settings/sites*',
                    'route' => 'admin.settings.sites.index',
                ],
            ]
        ]);
    }

    public function registerSeo()
    {
        $this->topaz->initSeo();
    }

    private function registerAdmin()
    {

    }

    private function registerHtmlFilters()
    {
        $this->topaz->registerHtmlFilter(function ($html) {
            return $this->topaz->parse_domain($html);
        }, function ($html) {
            return $this->topaz->unparse_domain($html);
        });
    }

}