<div class="form-group {{ $errors->has($field->name) ? 'has-error' : '' }}">
    <div class="bs-component">
        <div class="switch round switch-inline switch-primary pull-left">
            <input type="checkbox" id="{{ $field->name }}" name="{{ $field->name }}" value="1">
            <label for="{{ $field->name }}">{!! $field->label !!}</label>
        </div>
        @if ($field->label !== null)
            <label for="{{ $field->name }}" class="ml10 mt5">{!! $field->label !!}</label>
        @endif
        @if ($errors->has($field->name))
            @foreach ($errors->get($field->name) as $error)
                <span class="help-block">
                    <i class="fa fa-exclamation-circle"></i> {{ $error }}
                </span>
            @endforeach
        @endif
    </div>
</div>