<?php namespace Topaz\Core\Controllers;

use Carbon\Carbon;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Media;
use Topaz\Core\Models\Page;

class MediasController extends Controller {

    public static function routes()
    {
        Route::pattern('page_id', '[0-9]+');

        Route::get('/', ['as' => 'index', 'uses' => __CLASS__.'@index']);

        Route::any('upload', ['as' => 'upload', 'uses' => __CLASS__.'@upload']);

        Route::post('add', __CLASS__.'@create');
        Route::post('edit/{media_id}', __CLASS__.'@update');

        Route::get('/api', ['as' => 'api.list', 'uses' => __CLASS__.'@api_list']);
    }

    public function index()
    {
        $medias = Media::currentSite();

        $medias = $medias->orderBy('updated_at', 'desc');
        $medias = $medias->paginate(20);

        return view('topaz::medias.index', compact('medias'));
    }

    public function upload(Request $req)
    {
        $maxKb = config('topaz.medias.max_size', 16000);
        $this->validate($req, [
            'file' => 'max:' . $maxKb
        ]);

        $file = $req->file('file');

        $extension = $file->getClientOriginalExtension();
        $directory = storage_path(Media::$path);
        $filename = str_random(16).".{$extension}";

        $upload_success = $file->move($directory, $filename);

        $media = new Media;
        $media->name = $file->getClientOriginalName();
        $media->filename = $filename;
        $media->updateType();
        $media->save();

        if( $upload_success ) {
            return response()->json('success', 200);
        } else {
            return response()->json('error', 400);
        }
    }

    public function detail($media_id)
    {
        $media = Media::findOrFail($media_id);


    }

    public function edit(Guard $auth, $page_id)
    {
        $page = Page::findOrFail($page_id);
        return view('topaz::medias.form', compact('page'));
    }

    public function update(Request $req, $page_id)
    {
        $this->validate($req, [
            'title' => 'required',
            'author_id' => 'required|exists:topaz_users,id',
        ]);

        $page = Page::findOrFail($page_id);
        $page->fill($req->all());
        $page->save();

        $req->session()->flash('success', "La page <b>{$page}</b> a bien été modifiée");
        return redirect()->route('admin.medias.edit', $page);
    }

    public function api_list(Request $req)
    {
        $medias = Media::orderBy('updated_at', 'desc');

        if (!empty($req->get('filter', '')))
        {
            $medias = $medias->where('name', 'like', '%' . $req->get('filter') . '%');
        }

        if ($req->get('type', 'file') != 'file')
        {
            $medias = $medias->where('type', $req->get('type'));
        }

        $medias = $medias->paginate(15);

        return response()->json($medias);
    }

}
