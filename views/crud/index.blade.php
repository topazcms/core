@extends('topaz::app')

@section('title', $index_title)
@section('page_title', $index_title)

@section('breadcrumb')
    @foreach ($breadcrumb as $item)
        @include('topaz::breadcrumb_item', $item)
    @endforeach
    {{--@include('topaz::breadcrumb_item', ['title' => 'Accueil', 'url' => route('admin.index')])--}}
@endsection

@section('topbar_right')
    @if ($has_soft_delete)
        <ul class="nav nav-list nav-list-topbar pull-left mrn">
            <li class="{{ $is_trash ?: 'active' }}">
                <a href="{{ route($route_prefix . '.index') }}">{{ ucwords($object_name_plural) }} {{ $object_name_male ? 'actifs' : 'actives' }}</a>
            </li>
            <li class="{{ !$is_trash ?: 'active' }}">
                <a href="{{ route($route_prefix . '.index', ['trashed' => 'show']) }}"><i class="fa fa-trash-o"></i> Corbeille</a>
            </li>
        </ul>
    @endif
    <div class="btn-group pull-left">

    </div>
@append

@section('breadcrumb_tail', 'Pages')

@section('body')

    <div class="row mb15">
        <div class="col-md-9 mb5">
            <div class="btn-group">
                <a class="btn btn-primary btn-rounded text-bold" href="{{ route($route_prefix . '.add') }}">
                    <i class="fa fa-plus"></i>
                    @if ($new_button === null)
                        {{ $object_name_male ? 'Nouveau' : 'Nouvelle' }} {{ $object_name_singular }}
                    @else
                        {{ $new_button }}
                    @endif
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <form action="{{ URL::full() }}" method="GET">
                <div class="input-group {{ !$search_term ?: 'has-success' }}">
                    <input type="text" class="form-control" name="search" id="search" placeholder="Recherche rapide..." value="{{ $search_term }}">
                    <span class="input-group-btn">
                        @if ($search_term)
                            <a class="btn btn-success btn-rounded" href="{{ route(Route::currentRouteName(), Request::except('search')) }}"><i class="fa fa-times"></i></a>
                            <button class="btn btn-success btn-rounded" type="submit"><i class="fa fa-search"></i></button>
                        @else
                            <button class="btn btn-default btn-rounded" type="submit"><i class="fa fa-search"></i></button>
                        @endif
                    </span>
                </div>
            </form>
        </div>
    </div>

    <div class="panel panel-{{ $color or 'default' }} panel-border top">
        <div class="panel-body pn">

        <div class="table-responsive">
            <table class="table table-hovered">
                <thead>
                    <tr>
                        @foreach (array_keys($table) as $header)
                            <th>
                                @if (array_key_exists($header, $index_sorting))
                                    <a href="{{ $index_sorting_links[$header] }}">
                                        {{ $header }}
                                        @if ($index_sorting[$header] == $sortField)
                                            @if ($sortOrientation == 'desc')
                                                <i class="fa fa-caret-up"></i>
                                            @else
                                                <i class="fa fa-caret-down"></i>
                                            @endif
                                        @endif
                                    </a>
                                @else
                                    {{ $header }}
                                @endif
                            </th>
                        @endforeach
                        <th>

                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($objects as $object)
                        <tr>
                            @foreach ($table as $header => $prop)
                                <td>
                                    @if ($prop == '__toString')
                                        {!! (string)$object !!}
                                    @elseif (ends_with($prop, '()'))
                                        {!! (string)$object->$prop() !!}
                                    @else
                                        {!! (string)$object->$prop !!}
                                    @endif
                                </td>
                            @endforeach
                            <td>
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-sm  btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Actions <i class="fa fa-caret-down"></i>
                                    </button>
                                    <ul class="dropdown-menu actions">
                                        @if (!$is_trash)
                                            <li>
                                                <a href="{{ route($route_prefix . '.edit', $object) }}">
                                                    <span class="text-primary">
                                                        <i class="fa fa-edit"></i>
                                                        Modifier
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)" rel="delete" data-id="{{ $object->id }}" data-deletehref="{{ route($route_prefix . '.delete', $object) }}">
                                                    <span class="text-danger">
                                                        <i class="fa fa-trash-o"></i>
                                                        @if ($has_soft_delete)
                                                            Mettre à la corbeille
                                                        @else
                                                            Supprimer définitivement
                                                        @endif
                                                    </span>
                                                </a>
                                            </li>
                                            <?php $actions = $actions_for($object); ?>
                                            @if (!empty($actions))
                                                <li role="separator" class="divider"></li>
                                                @foreach ($actions as $action)
                                                    <li>
                                                        <a href="{{ array_key_exists('route', $action) ? route($action['route'], $object) : $action['url'] }}">
                                                            {!! $action['name'] !!}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            @endif
                                        @else
                                            <li>
                                                <a href="{{ route($route_prefix . '.restore', $object) }}">
                                                    <span class="text-success">
                                                        <i class="fa fa-reply"></i>
                                                        Restaurer
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route($route_prefix . '.delete', $object) }}">
                                                     <span class="text-danger font-bold">
                                                        <i class="fa fa-trash-o"></i>
                                                        Supprimer définitivement
                                                    </span>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="10" class=" text-center">
                                <div class="lead text-muted mt15">
                                    @if ($search_term)
                                        Aucun résultat à la recherche
                                        <div class="mt20">
                                            <a class="btn btn-default btn-sm" href="{{ route(Route::currentRouteName(), Request::except('search')) }}">
                                                <i class="fa fa-times"></i> Annuler la recherche
                                            </a>
                                        </div>
                                    @else
                                        @if (!$is_trash)
                                            {{ $object_name_male ? 'Aucun' : 'Aucune' }} {{ $object_name_singular }}
                                        @else
                                            {{ $object_name_male ? 'Aucun' : 'Aucune' }} {{ $object_name_singular }} {{ $object_name_male ? 'supprimé' : 'supprimée' }}
                                        @endif
                                    @endif
                                </div>
                                @if (!$is_trash && !$search_term)
                                    <a class="btn btn-primary btn-lg" href="{{ route($route_prefix . '.add') }}">
                                        <i class="fa fa-plus"></i> Créez votre {{ $object_name_male ? 'premier' : 'première' }} {{ $object_name_singular }}
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="text-center">
            {!! $objects->render() !!}
        </div>

        </div>
    </div>


@stop

@section('stylesheets')
@endsection

@section('javascripts')
    <script src="{{ asset('topaz/admin_assets/js/bootbox/bootbox.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('*[rel=delete]').on('click', function() {
                var item = $(this);
                var row = item.parents('tr');
                row.addClass('warning');
                bootbox.confirm({
                    message: "Voulez-vous vraiment {{ $has_soft_delete ? 'mettre à la corbeille' : 'supprimer'}} cet élément ?",
                    buttons: {
                        cancel: {
                            label: "Annuler",
                            className: 'btn-default'
                        },
                        confirm: {
                            label: "Supprimer définitivement",
                            className: 'btn-danger'
                        }
                    },
                    callback: function(choice) {
                        if (choice) {
                            window.location = item.data('deletehref');
                        } else {
                            row.removeClass('warning');
                        }
                    }
                });
            });
        });
    </script>
@endsection