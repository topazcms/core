@extends('topaz::scene')
@inject('sites', 'topaz.sites')

@section('title')
    Connexion
@stop

@section('body')
<div class="admin-form theme-info" id="login1" style="max-width: 500px">
    <div class="row table-layout">
        <div class="col-xs-6 va-m pln">
            <a href="{{ url(config('topaz.admin_root')) }}" style="color:white; font-size:1.9em; font-weight:300; text-decoration: none;">
                <i class="fa fa-diamond"></i> <span style=" font-weight: 700;">TOPAZ</span> {{ topaz_version() }}
            </a>
        </div>
        <div class="col-xs-6 text-right va-b pr5 pb5">
            <div class="login-links">
                <a href="{{ URL::full() }}" class="active">
                    Connexion
                </a>
            </div>
        </div>
    </div>

    <div class="panel panel-info mt5 br-n">
        <div class="panel-header bg-primary clearfix p10 ph15 text-center">
            <h3 class="mt10 mb5">{{ $sites->getConfig('site_name') }}</h3>
        </div>
        <div class="clearfix"></div>
        <!-- end .form-header section -->
        <form method="post" action="{{ URL::full() }}">
        <input name="_token" value="{{ csrf_token() }}" type="hidden"/>
            <div class="panel-body bg-light p30 ">
                @include('topaz::flashbag')
                <div class="row">
                    <div class="col-sm-12 pr30">

                        <div class="section row">
                            <div class="section">
                                <label for="username" class="field-label text-muted fs18 mb10">Identifiant</label>
                                <label for="username" class="field prepend-icon">
                                    <input type="text" name="username" id="username" class="gui-input" value="{{ $username or '' }}" placeholder="Entrez votre identifiant de connexion">
                                    <label for="username" class="field-icon">
                                        <i class="fa fa-user"></i>
                                    </label>
                                </label>
                            </div>
                            <!-- end section -->

                            <div class="section">
                                <label for="password" class="field-label text-muted fs18 mb10">Mot de Passe</label>
                                <label for="password" class="field prepend-icon">
                                    <input type="password" name="password" id="password" class="gui-input" placeholder="Entrez votre mot de passe">
                                    <label for="password" class="field-icon">
                                        <i class="fa fa-lock"></i>
                                    </label>
                                </label>
                            </div>
                            <!-- end section -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- end .form-body section -->
            <div class="panel-footer clearfix p10 ph15">
                <button type="submit" class="button btn-primary mr10 pull-right">Se Connecter</button>
                <label class="switch ib switch-primary pull-left input-align mt10">
                    <input type="checkbox" name="remember" id="remember" value="1" checked>
                    <label for="remember" data-on="OUI" data-off="NON"></label>
                    <span>Se souvenir de moi</span>
                </label>
            </div>
            <!-- end .form-footer section -->
        </form>
    </div>
</div>
@stop

@section('stylesheets')
    <style>
        .panel-header {
            border-top-right-radius: 2px;
            border-top-left-radius: 2px;
            border-bottom: 1px solid #DDD;
            background: #f2f2f2;
        }
    </style>
@endsection