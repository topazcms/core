<?php namespace Topaz\Core\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Topaz\Core\Models\User;
use Topaz\Core\Services\TopazService;

abstract class Controller extends BaseController {

    use DispatchesCommands, ValidatesRequests;

    protected $request;
    protected $auth;
    /** @var User $user */
    protected $user;

    /** @var TopazService */
    protected $topaz;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->auth = app('auth')->admin();
        $this->user = $this->auth->get();
        $this->topaz = app('topaz');
        $this->sites = app('topaz.sites');
    }

}
