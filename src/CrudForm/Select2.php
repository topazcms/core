<?php


namespace Topaz\Core\CrudForm;


class Select2 extends Field
{
    public function __construct($name, array $items, $label = null, $help = null)
    {
        parent::__construct($name, $label, $help);

        $this->addExtra('items', $items);
        $this->addExtra('select2-class', 'select2-single');
    }

    /**
     * @return $this
     */
    public function noSearch()
    {
        $this->addExtra('select2-class', 'select2-single-nosearch');
        return $this;
    }
}