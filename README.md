# Things to update

## User model

In config/app.php update

```php
'model' => 'Topaz\User',
```

by

```php
'model' => 'Topaz\Core\Models\User',
```

## Add Minify to Kernel

In app/Http/Kernel.php, add in the $middleware array :

```php
protected $middleware = [
		// ...
		'GrahamCampbell\HTMLMin\Http\Middleware\MinifyMiddleware',
	];
```