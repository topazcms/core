@extends('{THEME_ID}.app')

@section('body')
    <div class="page-header"><h3>{{ $page->title }}</h3></div>

    @if ($page->cover_id)
        <div class="text-center">
            <img src="{{ $page->cover->direct_url }}" alt="Couverture">
        </div>
    @endif

    {!! $page->page_body !!}
@stop