<script src="{{ asset('topaz/admin_assets/vendor/plugins/select2/select2.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $(".select2-single").select2();
        $(".select2-single-nosearch").select2({
            minimumResultsForSearch: Infinity
        });
    });
</script>