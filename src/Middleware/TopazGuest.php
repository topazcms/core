<?php

namespace Topaz\Core\Middleware;

use Closure;
use Illuminate\Auth\Guard;

class TopazGuest
{
    protected $auth;

    public function __construct()
    {
        $this->auth = app('auth')->admin();
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check())
        {
            return redirect()->route('admin.index');
        }

        return $next($request);
    }
}
