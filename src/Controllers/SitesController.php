<?php namespace Topaz\Core\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Topaz\Core\CrudForm\CKEditor;
use Topaz\Core\CrudForm\CrudForm;
use Topaz\Core\CrudForm\Datepicker;
use Topaz\Core\CrudForm\Image;
use Topaz\Core\CrudForm\Select2;
use Topaz\Core\CrudForm\Text;
use Topaz\Core\CrudForm\Checkbox;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\Settings;
use Topaz\Core\Models\Site;
use Topaz\Core\Models\User;
use Topaz\Core\Services\SitesManager;

class SitesController extends TopazCrudController {

    protected $modelName = Site::class;
    protected $route_prefix = 'admin.settings.sites';

    protected $object_name_singular = 'site';
    protected $object_name_plural = 'sites';
    protected $object_name_male = true;

    protected $color = 'danger';

    protected $index_table = [
        'Nom' => 'name_column',
        'Préfixe' => 'prefix',
        'Thème' => 'theme_column',
        'Langue' => 'lang',
        'Création' => 'dates_column',
    ];
    protected $index_sorting = [
        'Nom' => 'name',
        'Préfixe' => 'prefix',
        'Création' => 'created_at',
    ];

    protected $validation_rules = [
        'name' => 'required',
    ];

    protected $orderBy = ['created_at', 'asc'];
    protected $objectsPerPage = 20;

    public static function routes()
    {
        parent::crud_routes('settings/sites', 'settings.sites.', ['middleware' => 'permission:sites.manage']);
    }

    protected static function custom_routes()
    {
        Route::get('default/{site_id}', ['as' => 'default', 'uses' => get_called_class().'@setDefault']);
        Route::get('enable/{site_id}', ['as' => 'enable', 'uses' => get_called_class().'@enable']);
        Route::get('disable/{site_id}', ['as' => 'disable', 'uses' => get_called_class().'@disable']);
    }

    public function indexQuery($query)
    {
        return $query;
    }

    public function searchQuery($query, $term)
    {
        $like_term = "%$term%";
        return $query->where(function ($q) use ($like_term) {
            $q->orWhere('name', 'like', $like_term);
            $q->orWhere('prefix', 'like', $like_term);
            $q->orWhere('lang', 'like', $like_term);
        });
    }

    protected function beforeAddForm($object)
    {
        $object->theme = config('topaz.default_theme');
    }

    protected function afterCreateForm($object)
    {
        /** @var SitesManager $sites */
        $sites = app('topaz.sites');
        if ($this->request->get('duplicate_default', 0)) {
            $sites->callDuplicateScripts($object, $sites->getDefaultSite());
        }
    }

    protected function afterDelete($object)
    {
        /** @var SitesManager $sites */
        $sites = app('topaz.sites');
        $sites->callDeleteScripts($object);
    }

    protected function generateActionFor($object)
    {
        $actions = [];
        if ($object->active) {
            $actions[] = [
                'name' => "<i class='fa fa-ban'></i> Désactiver",
                'route' => 'admin.settings.sites.disable'
            ];
        } else {
            $actions[] = [
                'name' => "<i class='fa fa-check-circle'></i> Activer",
                'route' => 'admin.settings.sites.enable'
            ];
        }

        $actions[] = [
            'name' => "Définir par défaut",
            'route' => 'admin.settings.sites.default'
        ];

        return $actions;
    }


    /**
     * @param $object
     * @return CrudForm
     */
    public function getForm($object, $add)
    {
        $form = new CrudForm;

        $form->appendMain(with(new Text('name', "<i class=\"fa fa-globe\"></i> Nom du site"))->big());
        $form->appendMain(with(new Text('prefix', "Préfixe")));
        $form->appendMain(with(new Select2('raw_theme', config('topaz.themes'), "Thème"))->noSearch());
        $form->appendMain(with(new Text('lang', "Langue du site")));

        if ($add) {
            $form->appendSidebar(with(new Checkbox('duplicate_default', "Dupliquer du site par défaut", "Duplique tous les éléments (routes, menus, pages, etc..) du site par défaut sur le nouveau site.<div class='text-danger'>Attention : Les éléments mis dans la corbeille ne sont pas dupliqués !</div>", 'success')));
        }

        return $form;
    }

    public function setDefault($site_id)
    {
        $site = Site::findOrFail($site_id);
        Site::query()->update(['default' => 0]);
        $site->default = true;
        $site->save();
        $this->request->session()->flash('success', "Le site <b>{$site}</b> est désormais le site par défaut");

        return $this->redirectToIndex();
    }

    public function enable($site_id)
    {
        $site = Site::findOrFail($site_id);
        $site->active = true;
        $site->save();
        $this->request->session()->flash('success', "Le site <b>{$site}</b> a bien été activé");

        return $this->redirectToIndex();
    }

    public function disable($site_id)
    {
        $site = Site::findOrFail($site_id);
        $site->active = false;
        $site->save();
        $this->request->session()->flash('success', "Le site <b>{$site}</b> a bien été activé");

        return $this->redirectToIndex();
    }
}
