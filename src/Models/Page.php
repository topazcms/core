<?php namespace Topaz\Core\Models;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Topaz\Core\Services\RouteContext;
use Topaz\Core\Services\TopazBreadcrumb;

/**
 * Topaz\Core\Models\Page
 *
 * @property-read User $author
 * @property-read \Topaz\Core\Models\Media $cover
 * @property-read Page $parent
 * @property-read mixed $excerpt
 * @property-read mixed $edit_route
 * @property-read mixed $author_name
 * @property mixed $page_body
 * @property-read mixed $title_column
 * @property-read mixed $dates_column
 * @property-read mixed $all_parents
 * @property-read mixed $direct_children
 * @property-read mixed $all_children
 * @property-read \Illuminate\Database\Eloquent\Collection|Resource[] $resources
 * @property-read mixed $url
 * @property-read mixed $has_route
 * @property-read Site $site
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page userRestricted()
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page ofSite($site_id)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page defaultSite()
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page currentSite()
 * @property integer $id
 * @property string $title
 * @property integer $author_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property string $layout
 * @property integer $cover_id
 * @property integer $parent_id
 * @property integer $site_id
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page wherePageBody($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page whereAuthorId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page whereLayout($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page whereCoverId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Page whereSiteId($value)
 */
class Page extends ResourceableModel implements ResourceableInterface {

    use SoftDeletes, SEOToolsTrait, HasSite;

	protected $table = 'topaz_pages';
//    protected $route_prefix = 'page/';
	protected $fillable = ['title', 'page_body', 'author_id', 'layout', 'cover_id', 'parent_id'];
    protected $dates = ['deleted_at'];

    public function __toString()
    {
        return $this->title;
    }

    public function getResourceIcon()
    {
        return 'file';
    }

    public function getResourceTitle()
    {
        return 'Page';
    }

    public function getResourceResponse(RouteContext $context)
    {
        app('topaz')->setPageTitle($this->title);
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('article:author', $this->authorName);
        $this->seo()->opengraph()->addProperty('article:published_time', $this->created_at->format(\DateTime::ISO8601));
        $this->seo()->opengraph()->addProperty('article:modified_time', $this->updated_at->format(\DateTime::ISO8601));
        if ($this->trashed())
            $this->seo()->opengraph()->addProperty('article:expiration_time', $this->deleted_at->format(\DateTime::ISO8601));

        if ($this->cover) {
            $this->seo()->opengraph()->addImage($this->cover->inline_url);
            $this->seo()->twitter()->addImage($this->cover->inline_url);
        }

        /** @var TopazBreadcrumb $breadcrumb */
        $breadcrumb = app('topaz.breadcrumb');
        $breadcrumb->append($this->all_parents);
        $breadcrumb->append($this);

        return view(app('topaz.sites')->getCurrentThemeView($this->layout), ['page' => $this]);
    }

    public function getResources()
    {
        return static::orderBy('updated_at', 'desc')->get();
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function cover()
    {
        return $this->belongsTo('Topaz\Core\Models\Media');
    }

    public function parent()
    {
        return $this->belongsTo(Page::class);
    }

    public function scopeUserRestricted($query)
    {
        $user = app('auth')->admin()->get();
        if ($user->page_restriction === null) {
            return $query;
        }

        $root = Page::findOrFail($user->page_restriction);

        $ids = $root->all_children->pluck('id');
        $ids[] = $root->id;

        return $query->whereIn('id', $ids);
    }

    public function getExcerptAttribute()
    {
        return Str::words(strip_tags($this->page_body));
    }

    public function getEditRouteAttribute()
    {
        return route('admin.pages.edit', $this);
    }

    public function getAuthorNameAttribute()
    {
        return $this->author->display_name;
    }

    public function setPageBodyAttribute($value)
    {
        $minifier = app('htmlmin');
        $value = $minifier->html($value);
        $value = app('topaz')->parseHtml($value);
        $this->attributes['page_body'] = $value;
    }

    public function getPageBodyAttribute()
    {
        if (!array_key_exists('page_body', $this->attributes)) {
            return null;
        }
        $page_body = $this->attributes['page_body'];
        $page_body = app('topaz')->unparseHtml($page_body);
        return $page_body;
    }

    public function getTitleColumnAttribute()
    {
        $edit_route = route('admin.pages.edit', $this);
        return "<h4><a href='$edit_route'>{$this->title}</a></h4><div>{$this->excerpt}</div>";
    }

    public function getDatesColumnAttribute()
    {
        $dates = [
            "Créée le {$this->created_at->format('d/m/Y à H:i:s')}",
        ];

        if ($this->trashed()) {
            $dates[] = "<span class='text-danger'>Supprimée le {$this->deleted_at->format('d/m/Y à H:i:s')}</span>";
        }

        return implode(array_map(function($val) { return "<div>$val</div>"; }, $dates));
    }

    public function getAllParentsAttribute()
    {
        $parent = $this->parent;
        $parents = new Collection();
        while ($parent !== null) {
            $parents->push($parent);
            $parent = $parent->parent;
        }

        return $parents->reverse();
    }

    public function getDirectChildrenAttribute()
    {
        return Page::whereParentId($this->id)->get();
    }

    protected function getChildren()
    {
        $children = new Collection();
        foreach ($this->direct_children as $child) {
            $child_children = $child->getChildren();
            $children->merge($child_children);
        }
        return $children;
    }


    public function getAllChildrenAttribute()
    {
        $children = $this->direct_children;
        $children->merge($this->getChildren());
        return $children;
    }

    public static function parents_list()
    {
        $res = [0 => '(Aucun parent)'];
        foreach (Page::all() as $page)
        {
            $res[$page->id] = $page->title;
        }

        return $res;
    }
}
