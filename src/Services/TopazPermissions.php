<?php


namespace Topaz\Core\Services;


use Artesaos\Defender\Defender;
use Artesaos\Defender\Role;

class TopazPermissions
{
    protected $permissions_needed = [];
    protected $roles_needed = ['superadmin', 'admin'];
    protected $roles_cache = [];

    /** @var Defender */
    protected $defender;

    public function __construct()
    {
        $this->defender = app('defender');
    }

    public function needPermission($name, $description, array $roles = [])
    {
        $roles = array_merge(['superadmin', 'admin'], $roles);
        $this->permissions_needed[$name] = (object)compact('name', 'description', 'roles');
    }

    public function needRole($name)
    {
        $this->roles_needed[] = $name;
    }

    public function cacheRoles()
    {
        foreach ($this->defender->rolesList() as $role_name) {
            $roles_cache[$role_name] = $this->defender->findRole($role_name);
        }
    }

    /**
     * @param $role_name
     * @return Role|null
     */
    public function findRole($role_name)
    {
        if (!array_key_exists($role_name, $this->roles_cache)) {
            $this->roles_cache[$role_name] = $this->defender->findRole($role_name);
        }

        return $this->roles_cache[$role_name];
    }

    public function createNeededPermission($name)
    {
        if (!array_key_exists($name, $this->permissions_needed)) {
            return false;
        }

        $perm = $this->permissions_needed[$name];
        if ($this->defender->findPermission($name) !== null) {
            return true;
        }

        $permission = $this->defender->createPermission($name, $perm->description);

        foreach ($perm->roles as $role_name) {
            $role = $this->findRole($role_name);
            $role->attachPermission($permission);
        }

        return true;
    }

    public function createNeededRole($name)
    {
        if (!in_array($name, $this->roles_needed)) {
            return false;
        }

        if ($this->findRole($name) !== null) {
            return true;
        }

        $this->roles_cache[$name] = $this->defender->createRole($name);

        return true;
    }

    public function neededPermissions()
    {
        return collect($this->permissions_needed);
    }

    public function neededRoles()
    {
        return collect($this->roles_needed);
    }
}