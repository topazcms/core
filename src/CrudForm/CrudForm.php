<?php


namespace Topaz\Core\CrudForm;


use Illuminate\Database\Eloquent\Collection;

class CrudForm
{
    /**
     * @var Collection
     */
    protected $main;

    /**
     * @var Collection
     */
    protected $sidebar;

    public function __construct()
    {
        $this->main = [];
        $this->sidebar = [];
    }

    public function appendMain(Field $field)
    {
        $this->main[] = $field;
    }

    public function appendSidebar(Field $field)
    {
        $this->sidebar[] = $field;
    }

    /**
     * @return Collection
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * @return Collection
     */
    public function getSidebar()
    {
        return $this->sidebar;
    }

    public function renderJavascript()
    {
        $res = "";
        // Import scripts

        $scripts = [];
        /** @var Field $field */
        foreach (array_merge($this->main, $this->sidebar) as $field)
        {
            $scripts = array_merge($scripts, $field->needs_scripts);
        }

        foreach ($scripts as $script) {
            $res .= '<script src="' . asset($script) . '"></script>';
        }

        return $res;

    }

    public function renderStylesheet()
    {
        $res = "";
        // Import scripts

        $styles = [];
        /** @var Field $field */
        foreach (array_merge($this->main, $this->sidebar) as $field)
        {
            $styles = array_merge($styles, $field->needs_styles);
        }

        foreach ($styles as $style) {
            $res .= '<link rel="stylesheet" href="' . asset($style) .'"/>';
        }

        return $res;

    }

    public function callBeforeJs()
    {
        $res = "";
        // Import scripts

        $calls = [];
        /** @var Field $field */
        foreach (array_merge($this->main, $this->sidebar) as $field)
        {
            foreach ($field->call_before_js as $function => $args)
            $calls[] = compact('function', 'args');
        }

        return $calls;

    }


}