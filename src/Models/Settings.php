<?php namespace Topaz\Core\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Topaz\Core\Models\Settings
 *
 * @property integer $id
 * @property string $site_name
 * @property string $site_description
 * @property string $site_keywords
 * @property string $robots_index
 * @property string $robots_follow
 * @property string $site_slogan
 * @property integer $site_id
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Settings whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Settings whereSiteName($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Settings whereSiteDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Settings whereSiteKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Settings whereRobotsIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Settings whereRobotsFollow($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Settings whereSiteSlogan($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Settings whereSiteId($value)
 * @property boolean $maintenance
 * @property boolean $maintenance_message
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Settings whereMaintenance($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Settings whereMaintenanceMessage($value)
 */
class Settings extends Model
{
    protected $table = 'topaz_settings';
    public $timestamps = false;
}
