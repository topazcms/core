<?php namespace Topaz\Core\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Topaz\Core\Models\AdminUser;

class AdminAuthController extends Controller {

    protected $auth;

    public function __construct()
    {
        $this->auth = app('auth')->admin();
    }

    public static function routes()
    {
        Route::get('login', ['as' => 'login', 'uses' => __CLASS__ . '@getLogin']);
        Route::post('login', __CLASS__ . '@postLogin');

        Route::get('logout', ['as' => 'logout', 'uses' => __CLASS__ . '@logout']);
    }

    public function getLogin()
    {
        return view('topaz::auth.login');
    }

    public function postLogin(Request $req)
    {
        $this->validate($req, [
           'username' => 'required',
           'password' => 'required',
        ]);

        $username = $req->get('username');
        $password = $req->get('password');
        $remember = ($req->get('remember', '0') == 1);

        if ($this->auth->attempt(['username' => $username, 'password' => $password, 'model_type' => AdminUser::class], $remember))
        {
            return redirect()->intended(route('admin.index'));
        }

        $req->session()->flash('error', "Aucun compte n'existe avec ces identifiants. Veuillez réessayer");
        return redirect()->route('admin.auth.login')->with('username', $username);
    }

    public function logout() {
        $this->auth->logout();
        return redirect()->route('admin.auth.login');
    }

}
