<?php namespace Topaz\Core\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Topaz\Core\Models\Resource
 *
 * @property-read \)->withTrashed( $resource
 * @property-read mixed $url
 * @property-read Site $site
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource active()
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource ofSite($site_id)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource defaultSite()
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource currentSite()
 * @property integer $id
 * @property string $route
 * @property string $resource_type
 * @property integer $resource_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $type
 * @property string $parameter
 * @property boolean $active
 * @property string $password
 * @property integer $site_id
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource whereRoute($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource whereResourceType($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource whereResourceId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource whereParameter($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Resource whereSiteId($value)
 */
class Resource extends Model {

    use HasSite;

	protected $table = 'topaz_router';

	protected $fillable = ['route', 'resource_type', 'resource_id', 'site_id'];

    public $casts = [
        'active' => 'bool',
    ];

    public function resource() {
        return $this->morphTo()->withTrashed();
    }

    public static function getDuplicateRoutes()
    {
        $res = [];
        $routes = \DB::select("SELECT route FROM topaz_router WHERE site_id = :site_id GROUP BY route HAVING count(route) > 1",[
            'site_id' => app('topaz.sites')->getCurrentSiteId()
        ]);
        foreach ($routes as $resp)
            $res[] = $resp->route;
        return $res;
    }

    public function getUrlAttribute()
    {
        $site = app('topaz.sites')->getSite($this->site_id);
        if ($site->default && !config('topaz.force_site_prefix')) {
            return url($this->route);
        } else {
            return url($site->prefix . '/' . $this->route);
        }
    }

    public function getHasParametersAttribute()
    {
        return str_contains($this->route, ['{', '}']);
    }

    public function getStylizedRouteAttribute()
    {
        return preg_replace('~\{(.*?)\}~s', '<span class="text-bold text-success">{$1}</span>', $this->route);
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}
