<?php


namespace Topaz\Core\CrudForm;


class Datepicker extends Field
{
    public function __construct($name, $label, $withTime = false, $help = null)
    {
        parent::__construct($name, $label, $help);

        $this->addExtra('php_format', $withTime ? 'd/m/Y H:i:s' : 'd/m/Y');

        $this->needStyle('topaz/admin_assets/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css');
        $this->needScript('topaz/admin_assets/vendor/plugins/moment/moment.min.js');
        $this->needScript('topaz/admin_assets/vendor/plugins/moment/fr.js');
        $this->needScript('topaz/admin_assets/vendor/plugins/datepicker/js/bootstrap-datetimepicker.js');
        $this->callBeforeJs('datepicker', [$name, [
            'format' => $withTime ? 'DD/MM/YYYY HH:mm:ss' : 'DD/MM/YYY'
        ]]);
    }


}