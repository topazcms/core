<div class="form-group {{ $errors->has($field->name) ? 'has-error' : '' }}">
    <div class="bs-component">
        @if ($field->label !== null)
            <label for="{{ $field->name }}">{!! $field->label !!}</label>
        @endif
        @yield($field->name . '__field')
        @if ($errors->has($field->name))
            @foreach ($errors->get($field->name) as $error)
                <span class="help-block">
                    <i class="fa fa-exclamation-circle"></i> {{ $error }}
                </span>
            @endforeach
        @else
            @if (!empty($field->help))
                <span class="help-block">
                    <i class="fa fa-question-circle"></i> {!! $field->help !!}
                </span>
            @endif
        @endif
    </div>
</div>