<?php namespace Topaz\Core\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Topaz\Core\Services\RouteContext;

/**
 * Topaz\Core\Models\Menu
 *
 * @property-read Resource $route
 * @property-read self $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|self[] $children
 * @property-read mixed $url
 * @property-read Site $site
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu ofSite($site_id)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu defaultSite()
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu currentSite()
 * @property integer $id
 * @property string $menu
 * @property string $title
 * @property integer $order
 * @property integer $route_id
 * @property integer $parent_id
 * @property string $extra
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $site_id
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu whereMenu($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu whereRouteId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu whereExtra($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Menu whereSiteId($value)
 */
class Menu extends Model {

    use HasSite;

	protected $table = 'topaz_menus';
//    protected $route_prefix = 'page/';
	protected $fillable = ['title', 'route_id', 'parent_id', 'extra'];
    protected $casts = [
        'options' => 'array',
    ];

    public function __toString()
    {
        return $this->title;
    }

    public function route()
    {
        return $this->belongsTo(Resource::class);
    }

    public function parent()
    {
        return $this->belongsTo(self::class);
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function getUrlAttribute()
    {
        if ($this->route === null)
            return '#';
        return $this->route->url;
    }

    public function getIsActiveAttribute()
    {
        $routing = app('topaz.routing');
        /** @var RouteContext $context */
        $context = $routing->getContext();
        return ($context !== null && $context->getRoute()->id == $this->route_id);
    }
}
