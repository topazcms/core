@extends('topaz::app')

@section('title', !$page->exists ? "Nouvelle Page" : "Modifier une Page")
@section('page_title')@yield('title')@endsection

@section('breadcrumb')
    @include('topaz::breadcrumb_item', ['title' => 'Pages', 'url' => route('admin.pages.index')])
@endsection

@section('breadcrumb_tail')@yield('title')@endsection

@section('topbar_right')
    @if ($page->exists)
        @if (true)
            <a href="{{ $page->url }}" class="btn btn-default btn-sm" target='_blank' rel="tooltip" data-placement="left" title="{{ $page->url }}">
                <i class="fa fa-external-link"></i> Voir la page
            </a>
        @else
            <span class="label label-danger">
                <i class="fa fa-arrow-right"></i> <i class="fa fa-circle-o mr10"></i> Aucune route ne mène à cette page
            </span>
        @endif
        <a href="{{ route('admin.pages.add') }}" class="btn btn-primary btn-sm text-bold ml15">
            <i class="fa fa-plus"></i> Nouvelle Page
        </a>
    @endif
@endsection

@section('body')
    <form action="{{ URL::full() }}" method="post" class="">
        <input name="_token" value="{{ csrf_token() }}" type="hidden"/>
        <div class="row table-layout table-clear-xs">
            <div class="col-xs-12 col-sm-9 br-a br-light bg-light p20">

                <div class="form-group">
                    <div class="bs-component">
                        <input id="title" name="title" placeholder="Titre de la Page" class="form-control input-lg" type="text" value="{{ Session::get('_old_input.title', $page->title) }}">
                    </div>
                </div>


                <div class="mb5">
                    <a href="javascript: void(0)" class="btn btn-dark light btn-sm" id="pick_image">
                        <i class="fa fa-photo"></i> Insérer une image
                    </a>
                    <a href="javascript: void(0)" class="btn btn-dark light btn-sm" id="pick_file">
                        <i class="fa fa-download"></i> Insérer un lien pour télécharger un fichier
                    </a>
                </div>

                <textarea name="page_body" id="page_body" rows="16">{{ Session::get('_old_input.page_body', $page->page_body) }}</textarea>
            </div>
            <div class="col-xs-12 col-sm-3 br-a br-light bg-light dark va-t p10">

                <button type="submit" class="btn btn-primary dark  text-bold btn-block mb15">
                    <i class="fa fa-save"></i>
                    @if ($page->exists)
                        Enregistrer
                    @else
                        Créer
                    @endif
                </button>

                <div class="form-group">
                    <label for="author_id"><i class="fa fa-user mr5"></i> Auteur</label>
                    <div class="bs-component">
                        {!! Form::select('author_id',
                            $authors,
                            Session::get('_old_input.author_id', $page->author_id),
                            ['class' => 'select2-single form-control']
                        ) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="author_id"><i class="fa fa-list-alt mr5"></i> Mise en page</label>
                    <div class="bs-component">
                        {!! Form::select('layout',
                            config('topaz.pages.layouts', ['page'=>'Page Simple']),
                            Session::get('_old_input.layout', $page->layout),
                            ['class' => 'select2-single-nosearch form-control']
                        ) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="author_id"><i class="fa fa-sitemap mr5"></i> Parent</label>
                    <div class="bs-component">
                        {!! Form::select('parent_id',
                            \Topaz\Core\Models\Page::parents_list(),
                            old('parent_id', $page->parent_id),
                            ['class' => 'select2-single form-control']
                        ) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="author_id"><i class="fa fa-photo mr5"></i> Photo d'illustration</label>
                    <div class="bs-component" id="cover_container">

                    </div>
                </div>

                <hr class="mt10 mb15" />

                @if ($page->exists)
                    <div class="text-center mt5">
                        <button type="button" class="btn btn-sm btn-danger dark text-bold" data-toggle="popover"
                                data-html="true"
                                data-placement="left"
                                data-content="Voulez-vous vraiment mettre cette page à la corbeille ?<br/>
                                <a href='{{ route('admin.pages.delete', $page) }}' class='btn btn-block btn-danger dark mt5 mb5'>
                                <i class='fa fa-trash-o'></i> Supprimer la page
                                </a>
                                ">
                            <i class="fa fa-trash-o"></i>
                            Supprimer
                        </button>
                    </div>
                @endif
            </div>
        </div>
    </form>
@stop

@section('stylesheets')
    @include('topaz::form_css')
    <link rel="stylesheet" href="{{ asset('topaz/admin_assets/vendor/plugins/magnific/magnific-popup.css') }}"/>
@endsection

@section('javascripts')
    @include('topaz::form_js')
    <script src="{{ asset('topaz/admin_assets/vendor/plugins/magnific/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('topaz/admin_assets/vendor/plugins/ckeditor/ckeditor.js') }}"></script>
    {!! file_picker('pick_file', [
        'show_name' => "Texte à afficher"
    ]) !!}
    {!! image_picker('pick_image', [
        'show_name' => "Légende"
    ]) !!}
    {!! image_field('cover_container', 'cover_id', $page->cover ) !!}
    {!! ckeditor('page_body') !!}
    <script>
        $(document).ready(function() {
            $(document).on('picked', '#pick_file', function(e, url, name) {
                CKEDITOR.instances['page_body'].insertHtml('<a href="' + url + '">' + name + '</a>');
            });

            $(document).on('picked', '#pick_image', function(e, url, name) {
                CKEDITOR.instances['page_body'].insertHtml('<img src="' + url + '" alt="' + name + '" />');
            });
        });
    </script>
@endsection