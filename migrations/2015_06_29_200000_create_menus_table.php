<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topaz_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('menu');
            $table->string('title');
            $table->integer('order')->unsigned();
            $table->integer('route_id')->unsigned()->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->text('extra')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('topaz_menus');
    }
}
