@extends('topaz::crud.form.form_group')

@section($field->name . '__field')
    <input id="{{ $field->name }}" name="{{ $field->name }}" placeholder="{{ $field->placeholder }}" class="form-control {{ $field->getExtra('class') }}" type="{{ $field->getExtra('type', 'text') }}" value="{{ old($field->name, $field->getExtra('type') == 'password' ? '' : $object->{$field->name}) }}">
@endsection