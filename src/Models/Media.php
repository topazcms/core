<?php namespace Topaz\Core\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Topaz\Core\Models\Media
 *
 * @property-read mixed $type_icon
 * @property-read mixed $type_title
 * @property-read mixed $direct_url
 * @property-read mixed $inline_url
 * @property-read mixed $size
 * @property-read Site $site
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Media ofSite($site_id)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Media defaultSite()
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Media currentSite()
 * @property integer $id
 * @property string $name
 * @property string $filename
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $site_id
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Media whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Media whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Media whereFilename($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Media whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Media whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Media whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Media whereSiteId($value)
 */
class Media extends Model {

    use HasSite;

	protected $table = 'topaz_medias';
//    protected $route_prefix = 'page/';
	protected $fillable = ['name'];
    protected $casts = [
    ];

    protected $appends = ['type_icon', 'type_title', 'size', 'direct_url', 'inline_url'];

    public static $path = 'app/uploads/medias/';

    private $types = [
        'image' => [
            'title' => 'Image',
            'icon' => 'image',
            'extensions' => ['jpg','jpeg','bmp','png','gif']
        ]
    ];

    public function __toString()
    {
        return $this->name;
    }

    public function getTypeIconAttribute()
    {
        if (array_key_exists($this->type, $this->types)) {
            return $this->types[$this->type]['icon'];
        }

        return "file";
    }

    public function getTypeTitleAttribute()
    {
        if (array_key_exists($this->type, $this->types)) {
            return $this->types[$this->type]['title'];
        }

        return "Fichier";
    }

    public function getDirectUrlAttribute()
    {
        return route('medias.download', $this->filename);
    }

    public function getInlineUrlAttribute()
    {
        return route('medias.inline', $this->filename);
    }

    public function image_url($params = [])
    {
        return route('medias.image', array_merge(['filename' => $this->filename], $params));
    }

    public function getSizeAttribute()
    {
        $location = 'uploads/medias/' . $this->filename;
        $bytes = \Storage::size($location);
        $size = array('o','Ko','Mo','Go','To','Po','Eo','Zo','Yo');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.2f", $bytes / pow(1024, $factor)) . ' ' .  @$size[$factor];
    }

    public function updateType()
    {
        $type = 'file';

        foreach ($this->types as $t => $props)
        {
            $extensions = $props['extensions'];
            $upper = array();
            foreach ($extensions as $ext) {
                $upper[] = strtoupper($ext);
            }

            if (preg_match('/.('.implode('|', array_merge($extensions, $upper)).')$/', $this->filename))
            {
                $type = $t;
            }
        }

        $this->type = $type;
    }

}
