<?php namespace Topaz\Core\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Topaz\Core\CrudForm\CKEditor;
use Topaz\Core\CrudForm\CrudForm;
use Topaz\Core\CrudForm\Datepicker;
use Topaz\Core\CrudForm\Image;
use Topaz\Core\CrudForm\Select2;
use Topaz\Core\CrudForm\Text;
use Topaz\Core\CrudForm\Toggle;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\User;

class PagesController extends TopazCrudController {

    protected $modelName = Page::class;
    protected $route_prefix = 'admin.pages';

    protected $object_name_singular = 'page';
    protected $object_name_plural = 'pages';
    protected $object_name_male = false;

    protected $color = 'primary';

    protected $index_table = [
        'Titre/Extrait' => 'title_column',
        'Auteur' => 'author_name',
        'Dates' => 'dates_column',
    ];
    protected $index_sorting = [
        'Titre/Extrait' => 'title',
        'Dates' => 'updated_at',
    ];

    protected $validation_rules = [
        'title' => 'required',
        'author_id' => 'required|exists:topaz_users,id',
    ];

    protected $orderBy = ['updated_at', 'desc'];
    protected $objectsPerPage = 20;

    public static function routes()
    {
        parent::crud_routes('pages', 'pages.', ['middleware' => 'permission:pages.manage']);
    }

    public function globalQuery($query)
    {
        return $query->currentSite()->userRestricted();
    }

    public function indexQuery($query)
    {
        return $query->with('author');
    }

    public function searchQuery($query, $term)
    {
        $like_term = "%$term%";
        return $query->where(function ($q) use ($like_term) {
            $q->orWhere('title', 'like', $like_term);
        });
    }


    public function beforeAddForm($object)
    {
        $object->author_id = $this->user->id;
    }

    /**
     * @param $object
     * @return CrudForm
     */
    public function getForm($object, $add)
    {
        $form = new CrudForm;

        $form->appendMain(with(new Text('title', null))->setPlaceholder('Titre de la page')->big());
        $form->appendMain(with(new CKEditor('page_body', null)));

        $form->appendSidebar(with(new Select2('author_id', User::authors(), "<i class=\"fa fa-user\"></i> Auteur")));
        $form->appendSidebar(with(new Select2('layout', config('topaz.pages.layouts', ['page'=>'Page Simple']), "<i class=\"fa fa-list-alt mr5\"></i> Mise en page"))->noSearch());
        $form->appendSidebar(with(new Select2('parent_id', Page::parents_list(), "<i class=\"fa fa-sitemap mr5\"></i> Parent")));
        $form->appendSidebar(with(new Image('cover_id', $object->cover, "<i class=\"fa fa-photo mr5\"></i> Photo d'illustration")));

        return $form;
    }
}
