<?php namespace Topaz\Core\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Topaz\Core\CrudForm\CrudForm;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\User;

 class TopazCrudController extends Controller {

    protected $modelName = null;
    protected $index_title = null;
    protected $add_title = null;
    protected $edit_title = null;
    protected $route_prefix = null;

    protected $object_name_singular = null;
    protected $object_name_plural = null;
    protected $object_name_male = true;
    protected $object_name_defini = null;

    protected $color = 'default';
    protected $new_button = null;
    protected $has_search = true;

    protected $index_table = [
        'ID' => 'id',
        'Titre' => '__toString',
    ];
    protected $index_sorting = [
        'ID' => 'id',
        'Title' => 'title',
    ];
    protected $actions = [];

    protected $validation_rules = [];
    protected $create_validation_rules = [];
    protected $update_validation_rules = [];
    protected $validation_messages = [];
    protected $validation_attributes = [];

    protected $remove_in_trash = [];

    protected $sort_string = '-updated_at';
    protected $objectsPerPage = 20;

    public static function crud_routes($prefix, $as, $group_options = [])
    {
        Route::pattern('obj_id', '[0-9]+');

        Route::group(array_merge(['prefix' => $prefix, 'as' => $as], $group_options), function() {
            Route::get('/', ['as' => 'index', 'uses' => get_called_class().'@index']);

            Route::get('add', ['as' => 'add', 'uses' => get_called_class().'@add']);
            Route::post('add', get_called_class().'@create');

            Route::get('edit/{obj_id}', ['as' => 'edit', 'uses' => get_called_class().'@edit']);
            Route::post('edit/{obj_id}', get_called_class().'@update');

            Route::get('delete/{obj_id}', ['as' => 'delete', 'uses' => get_called_class().'@delete']);
            Route::get('restore/{obj_id}', ['as' => 'restore', 'uses' => get_called_class().'@restore']);

            static::custom_routes();

        });
    }

    public function __construct(Request $request)
    {
        parent::__construct($request);

        if ($this->modelName === null)
        {
            throw new \Exception('Please provide a $modelName attribute in ' . get_called_class());
        }

        if ($this->index_title === null)
        {
            $this->index_title = ucfirst($this->object_name_plural);
        }

        if ($this->add_title === null)
        {
            $this->add_title = ($this->object_name_male ? 'Nouveau' : 'Nouvelle') . ' ' . $this->object_name_singular;
        }

        if ($this->edit_title === null)
        {
            $this->edit_title = "Modifier " . ($this->object_name_male ? 'un' : 'une') . ' ' . $this->object_name_singular;
        }

        if ($this->object_name_defini === null)
        {
            $this->object_name_defini = ($this->object_name_male ? 'le' : 'la') . ' ' . $this->object_name_singular;
        }

        if ($this->route_prefix === null)
        {
            throw new \Exception('Please provide a $route_prefix attribute in ' . get_called_class());
        }
    }

    protected function hasSoftDelete()
    {
        return in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($this->modelName));
    }

    protected function generateActionFor($object)
    {
        return [];
    }

    protected function viewbag($vars)
    {
        $parameters = [
            'index_title' => $this->index_title,
            'add_title' => $this->add_title,
            'edit_title' => $this->edit_title,
            'route_prefix' => $this->route_prefix,
            'object_name_singular' => $this->object_name_singular,
            'object_name_plural' => $this->object_name_plural,
            'object_name_male' => $this->object_name_male,
            'new_button' => $this->new_button,
            'actions' => $this->actions,
            'color' => $this->color,
            'has_search' => $this->has_search,
            'breadcrumb' => $this->base_breadcrumb(),
            'has_soft_delete' => $this->hasSoftDelete(),
            'actions_for' => function ($object) {
                return array_merge($this->actions, $this->generateActionFor($object));
            }
        ];

        return array_merge($parameters, $vars);
    }

    private function crud_render($view, $vars)
    {
        return view('topaz::crud.' . $view, $this->viewbag($vars));
    }


    // Crud methods

    public function globalQuery($query)
    {
        return $query;
    }

    public function indexQuery($query)
    {
        return $query;
    }

    public function searchQuery($query, $term)
    {
        return $query;
    }

    protected function beforeAddForm($object) {}
    protected function beforeEditForm($object) {}
    protected function beforeCreateForm($object) {}
    protected function afterCreateForm($object) {}
    protected function beforeUpdateForm($object) {}
    protected function afterUpdateForm($object) {}
    protected function beforeDelete($object) {}
    protected function afterDelete($object) {}

    protected static function custom_routes() {}
    protected static function base_breadcrumb()
    {
        return [];
    }

    protected function getForm($object, $add) {
        return new CrudForm;
    }

    protected function newEntity($attributes = [])
    {
        $modelName = $this->modelName;
        return new $modelName($attributes);
    }

    protected function findOrFailEntity($id)
    {
        $modelName = $this->modelName;
        $q = $this->globalQuery($modelName::query());
        $entity = $q->where('id', $id)->first();
        if ($entity === null) {
            abort(404);
        }
        return $entity;
    }

    protected function queryEntity()
    {
        $modelName = $this->modelName;
        return $this->globalQuery($modelName::select($this->newEntity()->getTable() . '.*'));
    }

    /**
     * @return array
     */
    protected function sorting()
    {
        $sortString = $this->request->get('sort', $this->sort_string);
        $char = substr($sortString, 0, 1);
        if (in_array($char, ['+', '-'])) {
            $sortOrientation = ($char == '+') ? 'asc' : 'desc';
            $sortField = substr($sortString, 1, strlen($sortString) - 1);
            return array($sortOrientation, $sortField);
        } else {
            $sortField = 'id';
            $sortOrientation = 'desc';
            return array($sortOrientation, $sortField);
        }
    }

    // Actions

    public function index()
    {
        $objects = $this->indexQuery($this->queryEntity());

        $search_term = $this->request->get('search');
        if ($this->has_search && $search_term) {
            $this->searchQuery($objects, $search_term);
        }

        $is_trash = false;

        $table = $this->index_table;

        if ($this->request->has('trashed')) {
            $objects = $objects->onlyTrashed();
            $is_trash = true;

            foreach ($this->remove_in_trash as $key) {
                unset($table[$key]);
            }
        }

        // Sorting

        list($sortOrientation, $sortField) = $this->sorting();
        $index_sorting = $this->index_sorting;

        $index_sorting_links = [];
        foreach ($index_sorting as $header => $field) {
            $char = "+";
            if ($field == $sortField) {
                $char = ($sortOrientation == 'asc' ? '-' : '+');
            }

            $params = array_merge($this->request->all(), [
                'sort' => $char . $field
            ]);

            $index_sorting_links[$header] = route($this->route_prefix . '.index', $params);
        }

        $objects = $objects->orderBy($sortField, $sortOrientation);
        $objects = $objects->paginate($this->objectsPerPage);

        return $this->crud_render('index', compact('is_trash', 'objects', 'table', 'sortOrientation', 'sortField', 'index_sorting', 'index_sorting_links', 'search_term'));
    }

    public function add()
    {
        $object = $this->newEntity();
        $this->beforeAddForm($object);
        $form = $this->getForm($object, true);
        return $this->crud_render('form', compact('object', 'form'));
    }

    public function create()
    {
        $this->validate($this->request,
            array_merge($this->validation_rules, $this->create_validation_rules),
            $this->validation_messages,
            $this->validation_attributes
        );

        $object = $this->newEntity($this->request->except('_token'));
        $this->beforeCreateForm($object);
        $object->save();
        $this->afterCreateForm($object);

        $this->request->session()->flash('success', ucfirst($this->object_name_defini) . " <b>{$object}</b> a bien été " . ($this->object_name_male ? "créé" : "créée"));

        return redirect()->route($this->route_prefix . '.edit', $object);
    }

    public function edit($object_id)
    {
        $modelName = $this->modelName;
        $object = $this->findOrFailEntity($object_id);
        $this->beforeEditForm($object);
        $form = $this->getForm($object, false);
        return $this->crud_render('form', compact('object', 'form'));
    }

    public function update($object_id)
    {
        $this->validate($this->request,
            array_merge($this->validation_rules, $this->update_validation_rules),
            $this->validation_messages,
            $this->validation_attributes
        );

        $object = $this->findOrFailEntity($object_id);
        $object->fill($this->request->except('_token'));
        $this->beforeUpdateForm($object);
        $object->save();
        $this->afterUpdateForm($object);

        $this->request->session()->flash('success', ucfirst($this->object_name_defini) . " <b>{$object}</b> a bien été " . ($this->object_name_male ? "modifié" : "modifiée"));

        return redirect()->route($this->route_prefix . '.edit', $object);
    }

    public function delete($object_id)
    {
        $query = $this->queryEntity();

        if (!$this->hasSoftDelete()) {
            $object = $query->whereId($object_id)->firstOrFail();

            $this->beforeDelete($object);

            if (method_exists($object, 'clearRoutes')) {
                $object->clearRoutes();
            }

            $object->delete();
            $message = ucfirst($this->object_name_defini) . " <b>{$object}</b> a bien été définitivement " . ($this->object_name_male ? "supprimé" : "supprimée");
        } else {
            $object = $query->whereId($object_id)->withTrashed()->firstOrFail();

            $this->beforeDelete($object);

            if ($object->trashed()) {
                $object->clearRoutes();
                $object->forceDelete();
                $message = ucfirst($this->object_name_defini) . " <b>{$object}</b> a bien été définitivement " . ($this->object_name_male ? "supprimé" : "supprimée");
            } else {
                $object->disableRoutes();
                $object->delete();
                $message = ucfirst($this->object_name_defini) . " <b>{$object}</b> a bien été " . ($this->object_name_male ? "mis" : "mise") . ' à la corbeille';
            }
        }

        $this->afterDelete($object);

        $this->request->session()->flash('success', $message);
        return $this->redirectToIndex();
    }

    public function restore($page_id)
    {
        if (!$this->hasSoftDelete()) {
            $this->request->session()->flash('error', "Vous ne pouvez pas effectuer de restauration.");
            return redirect()->route($this->route_prefix . '.index', ['trashed' => 'show']);
        }

        $object = $this->queryEntity()->whereId($page_id)->withTrashed()->firstOrFail();

        if (!$object->trashed()) {
            $this->request->session()->flash('warning', ucfirst($this->object_name_defini) . " <b>{$object}</b> est déjà " . ($this->object_name_male ? "actif" : "active") . ". Impossible de " . ($this->object_name_male ? "le" : "la") . " restaurer.");
            return redirect()->back();
        }

        $object->restore();
        $object->enableRoutes();

        $this->request->session()->flash('success', ucfirst($this->object_name_defini) . " <b>{$object}</b> a bien été " . ($this->object_name_male ? "restauré" : "restaurée"));
        return $this->redirectToIndex();
    }

    /**
     * @return mixed
     */
    protected function redirectToIndex()
    {
        return redirect()->route($this->route_prefix . '.index');
    }

}
