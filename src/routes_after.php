<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => config('topaz.admin_root'), 'as' => 'admin.'], function() {

    Route::any('{catchall}', 'Topaz\Core\Controllers\AdminController@missing')
        ->where('catchall', '(.*)');

});

Route::any('{uri}', 'Topaz\Core\Controllers\EntrypointController@entrypoint')
    ->where('uri', '(.*)');