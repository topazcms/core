<?php namespace Topaz\Core\Models;

use Artesaos\Defender\Traits\HasDefender;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

/**
 * Topaz\Core\Models\User
 *
 * @property-write mixed $password
 * @property-read mixed $display_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\$roleModel[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\config('defender.permission_model')[] $permissions
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whichRoles($roles)
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $username
 * @property string $email
 * @property integer $model_id
 * @property string $model_type
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $page_restriction
 * @property integer $site_id
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereFirstname($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereLastname($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereModelId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereModelType($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User wherePageRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\User whereSiteId($value)
 */
class User extends Model implements AuthenticatableContract {

    use Authenticatable, HasDefender;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'topaz_users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['firstname', 'lastname', 'username', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = app('hasher')->make($value);
    }

	public function getDisplayNameAttribute()
	{
		return "{$this->firstname} {$this->lastname}";
	}

	public function can($permission)
	{
		return ($this->hasPermission($permission) || $this->hasRole('superadmin'));
	}

	public static function authors()
	{
		$res = [];
		$current_id = app('auth')->admin()->check() ? app('auth')->admin()->get()->id : 0;
		$users = User::all();
		foreach ($users as $user) {
			$name = $user->displayname;
			if ($current_id == $user->id)
				$name .= ' (Moi)';
			$res[$user->id] = $name;
		}

		return $res;
	}

	public function getDatesColumnAttribute()
	{
		$dates = [
			"Créé le {$this->created_at->format('d/m/Y à H:i:s')}",
			"Modifié le {$this->updated_at->format('d/m/Y à H:i:s')}",
		];

		return implode(array_map(function($val) { return "<div>$val</div>"; }, $dates));
	}

}
