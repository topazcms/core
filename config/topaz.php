<?php

return [
    'admin_root' => 'tpz-admin',
    'default_theme' => 'default',
    'force_site_prefix' => false,
    'themes' => [
        'default' => 'Thème par défaut',
    ],
    'pages'=> [
        'layouts' => [
            'page' => 'Page Simple',
        ],
        'slug' => '%TITLE%',
    ],
    'menus' => [
        'main' => "Menu Principal"
    ],
    'medias' => [
        // Max Upload size, in KB
        'max_size' => '16000'
    ]
];