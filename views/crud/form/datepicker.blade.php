@extends('topaz::crud.form.form_group')

@section($field->name . '__field')
    <input id="{{ $field->name }}" name="{{ $field->name }}" placeholder="{{ $field->placeholder }}" class="form-control {{ $field->getExtra('class') }}" type="text" value="{{ old($field->name, $object->{$field->name})->format($field->getExtra('php_format')) }}">
@endsection