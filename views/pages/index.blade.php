@extends('topaz::app')

@section('title', 'Pages')
@section('page_title', 'Pages')

@section('breadcrumb')
    {{--@include('topaz::breadcrumb_item', ['title' => 'Accueil', 'url' => route('admin.index')])--}}
@endsection

@section('topbar_right')

<ul class="nav nav-list nav-list-topbar pull-left mrn">
    <li class="active">
        <a href="{{ route('admin.pages.index') }}">Pages actives</a>
    </li>
    <li>
        <a href="{{ route('admin.pages.index', ['trashed' => 1]) }}"><i class="fa fa-trash-o"></i> Corbeille</a>
    </li>
</ul>
<div class="pull-left mt10 mr15 ml15 text-primary" style="font-size:0.5em">
    |
</div>
<div class="btn-group pull-left">
    <a class="btn btn-sm btn-primary text-bold" href="{{ route('admin.pages.add') }}">
        <i class="fa fa-plus"></i> Nouvelle Page
    </a>
</div>
@append

@section('breadcrumb_tail', 'Pages')

@section('body')
    <div class="panel panel-primary panel-border top">
        <div class="panel-body pn">

        <div class="table-responsive">
            <table class="table table-hovered">
                <thead>
                    <tr>
                        <th>Titre / Extrait</th>
                        <th>Auteur</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pages as $page)
                        <tr>
                            <td>
                                <h4><a href="{{ route('admin.pages.edit', $page) }}">{{ $page->title }}</a></h4>
                                {{ $page->excerpt }}
                            </td>
                            <td>
                                {{ $page->author->displayname }}
                            </td>
                            <td>
                                Créée le {{ $page->created_at->format('d/m/Y H:i:s') }}
                                <br>
                                Mise à jour le {{ $page->updated_at->format('d/m/Y H:i:s') }}
                            </td>

                        </tr>
                    @empty
                        <tr>
                            <td colspan="10" class=" text-center">
                                <div class="lead text-muted mt15">
                                    Aucune page
                                </div>
                                <a class="btn btn-primary btn-lg" href="{{ route('admin.pages.add') }}">
                                    <i class="fa fa-plus"></i> Créez votre première page
                                </a>
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="text-center">
            {!! $pages->render() !!}
        </div>

        </div>
    </div>
@stop