@extends('topaz::crud.form.form_group')

@section($field->name . '__field')
    <div class="mb5">
        <a href="javascript: void(0)" class="btn btn-dark light btn-sm" id="{{ $field->name }}__pick_image">
            <i class="fa fa-photo"></i> Insérer une image
        </a>
        <a href="javascript: void(0)" class="btn btn-dark light btn-sm" id="{{ $field->name }}__pick_file">
            <i class="fa fa-download"></i> Insérer un lien pour télécharger un fichier
        </a>
    </div>

    <textarea name="{{ $field->name }}" id="{{ $field->name }}" rows="16">{{ old($field->name, $object->{$field->name}) }}</textarea>
@endsection