<?php

use Topaz\Core\Services\TopazService;

if (!function_exists('topaz_version'))
{
    function topaz_version()
    {
        $json = json_decode(file_get_contents(__DIR__ . "/../composer.json"), true);
        return $json['version'];
    }
}

if (!function_exists('topaz_author'))
{
    function topaz_author()
    {
        $json = json_decode(file_get_contents(__DIR__ . "/../composer.json"), true);
        return $json['authors'][0]['name'];
    }
}

if (!function_exists('topaz_config'))
{
    function topaz_config($key, $site_id = null)
    {
        global $app;
        /** @var TopazService $topaz */
        $topaz = $app[TopazService::class];
        $topaz->getConfig($key, $site_id);
    }
}

if (!function_exists('topaz_jewel'))
{
    function topaz_jewel()
    {
        $words = [
            'bijou',
            'joyau',
            'diamant',
        ];

        return $words[array_rand($words)];
    }
}

if (!function_exists('file_picker'))
{
    function file_picker($button_id, $options = [])
    {
        $params = [
            'icon' => 'file',
            'title' => "Selectionner un fichier",
            'type' => "file",
            'modal_id' => 'modal_' . str_random(),
            'button_id' => $button_id,
            'show_name' => false
        ];

        $params = array_merge($params, $options);

        return view('topaz::helpers.media_picker', $params);
    }
}

if (!function_exists('image_picker'))
{
    function image_picker($button_id, $options = [])
    {
        $opts = array_merge([
            'icon' => 'photo',
            'title' => "Selectionner une image",
            'type' => "image",
        ], $options);
        return file_picker($button_id, $opts);
    }
}

if (!function_exists('image_field'))
{
    function image_field($container_id, $field_name, $media, $options = [])
    {
        $params = [
            'icon' => 'photo',
            'title' => "Selectionner une image",
            'type' => "image",
            'modal_id' => 'modal_' . str_random(),
            'container_id' => $container_id,
            'field_name' => $field_name,
            'media' => $media,
        ];

        $params = array_merge($params, $options);

        return view('topaz::helpers.image_field', $params);
    }
}

if (!function_exists('ckeditor'))
{
    function ckeditor($id, $options = [])
    {
        $params = compact('id');

        $params = array_merge($params, $options);

        return view('topaz::helpers.ckeditor', $params);
    }
}

if (!function_exists('ckeditor_pickers'))
{
    function ckeditor_pickers($editor_id, $file_picker_id, $image_picker_id, $options = [])
    {
        $params = compact('editor_id', 'file_picker_id', 'image_picker_id');

        $params = array_merge($params, $options);

        return view('topaz::helpers.ckeditor_pickers', $params);
    }
}

if (!function_exists('datepicker'))
{
    function datepicker($id, $options = [])
    {
        $params = compact('id');

        $params = array_merge($params, $options);

        return view('topaz::helpers.form_datepicker', $params);
    }
}

if (!function_exists('form_checkbox'))
{
    function form_checkbox($id, $options = [])
    {
        $params = compact('id');
        $params = array_merge($params, $options);

        return view('topaz::helpers.form_checkbox', $params);
    }
}

if (!function_exists('parse_domain'))
{
    function parse_domain($text)
    {
        return app('topaz')->parse_domain($text);
    }
}

if (!function_exists('unparse_domain'))
{
    function unparse_domain($text)
    {
        return app('topaz')->unparse_domain($text);
    }
}

if (!function_exists('copyThemeFile'))
{
    /**
     * @param $source
     * @param $destination
     * @return bool
     */
    function copyThemeFile($source, $destination)
    {
        if (!file_exists($destination)) {
            copy($source, $destination);

            $fileContents = file_get_contents($destination);



            $newContents = str_replace($search, $replace, $fileContents);
            $handle = fopen("theFile","w");
            fwrite($handle, $newContents);
            fclose($handle);

        }

        return true;
    }
}