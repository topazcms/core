<?php


namespace Topaz\Core\CrudForm;


class Image extends Field
{
    public function __construct($name, $image, $label, $help = null)
    {
        parent::__construct($name, $label, $help);

        $this->needStyle('topaz/admin_assets/vendor/plugins/magnific/magnific-popup.css');
        $this->needScript('topaz/admin_assets/vendor/plugins/magnific/jquery.magnific-popup.js');
        $this->callBeforeJs('image_field', [
            $name . '_container',
            $name,
            $image,
        ]);
    }


}