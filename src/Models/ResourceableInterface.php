<?php


namespace Topaz\Core\Models;


use Topaz\Core\Services\RouteContext;

interface ResourceableInterface
{
    /**
     * Return the Font Awesome resource icon
     * @return mixed
     */
    public function getResourceIcon();
    public function getResourceTitle();
    public function getResourceResponse(RouteContext $context);
    public function getResources();
}