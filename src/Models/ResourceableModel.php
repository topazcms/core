<?php namespace Topaz\Core\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Topaz\Core\Models\ResourceableModel
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|Resource[] $resources
 * @property-read mixed $url
 * @property-read mixed $has_route
 */
class ResourceableModel extends Model {

    protected $route_prefix = "";

    public function resources() {
        $this->morphMany(Resource::class, 'resource');
    }

    public function addRoute($route)
    {
        $uri = $this->route_prefix . $route;
        $uriCount = count(Resource::currentSite()->whereRaw("route REGEXP '^{$uri}(-[0-9]*)?$'")->get());

        $uri = ($uriCount > 0) ? "{$uri}-{$uriCount}" : $uri;

        $resource = new Resource([
            'route' => $uri,
            'resource_type' => get_called_class(),
            'resource_id' => $this->id,
            'site_id' => app('topaz.sites')->getCurrentSiteId()
        ]);

        $resource->save();
    }

    public function clearRoutes()
    {
        Resource::where('resource_type', get_called_class())->where('resource_id', $this->id)->delete();
    }

    public function disableRoutes()
    {
        Resource::where('resource_type', get_called_class())->where('resource_id', $this->id)->update(['active' => false]);
    }

    public function enableRoutes()
    {
        Resource::where('resource_type', get_called_class())->where('resource_id', $this->id)->update(['active' => true]);
    }

    public function getUrlAttribute()
    {
        return app('topaz')->url_cache(get_called_class(), $this->id);
    }

    public function getHasRouteAttribute()
    {
        return Resource::where('resource_type', get_called_class())
            ->where('resource_id', $this->id)
            ->count() > 1;
    }
}
