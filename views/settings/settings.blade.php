@extends('topaz::app')

@section('title', "Paramètres du site")
@section('page_title')@yield('title')@endsection

@section('breadcrumb_tail')@yield('title')@endsection

@section('body')
    <form action="{{ URL::full() }}" method="post" class="">
        <input name="_token" value="{{ csrf_token() }}" type="hidden"/>

        <div class="panel mb35">
            <div class="panel-heading">
                <span class="panel-title"><i class="fa fa-globe"></i> Propriétés du site</span>
            </div>
            <div class="panel-body">
                <div class="admin-form">

                    <div class="section row mb10">
                        <label for="site_name" class="field-label col-md-2 text-right text-bold">
                            Nom du site :
                        </label>
                        <div class="col-md-10">
                            <label for="site_name" class="field prepend-icon">
                                <input type="text" name="site_name" id="site_name" class="gui-input" value="{{ Session::get('_old_input.site_name', $settings->site_name) }}">
                                <label for="site_name" class="field-icon">
                                    <i class="fa fa-globe"></i>
                                </label>
                            </label>
                        </div>
                    </div>

                    <div class="section row mb10">
                        <label for="site_slogan" class="field-label col-md-2 text-right text-bold">
                            Slogan du site :
                        </label>
                        <div class="col-md-10">
                            <label for="site_slogan" class="field prepend-icon">
                                <input type="text" name="site_slogan" id="site_slogan" class="gui-input" value="{{ Session::get('_old_input.site_slogan', $settings->site_slogan) }}">
                                <label for="site_name" class="field-icon">
                                    <i class="fa fa-ellipsis-h"></i>
                                </label>
                            </label>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="panel mb35">
            <div class="panel-heading">
                <span class="panel-title"><i class="fa fa-search"></i> Référencement</span>
            </div>
            <div class="panel-body">
                <div class="admin-form">

                    <div class="section row mb10">
                        <label for="site_keywords" class="field-label col-md-2 text-right text-bold">
                            Description du site :
                        </label>
                        <div class="col-md-10">
                            <label for="site_description" class="field prepend-icon">
                                <textarea name="site_description" id="site_description" class="gui-textarea">{{ old('site_description', $settings->site_description) }}</textarea>
                                <label for="site_description" class="field-icon">
                                    <i class="fa fa-paragraph"></i>
                                </label>
                            </label>
                        </div>
                    </div>

                    <div class="section row mb10">
                        <label for="site_keywords" class="field-label col-md-2 text-right text-bold">
                            Mots-clés :
                        </label>
                        <div class="col-md-10">
                            <div class="tag-container tags"></div>
                            <label for="site_keywords" class="field prepend-icon">
                                <input type="hidden" name="site_keywords" id="site_keywords" value="{{ old('site_keywords', $settings->site_keywords) }}">
                                <input type="text" id="site_keywords_add" class="form-control tm-input" placeholder="Ajouter un mot-clé...">
                                <label for="site_keywords" class="field-icon">
                                    <i class="fa fa-tags"></i>
                                </label>
                            </label>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="panel mb35">
            <div class="panel-heading">
                <span class="panel-title"><i class="fa fa-search"></i> Robots d'indexation</span>
            </div>
            <div class="panel-body">
                <div class="admin-form">

                    <div class="section row mb10">
                        <label for="site_keywords" class="field-label col-md-2 text-right text-bold">
                            Indexation :
                        </label>
                        <div class="col-md-10">
                            <div class="radio-custom mb10">
                                <input type="radio" id="robots_index" name="robots_index" value="index" {{ old('robots_index', $settings->robots_index) == 'index' ? 'checked=checked' : '' }}>
                                <label for="robots_index">Indéxer les pages du site</label>
                            </div>
                            <div class="radio-custom mb5">
                                <input type="radio" id="robots_noindex" name="robots_index" value="noindex" {{ old('robots_index', $settings->robots_index) == 'noindex' ? 'checked=checked' : '' }}>
                                <label for="robots_noindex">Ne pas indéxer les pages du site</label>
                            </div>
                        </div>
                    </div>

                    <div class="section row mb10">
                        <label for="site_keywords" class="field-label col-md-2 text-right text-bold">
                            Suivi des liens :
                        </label>
                        <div class="col-md-10">
                            <div class="radio-custom mb10">
                                <input type="radio" id="robots_follow" name="robots_follow" value="follow" {{ old('robots_follow', $settings->robots_follow) == 'follow' ? 'checked=checked' : '' }}>
                                <label for="robots_follow">Suivre les liens des pages</label>
                            </div>
                            <div class="radio-custom mb5">
                                <input type="radio" id="robots_nofollow" name="robots_follow" value="nofollow" {{ old('robots_follow', $settings->robots_follow) == 'nofollow' ? 'checked=checked' : '' }}>
                                <label for="robots_nofollow">Ne pas suivre les liens des pages</label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="panel mb35">
            <div class="panel-heading">
                <span class="panel-title"><i class="fa fa-lock"></i> Mode maintenance</span>
            </div>
            <div class="panel-body">
                <div class="admin-form">

                    <div class="section row mb10">
                        <label for="maintenance" class="field-label col-md-2 text-right text-bold">

                        </label>
                        <div class="col-md-10">
                            <div class="checkbox-custom mb10">
                                <input type="checkbox" id="maintenance" name="maintenance" value="1" {{ old('maintenance', $settings->maintenance) ? 'checked=checked' : '' }}>
                                <label for="maintenance" class="text-bold">Activer le mode maintenance</label>
                            </div>
                        </div>
                    </div>

                    <div class="section row mb10">
                        <label for="maintenance" class="field-label col-md-2 text-right text-bold">
                            Message :
                        </label>
                        <div class="col-md-10">
                            <label for="maintenance_message" class="field prepend-icon">
                                <textarea name="maintenance_message" id="maintenance_message" class="gui-textarea">{{ old('maintenance_message', $settings->maintenance_message) }}</textarea>
                                <label for="site_description" class="field-icon">
                                    <i class="fa fa-file-o"></i>
                                </label>
                            </label>
                        </div>
                    </div>

                    <div class="section row mb10">
                        <label for="maintenance_allow_admin" class="field-label col-md-2 text-right text-bold">

                        </label>
                        <div class="col-md-10">
                            <div class="checkbox-custom mb10">
                                <input type="checkbox" id="maintenance_allow_admin" name="maintenance_allow_admin" value="1" {{ old('maintenance_allow_admin', $settings->maintenance_allow_admin) ? 'checked=checked' : '' }}>
                                <label for="maintenance_allow_admin">Autoriser l'accès aux comptes connectés</label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="panel panel-warning mb35">
            <div class="panel-body bg-light">
                <div class="admin-form text-right">
                   <button type="submit" class="btn btn-primary font-bold">
                    <i class="fa fa-save"></i> Enregistrer les modifications
                   </button>
                </div>
            </div>
        </div>
    </form>
@stop

@section('stylesheets')
    @include('topaz::form_css')
    <link rel="stylesheet" href="{{ asset('topaz/admin_assets/vendor/plugins/tagmanager/tagmanager.css') }}"/>
@endsection

@section('javascripts')
    @include('topaz::form_js')
    <script src="{{ asset('topaz/admin_assets/vendor/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('topaz/admin_assets/vendor/plugins/tagmanager/tagmanager.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".tm-input").tagsManager({
                output: '#site_keywords',
                tagsContainer: $('.tag-container'),
                tagClass: 'tm-tag tm-tag-primary'
            });
        });
    </script>
@endsection