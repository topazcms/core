<?php


namespace Topaz\Core\CrudForm;


class Text extends Field
{
    public function __construct($name, $label = null, $help = null)
    {
        parent::__construct($name, $label, $help);
        $this->addExtra('type', 'text');
    }

    public function big()
    {
        $this->addExtra('class', 'input-lg');
        return $this;
    }

    public function password()
    {
        $this->addExtra('type', 'password');
        return $this;
    }
}