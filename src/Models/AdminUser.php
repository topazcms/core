<?php namespace Topaz\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Topaz\Core\Models\AdminUser
 *
 */
class AdminUser extends Model {

	protected $table = 'topaz_users_admin';
	protected $fillable = ['role', 'displayname'];
    public $timestamps = false;

    public function isAdmin() {
        return ($this->role == 'admin' || $this->role == 'superadmin');
    }

    public static function authors()
    {
        $res = [];
        $current_id = Auth::check() ? Auth::user()->id : 0;
        $users = User::select('topaz_users.id', 'topaz_users_admin.displayname')
            ->join('topaz_users_admin', 'topaz_users.model_id', '=', 'topaz_users_admin.id')
            ->where('model_type', self::class)->get();
        foreach ($users as $user) {
            $name = $user->displayname;
            if ($current_id == $user->id)
                $name .= ' (Moi)';
            $res[$user->id] = $name;
        }

        return $res;
    }

}
