<?php

namespace Topaz\Core\Middleware;

use Closure;
use Illuminate\Auth\Guard;

class TopazMinify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        global $app;
        $response = $next($request);
        if (env('APP_DEBUG'))
            return $response;
        $htmlmin = $app['htmlmin'];
        $response->setContent($htmlmin->html($response->getContent()));
        return $response;
    }
}
