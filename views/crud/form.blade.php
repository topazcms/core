@extends('topaz::app')

@section('title', !$object->exists ? $add_title : $edit_title)
@section('page_title')@yield('title')@endsection

@section('breadcrumb')
    @foreach ($breadcrumb as $item)
        @include('topaz::breadcrumb_item', $item)
    @endforeach
    @include('topaz::breadcrumb_item', ['title' => $index_title, 'url' => route($route_prefix . '.index')])
@endsection

@section('flashbag_errors')
    <div class="alert alert-border-left alert-danger">
        <i class="fa fa-exclamation-triangle"></i>
        <b class="alert-title">Veuillez résoudre les erreurs ci-dessous afin de poursuivre</b>
    </div>
@endsection

@section('breadcrumb_tail')@yield('title')@endsection

@section('topbar_right')
    @if ($object->exists && $object instanceof \Topaz\Core\Models\ResourceableModel)
        @if (true)
            <a href="{{ $object->url }}" class="btn btn-default btn-sm" target='_blank' rel="tooltip" data-placement="left" title="{{ $object->url }}">
                <i class="fa fa-external-link"></i> Voir la page
            </a>
        @else
            <span class="label label-danger">
                <i class="fa fa-arrow-right"></i> <i class="fa fa-circle-o mr10"></i> Aucune route ne mène à cette page
            </span>
        @endif
    @endif
@endsection

@section('body')
    <form action="{{ URL::full() }}" method="post" class="">
        <input name="_token" value="{{ csrf_token() }}" type="hidden"/>
        <div class="row table-layout table-clear-xs">
            <div class="col-xs-12 col-sm-9 br-a br-light bg-light p20" style="vertical-align: top">

                @foreach ($form->getMain() as $field)
                    @include('topaz::crud.form.' . $field->getViewName(), ['field' => $field])
                @endforeach


                {{--<div class="mb5">--}}
                    {{--<a href="javascript: void(0)" class="btn btn-dark light btn-sm" id="pick_image">--}}
                        {{--<i class="fa fa-photo"></i> Insérer une image--}}
                    {{--</a>--}}
                    {{--<a href="javascript: void(0)" class="btn btn-dark light btn-sm" id="pick_file">--}}
                        {{--<i class="fa fa-download"></i> Insérer un lien pour télécharger un fichier--}}
                    {{--</a>--}}
                {{--</div>--}}

                {{--<textarea name="page_body" id="page_body" rows="16">{{ Session::get('_old_input.page_body', $object->page_body) }}</textarea>--}}
            </div>
            <div class="col-xs-12 col-sm-3 br-a br-light bg-light dark va-t p10">

                <button type="submit" class="btn btn-primary dark  text-bold btn-block mb15">
                    <i class="fa fa-save"></i>
                    @if ($object->exists)
                        Enregistrer
                    @else
                        Créer
                    @endif
                </button>


                @foreach ($form->getSidebar() as $field)
                    @include('topaz::crud.form.' . $field->getViewName(), ['field' => $field])
                @endforeach

                <hr class="mt10 mb15" />

                @if ($object->exists)
                    <div class="text-center mt5">
                        <button type="button" class="btn btn-sm btn-danger dark text-bold" data-toggle="popover"
                                data-html="true"
                                data-placement="left"
                                data-content="Voulez-vous vraiment {{ $has_soft_delete ? 'mettre ceci à la corveille' : 'supprimer ceci définitivement' }} ?<br/>
                                <a href='{{ route($route_prefix . '.delete', $object) }}' class='btn btn-block btn-danger dark mt5 mb5'>
                                <i class='fa fa-trash-o'></i> Supprimer
                                </a>
                                ">
                            <i class="fa fa-trash-o"></i>
                            Supprimer
                        </button>
                    </div>
                @endif
            </div>
        </div>
    </form>
@stop

@section('stylesheets')
    @include('topaz::form_css')
{{--    <link rel="stylesheet" href="{{ asset('topaz/admin_assets/vendor/plugins/magnific/magnific-popup.css') }}"/>--}}
    {!! $form->renderStylesheet() !!}
@endsection

@section('javascripts')
    @include('topaz::form_js')
    {{--<script src="{{ asset('topaz/admin_assets/vendor/plugins/magnific/jquery.magnific-popup.js') }}"></script>--}}
    {{--<script src="{{ asset('topaz/admin_assets/vendor/plugins/ckeditor/ckeditor.js') }}"></script>--}}
    {!! $form->renderJavascript() !!}

    @foreach($form->callBeforeJs() as $call)
        {!! call_user_func_array($call['function'], $call['args']) !!}
    @endforeach
@endsection