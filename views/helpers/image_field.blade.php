
<script>
    $(document).ready(function() {

        var container = $('#{{ $container_id }}');

        container.empty();
        container.append($('<input>')
                .attr('id', '{{ $field_name }}')
                .attr('name', '{{ $field_name }}')
                .attr('type', 'hidden')
        );
        container.append($('<div>').attr('id', 'image_{{ $container_id }}'));
        container.append(
                $('<div>').addClass('text-center').append($('<a>')
                            .attr('class', 'btn btn-default btn-sm mt5')
                            .attr('id', 'pick_image_{{ $container_id }}')
                            .html('<i class="fa fa-folder-open"></i> Lier...'))
                        .append($('<a>')
                            .attr('class', 'btn btn-warning btn-sm mt5 ml5')
                            .attr('style', 'display: none')
                            .attr('id', 'delete_{{ $container_id }}')
                            .html('<i class="fa fa-times"></i> Supprimer'))
        );

        function setImage(id, url)
        {
            var field = $('#{{ $field_name }}');
            field.val(id);
            refreshImage(url);
        }

        function refreshImage(url)
        {
            var div = $('#image_{{ $container_id }}');
            div.empty();
            if (url == undefined || url == '') {
                $('#delete_{{ $container_id }}').css('display', 'none');
                div.html('<div class="text-muted text-center mt5 well well-sm mbn"><i class="fa fa-times"></i> Aucune image</div>');
            } else {
                $('#delete_{{ $container_id }}').css('display', 'inline-block');
                div.append($('<img>').attr('src', url).attr('alt', '').attr('style', 'width: 100%'));
            }
        }

        setImage('{{ $media->id or '' }}', '{{ $media->direct_url or '' }}');

        $(document).on('picked', '#pick_image_{{ $container_id }}', function(e, url, name, id) {
           setImage(id, url);
        });

        $(document).on('click', '#delete_{{ $container_id }}', function() {
           setImage('', '');
        });

    });
</script>
{!! image_picker('pick_image_' . $container_id, ['title' => "Choisissez une image de couverture"]) !!}
