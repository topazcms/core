<?php


namespace Topaz\Core\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Topaz\Core\Models\Settings;
use Topaz\Core\Models\Site;
use Topaz\Core\Models\User;

class SitesManager
{
    protected $current_site_id = null;
    protected $sites = null;
    protected $configs = null;

    protected $duplicate_scripts = [];
    protected $delete_scripts = [];
    protected $theme_creation_scripts = [];

    public function __construct()
    {
        $this->sites = Site::all()->keyBy('id');
        $this->configs = Settings::all()->keyBy('site_id');
        $this->current_site_id = $this->getDefaultSiteId();
        $this->defineId();
    }

    public function defineId()
    {
        if (Request::is(config('topaz.admin_root').'*')) {
            $new_site = app('session')->get('current_site_id', $this->current_site_id);
            if (!in_array($new_site, $this->sites->keys()->toArray())) {
                app('session')->set('current_site_id', $this->current_site_id);
            } else {
                $this->current_site_id = $new_site;
            }
        }
    }

    public function getDefaultSiteId()
    {
        return intval($this->sites->where('default', true)->first()->id);
    }

    public function getCurrentSiteId()
    {
        return $this->current_site_id;
    }

    public function setCurrentSiteId($site_id)
    {
        $this->current_site_id = $site_id;
    }

    /**
     * @return Site|null
     */
    public function getCurrentSite()
    {
        return $this->getSite($this->getCurrentSiteId());
    }

    /**
     * @return Site|null
     */
    public function getDefaultSite()
    {
        return $this->getSite($this->getDefaultSiteId());
    }

    /**
     * @param $site_id
     * @return Site|null
     */
    public function getSite($site_id)
    {
        if (!array_key_exists($site_id, $this->sites->toArray())) {
            return null;
        }
        return $this->sites[$site_id];
    }

    public function getSiteOfPrefix($prefix, $active = true)
    {
        return $this->sites->where('active', $active)->where('prefix', $prefix)->first();
    }

    public function setCurrentSite(Site $site)
    {
        $this->setCurrentSiteId($site->id);
    }

    public function getSettings($site_id = null)
    {
        if ($site_id === null) {
            $site_id = $this->current_site_id;
        }

        return $this->configs[$site_id];
    }

    public function getConfig($key, $site_id = null)
    {
        return $this->getSettings($site_id)->$key;
    }

    public function setConfig($key, $value, $site_id = null)
    {
        if ($site_id === null) {
            $site_id = $this->current_site_id;
        }

        $settings = $this->configs[$site_id];
        $settings->$key = $value;
    }

    public function commitSettings($site_id = null)
    {
        if ($site_id === null) {
            $site_id = $this->current_site_id;
        }
        return $this->configs[$site_id]->save();
    }

    public function setAdminSiteId($site_id)
    {
        $this->setCurrentSiteId($site_id);
        app('session')->set('current_site_id', $site_id);
    }

    public function getSites()
    {
        return $this->sites;
    }

    public function onDuplicate(\Closure $closure)
    {
        $this->duplicate_scripts[] = $closure;
    }

    public function callDuplicateScripts(Site $new_site, Site $source_site)
    {
        /** @var \Closure $closure */
        foreach ($this->duplicate_scripts as $closure) {
            $closure($new_site, $source_site);
        }
    }

    public function duplicateEntity(Site $new_site, $entity)
    {
        $className = get_class($entity);
        $new = new $className;
        $attributes = $entity->getAttributes();

        // We don't duplicate if the entity is trashed if model has soft deletion
        if (array_key_exists('deleted_at', $attributes) && $attributes['deleted_at'] !== null) {
            return false;
        }

        unset($attributes['id']);

        if (array_key_exists('created_at', $attributes)) {
            unset($attributes['created_at']);
        }

        if (array_key_exists('updated_at', $attributes)) {
            unset($attributes['updated_at']);
        }

        foreach ($attributes as $field => $value) {
            $new->$field = $value;
        }

        $new->site_id = $new_site->id;
        $new->save();

        return true;
    }

    public function onDelete(\Closure $closure)
    {
        $this->delete_scripts[] = $closure;
    }

    public function callDeleteScripts(Site $site)
    {
        /** @var \Closure $closure */
        foreach ($this->delete_scripts as $closure) {
            $closure($site);
        }
    }

    public function onThemeCreation(\Closure $closure)
    {
        $this->theme_creation_scripts[] = $closure;
    }

    public function callThemeCreationScripts($theme_id, $theme_name)
    {
        $theme_path = app()->basePath() . '/resources/views/' . $theme_id;

        $copy_function = function($source, $destination) use ($theme_id, $theme_path) {
            $destination = $theme_path . '/' . $destination;

            if (!file_exists($destination)) {

                if(!file_exists(dirname($destination)))
                    mkdir(dirname($destination), 0777, true);

                copy($source, $destination);

                $fileContents = file_get_contents($destination);

                $replaces = [
                    '{THEME_ID}' => $theme_id
                ];

                $newContents = str_replace(array_keys($replaces), array_values($replaces), $fileContents);
                $handle = fopen($destination, "w");
                fwrite($handle, $newContents);
                fclose($handle);

            }

            return true;
        };

        /** @var \Closure $closure */
        foreach ($this->theme_creation_scripts as $closure) {
            $closure($copy_function, $theme_id, $theme_path, $theme_name);
        }

    }

    public function getCurrentHomeUrl()
    {
        $site = $this->getCurrentSite();
        return url($site->default && !config('topaz.force_site_prefix') ? '/' : $site->prefix);
    }

    public function getActiveSites()
    {
        return $this->getSites()->where('active', true);
    }

    public function getCurrentThemeView($view)
    {
        return $this->getCurrentSite()->theme . '.' . $view;
    }

    public function getDefaultThemeView($view)
    {
        return $this->getDefaultSite()->theme . '.' . $view;
    }

    public function getThemeView($site_id, $view)
    {
        return $this->getSite($site_id)->theme . '.' . $view;
    }

    /**
     * @return bool
     */
    public function isMaintenance()
    {
        return $this->getConfig('maintenance');
    }


}