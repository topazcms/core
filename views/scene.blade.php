@extends('topaz::skeleton')

@section('body_class', "external-page sb-l-c sb-r-c")

@section('skeleton_body')
    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">

            <!-- Begin: Content -->
            <section id="content">

                @yield('body')

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->
@stop

@section('skeleton_javascripts')
@endsection

@section('js_inits')

@endsection