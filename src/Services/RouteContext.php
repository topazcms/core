<?php


namespace Topaz\Core\Services;


use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Topaz\Core\Models\Resource;

class RouteContext
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Resource
     */
    protected $route;

    /**
     * @var Collection
     */
    protected $route_params;

    /**
     * RouteContext constructor.
     * @param Request $request
     * @param Resource $route
     * @param Collection $route_params
     */
    public function __construct(Request $request, Resource $route, Collection $route_params = null)
    {
        if ($route_params == null) {
            $route_params = collect();
        }

        $this->request = $request;
        $this->route = $route;
        $this->route_params = $route_params;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Resource
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @return Collection
     */
    public function getRouteParams()
    {
        return $this->route_params;
    }

    /**
     * @param Resource $route
     * @return RouteContext
     */
    public function setRoute($route)
    {
        $this->route = $route;
        return $this;
    }

    /**
     * @param Collection $route_params
     * @return RouteContext
     */
    public function setRouteParams($route_params)
    {
        $this->route_params = $route_params;
        return $this;
    }

}