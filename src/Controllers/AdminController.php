<?php namespace Topaz\Core\Controllers;

use Illuminate\Support\Facades\Route;
use Topaz\Core\Services\SitesManager;

class AdminController extends Controller {

    public static function routes()
    {
        Route::get('/', ['as' => 'index', 'uses' => __CLASS__.'@index']);
        Route::get('/set-site/{site_id}', ['as' => 'set-site', 'uses' => __CLASS__.'@setSite']);
        Route::get('/notification/{notification_id}', ['as' => 'notification', 'uses' => __CLASS__.'@notification']);
    }

    public function index()
    {
        return view('topaz::index');
    }

    public function setSite($site_id)
    {
        /** @var SitesManager $sites */
        $sites = app('topaz.sites');
        $sites->setAdminSiteId($site_id);
        return redirect()->back();
    }

    public function notification($notification_id)
    {
        $notifynder = $this->topaz->notifynder();
        $notification = $notifynder->readOne($notification_id);
        if ($notification->to_id !== $this->user->id) {
            return redirect()->back();
        }
        $notification->read = 1;
        $notification->save();
        return redirect()->to($notification->url);
    }

    public function missing() {
        return view('topaz::index');
    }

}
