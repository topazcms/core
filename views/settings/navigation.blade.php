@extends('topaz::app')

@section('title', 'Navigation')
@section('page_title', 'Navigation')

@section('breadcrumb_tail', 'Navigation')

@inject('topaz', 'topaz')

@section('body')
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul class="nav panel-tabs-border panel-tabs panel-tabs-left">
                @foreach (config('topaz.menus') as $name => $title)
                    <li class="{{ $menu_name == $name ? 'active' : '' }}">
                        <a href="{{ route('admin.settings.navigation', $name) }}">{{ $title }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="panel-menu">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a class="btn btn-primary text-bold openModal" href="#">
                            <i class="fa fa-plus"></i> Nouveau
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">

        <div class="table-responsive">
            <table class="table table-hovered">
                <thead>
                    <tr>
                        <th>Titre</th>
                        <th>Route</th>
                        <th>Destination</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($items as $item)
                    <?php if ($item->route !== null) $route_type = $topaz->getRouteType($item->route->type); ?>
                        <tr>
                            <td>
                                {{ $item->title }}
                            </td>
                            <td>
                                @if ($item->route !== null)
                                    <code>{{ $item->route->route }}</code>
                                @endif
                            </td>
                            <td>
                                @if ($item->route === null)
                                    <i class="fa fa-circle-o"></i> Aucune action
                                @else
                                    <i class="fa fa-{{ $route_type['icon'] }}"></i> {{ $route_type['title'] }}
                                    <br/>
                                    @if ($route_type['name'] == 'resource')
                                        <i class="fa fa-{{ $item->route->resource->getResourceIcon() }}"></i> {{ $item->route->resource->getResourceTitle() }}
                                        /
                                        <a href="{{ $item->route->resource->editRoute }}" class="text-bold"><span class="text-primary">{{ $item->route->resource }}</span></a>
                                    @else
                                        {{ $item->route->parameter }}
                                    @endif
                                @endif
                            </td>
                            <td style="width: 200px;" class="text-right">
                                <div class="">
                                    <div class="btn-group">
                                        <a class="btn btn-default dark btn-sm move_menu" data-dir="up" data-id="{{ $item->id }}">
                                            <i class="fa fa-caret-up"></i>
                                        </a>
                                        <a class="btn btn-default dark btn-sm move_menu" data-dir="down" data-id="{{ $item->id }}">
                                            <i class="fa fa-caret-down"></i>
                                        </a>
                                    </div>
                                    <a class="btn btn-primary dark btn-sm openModal" data-id="{{ $item->id }}">
                                        <i class="fa fa-pencil"></i> Modifier
                                    </a>
                                </div>
                            </td>

                        </tr>
                        @foreach ($item->children as $subitem)
                            <tr class="bg-light">
                                <td>
                                    <span class="text-muted">{{ $item }} <i class="fa fa-caret-right"></i></span> {{ $subitem->title }}
                                </td>
                                <td>
                                    <code>{{ $subitem->route->route }}</code>
                                </td>
                                <td>
                                    <i class="fa fa-{{ $route_type['icon'] }}"></i> {{ $route_type['title'] }}
                                    <br/>
                                    @if ($route_type['name'] == 'resource')
                                        <i class="fa fa-{{ $subitem->route->resource->getResourceIcon() }}"></i> {{ $subitem->route->resource->getResourceTitle() }}
                                        /
                                        <a href="{{ $subitem->route->resource->editRoute }}" class="text-bold"><span class="text-primary">{{ $subitem->route->resource }}</span></a>
                                    @else
                                        {{ $subitem->route->parameter }}
                                    @endif
                                </td>
                                <td style="width: 200px;" class="text-right">
                                    <div class="">
                                        <div class="btn-group">
                                            <a class="btn btn-default dark btn-sm move_menu" data-dir="up" data-id="{{ $subitem->id }}">
                                                <i class="fa fa-caret-up"></i>
                                            </a>
                                            <a class="btn btn-default dark btn-sm move_menu" data-dir="down" data-id="{{ $subitem->id }}">
                                                <i class="fa fa-caret-down"></i>
                                            </a>
                                        </div>
                                        <a class="btn btn-primary dark btn-sm openModal" data-id="{{ $subitem->id }}">
                                            <i class="fa fa-pencil"></i> Modifier
                                        </a>
                                    </div>
                                </td>

                            </tr>
                        @endforeach
                    @empty
                        <tr>
                            <td colspan="10" class=" text-center">
                                <div class="lead text-muted mt15">
                                    Aucun élément de menu
                                </div>
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>


        </div>
    </div>

    <!-- Admin Form Popup -->
            <div id="modal" class="popup-basic horizontal-form mfp-with-anim mfp-hide">
              <div class="panel">
                <div class="panel-heading pb0">
                  <span class="panel-title">

                  </span>
                </div>
                <!-- end .panel-heading section -->

                <form method="post" action="/" id="modal-form">
                <input type="hidden" name="obj_id" value="new" />
                <input type="hidden" name="menu" value="{{ $menu_name }}" />
                  <div class="panel-body p25">

                    <div class="form-group">
                        <label for="title"><i class="fa fa-compass ml5 mr5"></i> Titre du menu</label>
                        <div class="bs-component">
                            <input id="title" name="title" placeholder="Titre" class="form-control" type="text" value="{{ Session::get('_old_input.title', '') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="route_id"><i class="fa fa-shuffle ml5 mr5"></i> Route</label>
                        <div class="bs-component">
                            {!! Form::select('route_id',
                                $routes,
                                '',
                                ['class' => 'select2-routes form-control', 'style' => 'width: 100%']
                            ) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="parent_id"><i class="fa fa-sitemap ml5 mr5"></i> Parent</label>
                        <div class="bs-component">
                            {!! Form::select('parent_id',
                                $items_list,
                                '',
                                ['class' => 'select2-single form-control', 'style' => 'width: 100%']
                            ) !!}
                        </div>
                    </div>

                  </div>
                  <!-- end .form-body section -->

                  <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">

                            </button>
                        </div>
                        <div class="col-md-8 text-right">
                            <div id="delete_step1">
                                <button type="button" class="btn btn-danger btn-rounded btn-sm mt5 toggle_delete">
                                    <i class="fa fa-trash-o"></i> Supprimer
                                </button>
                            </div>
                            <div id="delete_step2" style="display: none">
                                <button type="button" class="btn btn-danger btn-rounded btn-sm mt5" id="delete_obj">
                                    <b><i class="fa fa-trash-o"></i> Confirmer</b>
                                </button>
                                <button type="button" class="btn btn-default btn-rounded btn-sm mt5 toggle_delete">
                                    <i class="fa fa-undo"></i> Annuler
                                </button>
                            </div>
                        </div>
                    </div>
                  </div>
                  <!-- end .form-footer section -->
                </form>
              </div>
              <!-- end: .panel -->
            </div>
            <!-- end: .admin-form -->
@stop

@section('stylesheets')
    @include('topaz::form_css')
    <link rel="stylesheet" href="{{ asset('topaz/admin_assets/vendor/plugins/magnific/magnific-popup.css') }}"/>
@endsection

@section('javascripts')
    @include('topaz::form_js')
    <script src="{{ asset('topaz/admin_assets/vendor/plugins/magnific/jquery.magnific-popup.js') }}"></script>
    <script>

        $(document).ready(function() {

            function formatState(state) {
                if (!state.id) return state.text;
                var parts = state.text.split("|");
                var $state = "";
                if (parts[0].trim() != "")
                    $state = $("<code>" + parts[0] + "</code> <i class='fa fa-chevron-right'></i> <span><i class='fa fa-" + parts[1] + "'></i> " + parts[2] + "</span> " + parts[3]);
                else
                    $state = $("<span><i class='fa fa-" + parts[1] + "'></i> " + parts[2] + "</span> " + parts[3]);

                return $state;
            }

            $(".select2-routes").select2({
                templateResult: formatState,
                templateSelection: formatState
            });

            function updateModalStates()
            {

            }

            $('select[name=type]').on('change', updateModalStates);

            $('#modal-form').submit(function(e) {
                var form = $(this);
                var params = {
                    obj_id: $('input[name=obj_id]').val(),
                    menu: $('input[name=menu]').val(),
                    title: $('input[name=title]').val(),
                    route_id: $('select[name=route_id]').val(),
                    parent_id: $('select[name=parent_id]').val()
                };
                var submitBtn = form.find('button[type=submit]');
                submitBtn.addClass('disabled').html("<i class='fa fa-spinner fa-pulse'></i> Chargement...");

                $.ajax({
                    type: 'POST',
                    url: '{{ route('admin.settings.navigation.form') }}',
                    dataType: 'json',
                    data: params
                })
                .success(function(data) {
                      if (data.success) {
                          document.location.reload();
                      } else {
                          //alert(data.errors);
                      }
                  })
                  .error(function(xhr, ajaxOptions, thrownError) {
                    document.write(xhr.responseText);
                  });

                e.preventDefault();
                return false;
            });

            $('.toggle_delete').on('click', function() {
                $('#delete_step1').toggle();
                $('#delete_step2').toggle();
            });

            $('#delete_obj').on('click', function() {
                $(this).addClass('disabled').html("<i class='fa fa-spinner fa-pulse'></i> Chargement...");
                $.post('{{ route('admin.settings.navigation.delete') }}', {id: $(this).attr('data-id')}, function(data) {
                    location.reload();
                });
            });

            $('.move_menu').on('click', function() {
                $.post('{{ route('admin.settings.navigation.move') }}', {id: $(this).attr('data-id'), dir: $(this).attr('data-dir')}, function(data) {
                    location.reload();
                });
            });

            var openModal = $('.openModal');
            openModal.attr('data-effect', "mfp-zoomOut");

            openModal.on('click', function() {
                var btn = $(this);
                var modal = $('#modal');

                 if (!btn.is('[data-id]')) {
                    modal.find('.panel-title').html('<i class="fa fa-plus"></i> Nouvel élement de menu');
                    modal.find('button[type=submit]').html('<i class="fa fa-plus"></i> Créer');
                    $('input[name=obj_id]').val('new');

                    $('input[name=title]').val('');
                    $('select[name=route_id]').val($('select[name=route_id] option:first-child').attr('value')).trigger('change');
                    $('select[name=parent_id]').val($('select[name=parent_id] option:first-child').attr('value')).trigger('change');
                    $('#delete_step1').hide();
                 } else {
                    modal.find('.panel-title').html('<i class="fa fa-pencil"></i> Modifier une Route');
                    modal.find('button[type=submit]').html('<i class="fa fa-save"></i> Enregistrer');
                    $('input[name=obj_id]').val(btn.attr('data-id'));
                    $.post('{{ route('admin.settings.navigation.menu') }}', {id: btn.attr('data-id')}, function(data) {
                        $('input[name=title]').val(data.title);
                        $('select[name=route_id]').val(data.route_id).trigger('change');
                        $('select[name=parent_id]').val(data.parent_id).trigger('change');

                        $('#delete_step1').show().attr('data-id', btn.attr('data-id'));
                        $('button#delete_obj').attr('data-id', btn.attr('data-id'));
                    }, 'json');

                 }

                 updateModalStates();

                 $.magnificPopup.open({
                    removalDelay: 500, //delay removal by X to allow out-animation,
                    items: {
                        src: '#modal'
                    },
                    // overflowY: 'hidden', //
                    callbacks: {
                        beforeOpen: function(e) {
                            var Animation = btn.attr('data-effect');
                            this.st.mainClass = Animation;
                        }
                    },
                    midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
                });
            });

        });
    </script>
@endsection