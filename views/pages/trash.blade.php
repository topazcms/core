@extends('topaz::app')

@section('title', 'Pages')
@section('page_title', 'Pages')

@section('breadcrumb')
    {{--@include('topaz::breadcrumb_item', ['title' => 'Accueil', 'url' => route('admin.index')])--}}
@endsection

@section('topbar_right')

<ul class="nav nav-list nav-list-topbar pull-left mrn">
    <li>
        <a href="{{ route('admin.pages.index') }}">Pages actives</a>
    </li>
    <li class="active">
        <a href="{{ route('admin.pages.index', ['trashed' => 1]) }}"><i class="fa fa-trash-o"></i> Corbeille</a>
    </li>
</ul>
<div class="pull-left mt10 mr15 ml15 text-primary" style="font-size:0.5em">
    |
</div>
<div class="btn-group pull-left">
    <a class="btn btn-sm btn-primary text-bold" href="{{ route('admin.pages.add') }}">
        <i class="fa fa-plus"></i> Nouvelle Page
    </a>
</div>
@append

@section('breadcrumb_tail', 'Pages')

@section('body')
    <div class="panel panel-primary panel-border top">
        <div class="panel-body pn">

        <div class="table-responsive">
            <table class="table table-hovered">
                <thead>
                    <tr>
                        <th>Titre / Extrait</th>
                        <th>Extrait</th>
                        <th>Auteur</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pages as $page)
                        <tr>
                            <td>
                                <h4><a href="{{ route('admin.pages.edit', $page) }}">{{ $page->title }}</a></h4>
                                <div>{{ $page->excerpt }}</div>
                                <a href="{{ route('admin.pages.restore', $page) }}" class="btn btn-xs btn-success dark">
                                    <i class="fa fa-reply"></i> Restaurer la page
                                </a>
                                <a href="{{ route('admin.pages.delete', $page) }}" class="btn btn-xs btn-danger">
                                    <i class="fa fa-times"></i> Supprimer définitivement
                                </a>
                            </td>
                            <td>
                                {{ $page->author->displayname }}
                            </td>
                            <td>
                                Créée le {{ $page->created_at->format('d/m/Y H:i:s') }}
                                <br>
                                Mise à jour le {{ $page->updated_at->format('d/m/Y H:i:s') }}
                                <br>
                                <span class="text-danger">
                                    Supprimée le {{ $page->deleted_at->format('d/m/Y H:i:s') }}
                                </span>
                            </td>

                        </tr>
                    @empty
                        <tr>
                            <td colspan="10" class=" text-center">
                                <div class="lead text-muted mt15">
                                    Aucune page supprimée
                                </div>
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="text-center">
            {!! $pages->render() !!}
        </div>

        </div>
    </div>
@stop