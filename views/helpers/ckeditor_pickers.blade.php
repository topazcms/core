<script>
    $(document).ready(function() {
        $(document).on('picked', '#{{ $file_picker_id }}', function(e, url, name) {
            CKEDITOR.instances['{{ $editor_id }}'].insertHtml('<a href="' + url + '">' + name + '</a>');
        });

        $(document).on('picked', '#{{ $image_picker_id }}', function(e, url, name) {
            CKEDITOR.instances['{{ $editor_id }}'].insertHtml('<img src="' + url + '" alt="' + name + '" />');
        });
    });
</script>