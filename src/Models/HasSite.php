<?php


namespace Topaz\Core\Models;


trait HasSite
{
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    public function scopeOfSite($query, $site_id)
    {
        if ($site_id instanceof Site) {
            $site_id = $site_id->id;
        }

        return $query->whereSiteId($site_id);
    }

    public function scopeDefaultSite($query)
    {
        return $query->whereSiteId(app('topaz.sites')->getDefaultSiteId());
    }

    public function scopeCurrentSite($query)
    {
        return $query->whereSiteId(app('topaz.sites')->getCurrentSiteId());
    }


}