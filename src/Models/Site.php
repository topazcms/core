<?php namespace Topaz\Core\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

/**
 * Topaz\Core\Models\Site
 *
 * @property-read mixed $name_column
 * @property-read mixed $dates_column
 * @property-read mixed $url
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Site active()
 * @property integer $id
 * @property string $name
 * @property string $prefix
 * @property boolean $default
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property boolean $active
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Site whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Site whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Site wherePrefix($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Site whereDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Site whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Site whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Site whereActive($value)
 * @property string $theme
 * @method static \Illuminate\Database\Query\Builder|\Topaz\Core\Models\Site whereTheme($value)
 * @property mixed $raw_theme
 * @property-read mixed $theme_column
 */
class Site extends Model
{

    protected $table = 'topaz_sites';
//    protected $route_prefix = 'page/';
    protected $fillable = ['name', 'prefix', 'theme', 'raw_theme', 'lang'];
    protected $casts = [
        'default' => 'bool',
        'active' => 'bool',
    ];

    protected static $_defaultSite = null;
    protected static $_currentSite = null;

    public function __toString()
    {
        return $this->name;
    }

    public static function boot()
    {
        parent::boot();

        self::created(function ($site) {
            // Creating settings for the new site

            $settings = new Settings();
            $settings->site_id = $site->id;
            $settings->save();

        });
    }

    public function getNameColumnAttribute()
    {
        $res = '';

        if ($this->active) {
            $res.= '<span class="text-success"><i class="fa fa-circle"></i></span> ';
        } else {
            $res.= '<span class="text-danger"><i class="fa fa-circle"></i></span> ';
        }

        $res .= $this->name;
        if ($this->default) {
            $res .= '<span class="label label-primary ml5">Site par défaut</span>';
        }

        return $res;
    }

    public function getDatesColumnAttribute()
    {
        $dates = [
            "Créé le {$this->created_at->format('d/m/Y à H:i:s')}",
        ];

        return implode(array_map(function($val) { return "<div>$val</div>"; }, $dates));
    }

    public function getUrlAttribute()
    {
        if ($this->default) {
            return url('/');
        }

        return url('/' . $this->prefix);
    }

    public function getThemeAttribute()
    {
        if ($this->attributes['theme'] === null) {
            return config('topaz.default_theme');
        }
        return $this->attributes['theme'];
    }

    public function getRawThemeAttribute()
    {
        return $this->attributes['theme'];
    }

    public function setRawThemeAttribute($value)
    {
        $this->attributes['theme'] = $value;
    }

    public function getThemeColumnAttribute()
    {
        return config('topaz.themes')[$this->theme];
    }

    public function scopeActive($query)
    {
        return $query->whereActive(1);
    }

}
