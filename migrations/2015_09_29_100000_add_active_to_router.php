<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveToRouter extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('topaz_router', function(Blueprint $table)
		{
			$table->boolean('active')->default(true);
			$table->string('password')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('topaz_router', function(Blueprint $table)
        {
            $table->dropColumn('active');
            $table->dropColumn('password');
        });
	}

}
