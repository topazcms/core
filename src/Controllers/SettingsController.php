<?php namespace Topaz\Core\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Topaz\Core\Models\Menu;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\Resource;
use Topaz\Core\Services\SitesManager;

class SettingsController extends Controller {

    public static function routes()
    {
        get('edit', ['as' => 'edit', 'uses' => __CLASS__.'@editSettings']);
        post('edit', __CLASS__.'@updateSettings');

        get('router', ['as' => 'routing', 'uses' => __CLASS__.'@editRouting']);
        post('router/form', ['as' => 'router.form', 'uses' => __CLASS__.'@routerForm']);
        post('router/route', ['as' => 'router.route', 'uses' => __CLASS__.'@routerGet']);
        post('router/delete', ['as' => 'router.delete', 'uses' => __CLASS__.'@routerDelete']);
        get('router/active/{route_id}', ['as' => 'router.active', 'uses' => __CLASS__.'@routerActive']);

        post('navigation/form', ['as' => 'navigation.form', 'uses' => __CLASS__.'@navigationForm']);
        post('navigation/menu', ['as' => 'navigation.menu', 'uses' => __CLASS__.'@navigationGet']);
        post('navigation/delete', ['as' => 'navigation.delete', 'uses' => __CLASS__.'@navigationDelete']);
        post('navigation/move', ['as' => 'navigation.move', 'uses' => __CLASS__.'@navigationMove']);
        get('navigation/{menu?}', ['as' => 'navigation', 'uses' => __CLASS__.'@editNavigation']);
    }

    public function editSettings()
    {
        $settings = $this->sites->getSettings();
        return view('topaz::settings.settings', compact('settings'));
    }

    public function updateSettings()
    {
        $this->validate($this->request, [
            'site_name' => 'required',
        ]);

        $this->sites->setConfig('site_name', $this->request->get('site_name'));
        $this->sites->setConfig('site_slogan', $this->request->get('site_slogan'));
        $this->sites->setConfig('site_description', $this->request->get('site_description'));
        $this->sites->setConfig('site_keywords', $this->request->get('site_keywords'));
        $this->sites->setConfig('robots_index', $this->request->get('robots_index'));
        $this->sites->setConfig('robots_follow', $this->request->get('robots_follow'));
        $this->sites->setConfig('maintenance', $this->request->get('maintenance', 0));
        $this->sites->setConfig('maintenance_message', $this->request->get('maintenance_message'));
        $this->sites->setConfig('maintenance_allow_admin', $this->request->get('maintenance_allow_admin', 0));
        $this->sites->commitSettings();

        $this->request->session()->flash('success', "Les modifications ont bien été enregistrées");
        return redirect()->route('admin.settings.edit');
    }

    public function editRouting()
    {
        $routes = Resource::currentSite()->with('resource')->orderBy(\DB::raw('LENGTH(route)'))->paginate(20);
        $duplicates = Resource::getDuplicateRoutes();
        $nbDuplicates = count($duplicates);
        if ($nbDuplicates > 0) {
            $this->request->session()->flash('warning', "<b>ATTENTION !!</b> Il y a " . $nbDuplicates . ' ' . Str::plural('route', $nbDuplicates) . " en doublon. Ces routes peuvent rentrer en conflit et perturber le routage.");
        }
        return view('topaz::settings.routing', compact('routes', 'duplicates'));
    }

    public function routerForm()
    {
        $params = $this->request->all();
        $errors = [];

        $res = Resource::findOrNew($params['obj_id']);
        $res->route = ($params['route']);
        $res->type = $params['type'];
        if ($res->type == 'resource') {
            $resource = explode('|', $params['resource']);
            $res->resource_type = $resource[0];
            $res->resource_id = $resource[1];
        } else {
            $res->parameter = $params['parameter'];
        }

        if (empty($this->request->get('password', '')))
        {
            $res->password = null;
        } else {
            $res->password = bcrypt($this->request->get('password'));
        }

        $success = $res->save();

        return json_encode(compact('success','errors'));
    }

    public function routerGet()
    {
        return Resource::findOrFail($this->request->get('id'))->toJson();
    }

    public function routerDelete()
    {
        return json_encode(['success' => Resource::destroy($this->request->get('id'))]);
    }

    public function routerActive($route_id)
    {
        $res = Resource::findOrFail($route_id);
        $res->active = !$res->active;
        $res->save();
        return redirect()->route('admin.settings.routing');
    }

    public function editNavigation($menu = null)
    {
        global $app;
        $menu_name = ($menu === null) ? array_keys(config('topaz.menus'))[0] : $menu;
        $menu_title = config('topaz.menus')[$menu_name];
        $items = Menu::currentSite()->with('route')->orderBy('order')->where('parent_id', '0')->where('menu', $menu_name)->get();

        $items_list = [0 => 'Racine du menu'];
        foreach ($items as $item)
            $items_list[$item->id] = $item->title;

        $routes = [
            '0' => '|circle-o|Aucune Action|'
        ];
        foreach (Resource::currentSite()->with('resource')->orderBy(\DB::raw('LENGTH(route)'))->get() as $route) {
            $type = $app['topaz']->getRouteType($route->type);
            $route_item = $route->route . '|' . $type['icon'] . '|' . $type['title'] . '|';
            if ($type['name'] == 'resource')
                $route_item .= "<i class='{$route->resource->getResourceIcon()}'></i> {$route->resource->getResourceTitle()} / <span class='text-bold'>{$route->resource}</span>";
            else
                $route_item .= $route->parameter;
            $routes[$route->id] = $route_item;
        }
        return view('topaz::settings.navigation', compact('items', 'menu_name', 'menu_title', 'routes', 'items_list'));
    }

    public function navigationForm()
    {
        $params = $this->request->all();
        $errors = [];

        $menu = Menu::findOrNew($params['obj_id']);

        $menu->menu = $params['menu'];
        $menu->title = $params['title'];
        $menu->route_id = $params['route_id'];
        if (!$menu->exists || $menu->id != $params['parent_id'])
            $menu->parent_id = $params['parent_id'];
//        $menu->extra = $this->request->except(['title', 'route_id', 'parent_id', '_token', 'obj_id']);
        $menu->extra = '';
        $menu->site_id = $this->sites->getCurrentSiteId();

        $success = $menu->save();

        return json_encode(compact('success','errors'));
    }

    public function navigationGet()
    {
        return Menu::findOrFail($this->request->get('id'))->toJson();
    }

    public function navigationMove()
    {
        $menu = Menu::findOrFail($this->request->get('id'));
        $dir = $this->request->get('dir', '');
        $order = $menu->order;
        if ($dir == 'down') {
            $moved_menu = Menu::where('menu', $menu->menu)->orderBy('order', 'asc')->where('order', '>', $order)->first();
        } else {
            $moved_menu = Menu::where('menu', $menu->menu)->orderBy('order', 'desc')->where('order', '<', $order)->first();
        }

        $moved_menu_order = $moved_menu->order;
        $menu->order = $moved_menu_order;
        $moved_menu->order = $order;

        $menu->save();
        $moved_menu->save();

        return '1';
    }

    public function navigationDelete()
    {
        return json_encode(['success' => Menu::destroy($this->request->get('id'))]);
    }

}
