<div class="form-group {{ $errors->has($field->name) ? 'has-error' : '' }}">
    <div class="bs-component">
        <input type="hidden" id="{{ $field->name }}" name="{{ $field->name }}" value="{{ $object->{$field->name} ? '1' : '0' }}">

        <a href="javascript:void(0)" class="btn btn-default btn-block text-left" id="{{ $field->name }}_button">
            <i class="fa fa-square-o"></i> {!! $field->label !!}
        </a>

        @if ($errors->has($field->name))
            @foreach ($errors->get($field->name) as $error)
                <span class="help-block">
                    <i class="fa fa-exclamation-circle"></i> {{ $error }}
                </span>
            @endforeach
        @else
            @if (!empty($field->help))
                <span class="help-block">
                    <i class="fa fa-question-circle"></i> {!! $field->help !!}
                </span>
            @endif
        @endif
    </div>
</div>