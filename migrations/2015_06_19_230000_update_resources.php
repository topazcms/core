<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateResources extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('topaz_router', function(Blueprint $table)
		{
			$table->string('type')->default('resource');
			$table->string('parameter')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('topaz_router', function(Blueprint $table)
        {
            $table->dropColumn('type');
        });
	}

}
