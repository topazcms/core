<script>
    $(document).ready(function() {
        // CKEDITOR.config.defaultLanguage = 'fr';
        CKEDITOR.replace('{{ $id }}', {
            height: '{{ $height or 400 }}',
            toolbar: [
                [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
                '/',
                [ 'Format','Font','FontSize' ],
                [ 'Bold', 'Italic', 'Underline', 'Strike' ],
                [ 'NumberedList','BulletedList','-','Outdent','Indent' ],
                [ 'Link','Unlink','Anchor' ],
                [ 'Image' ],
                [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ]
            ],
            on: {
                instanceReady: function(evt) {
                    $('.cke').addClass('admin-skin cke-hide-bottom');
                }
            }
        });
    });
</script>