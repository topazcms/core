<?php namespace Topaz\Core\Commands;

use Artesaos\Defender\Defender;
use Fenos\Notifynder\Contracts\NotifynderCategory;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Topaz\Core\Models\User;
use Topaz\Core\Services\TopazPermissions;
use Topaz\Core\Services\TopazService;

class TopazSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'topaz:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup Topaz';

    /** @var  TopazService */
    protected $topaz;

    /** @var Defender $defender */
    protected $defender;

    /** @var TopazPermissions $permissions */
    protected $permissions;

    /** @var  ProgressBar */
    protected $bar;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->topaz = app('topaz');
        $this->defender = app('defender');
        $this->permissions = $this->topaz->permissions();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $needed_permissions = $this->permissions->neededPermissions();
        $needed_roles = $this->permissions->neededRoles();
        $this->bar = $this->output->createProgressBar($needed_roles->count() + $needed_permissions->count());
        $this->bar->setBarCharacter('<fg=white;bg=green> </>');
        $this->bar->setEmptyBarCharacter(' ');
        $this->bar->setProgressCharacter('<fg=white;bg=yellow> </>');
        $this->bar->setFormat(" %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%");

//        $superadminRole = $this->createRole('superadmin');
//        $adminRole = $this->createRole('admin');
//        $redactorRole = $this->createRole('redactor');

//        $this->createPermission('pages.manage', "Gérer les pages", [$superadminRole, $adminRole, $redactorRole]);
//        $this->createPermission('settings.manage', "Gérer les réglages", [$superadminRole, $adminRole]);
//        $this->createPermission('settings.router', "Gérer le routage", [$superadminRole, $adminRole]);
//        $this->createPermission('settings.navigation', "Gérer la navigation", [$superadminRole, $adminRole]);
//        $this->createPermission('settings.users', "Gérer les utilisateurs", [$superadminRole, $adminRole]);
//        $this->createPermission('settings.sites', "Gérer les sites", [$superadminRole, $adminRole]);

        foreach ($needed_roles as $role_name) {
            $this->createRole($role_name);
        }

        foreach ($needed_permissions as $permission) {
            $this->createPermission($permission->name);
        }

        $this->createNotification('topaz.test', "Ceci est un test pour {from.display_name} !");

        $this->bar->finish();
    }

    protected function createRole($name)
    {
        $this->bar->advance();
        $this->bar->setMessage("Created $name role");
        $this->permissions->createNeededRole($name);
    }

    protected function createPermission($name)
    {
        $this->bar->advance();
        $this->bar->setMessage("Created $name permission");
        $this->permissions->createNeededPermission($name);
    }

    protected function createNotification($name, $text)
    {
        $this->bar->advance();
        $this->bar->setMessage("Created $name notification");
        if ($this->topaz->createNotificationType($name, $text)) {
//            $this->info("Created $name notification");
        } else {
//            $this->comment("Notification $name already exists");
        }
    }

}