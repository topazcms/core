<div id="{{ $modal_id }}" class="popup-basic horizontal-form mfp-with-anim mfp-hide" style="max-width: 700px">
    <div class="panel">
        <div class="panel-heading pb0">
            <span class="panel-title">
                <i class="fa fa-{{ $icon }}"></i> {{ $title }}
            </span>
        </div>
        <!-- end .panel-heading section -->
        <div class="panel-body pn">

            @if ($show_name)
                <div class="p25">
                    @if ($show_name)
                        <input class="form-control" type="text" placeholder="{{ $show_name }}" id="name_{{ $modal_id }}">
                    @endif
                </div>
                <hr class="mn">
            @endif

           <div class="p25">
               <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-filter"></i>
                </span>
                   <input class="form-control" type="text" placeholder="Filter parmi les fichiers..." id="filter_{{ $modal_id }}">
               </div>

               <table class="table">
                   <thead>
                   <tr>
                       <th>Nom</th>
                       <th>Type</th>
                       <th>Date de modification</th>
                   </tr>
                   </thead>
                   <tbody id="body_{{ $modal_id }}">

                   </tbody>
               </table>

               <div class="text-center mt10">
                   <button type="button" class="btn btn-default btn-sm" id="next_{{ $modal_id }}">

                   </button>
               </div>
           </div>

        </div>
        <!-- end .form-footer section -->
    </div>
    <!-- end: .panel -->
</div>
<!-- end: .admin-form -->

<script>
    $(document).ready(function() {

        var openModal = $('#{{ $button_id }}');
        openModal.attr('data-effect', "mfp-zoomIn");

        openModal.on('click', function() {
            var btn = $(this);

            $.magnificPopup.open({
                removalDelay: 500, //delay removal by X to allow out-animation,
                items: {
                    src: '#{{ $modal_id }}'
                },
                // overflowY: 'hidden', //
                callbacks: {
                    beforeOpen: function(e) {
                        var Animation = btn.attr('data-effect');
                        this.st.mainClass = Animation;
                    }
                },
                midClick: true,
                alignTop: true
            });

            fetch(true);
        });

        var page = 1;

        function fetch(replace)
        {
            var btn = $('#next_{{ $modal_id }}');
            var body = $('#body_{{ $modal_id }}');

            btn.prop('disabled', true);
            btn.html('<i class="fa fa-pulse fa-spinner"></i> Chargement...');

            if (replace) {
                page = 1;
                body.empty();
            }

            $.get('{{ route('admin.medias.api.list') }}', {
                type: '{{ $type }}',
                page: page,
                filter: $('#filter_{{ $modal_id }}').val()
            }, function (json) {

                if (replace) {
                    body.empty();
                }

                $.each(json.data, function (k, v) {
                   var tr = $('<tr>');
                    tr.append($('<td>').html('<a href="javascript: void()" rel="insert" data-url="' + v.inline_url + '" data-name="' + v.name + '" data-id="' + v.id + '"><i class="fa fa-' + v.type_icon + '"></i> ' + v.name + '</a>'));
                    tr.append($('<td>').html(v.type_title));
                    tr.append($('<td>').html(v.updated_at));

                    body.append(tr);
                });

                btn.prop('disabled', false);
                btn.html('<i class="fa fa-arrow-down"></i> Charger plus de résultats');
                page++;
            });
        }

        $(document).on('click', '#next_{{ $modal_id }}', function() {
            fetch(false);
        });

        $(document).on('keyup', '#filter_{{ $modal_id }}', function() {
            fetch(true);
        });

        $(document).on('click', '#body_{{ $modal_id }} a[rel=insert]', function() {
            var obj = $(this);
            var url = obj.data('url');
            var name = obj.data('name');
            var id = obj.data('id');

            var nameField = $('#name_{{ $modal_id }}');
            if (nameField.length > 0 && nameField.val().trim() != "")
            {
                name = nameField.val();
            }

            $('#{{ $button_id }}').trigger('picked', [url, name, id]);
            $.magnificPopup.close();
        });

    });
</script>