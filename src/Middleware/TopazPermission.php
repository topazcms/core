<?php

namespace Topaz\Core\Middleware;

use Closure;
use Illuminate\Auth\Guard;
use Topaz\Core\Models\User;

class TopazPermission
{
    protected $auth;
    protected $defender;

    public function __construct()
    {
        $this->auth = app('auth')->admin();
        $this->defender = app('defender');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission = null)
    {
        /** @var User $user */
        $user = $this->auth->get();
        if ($this->auth->guest() || !$user->can($permission))
        {
            if ($request->ajax())
            {
                return response('Unauthorized.', 403);
            }
            else
            {
                abort(403, "Vous n'avez pas l'autorisation d'accéder à cette ressource");
            }
        }

        return $next($request);
    }
}
