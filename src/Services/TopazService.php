<?php namespace Topaz\Core\Services;

use Artesaos\Defender\Defender;
use Carbon\Carbon;
use Fenos\Notifynder\Contracts\NotifynderCategory;
use Fenos\Notifynder\Exceptions\CategoryNotFoundException;
use Fenos\Notifynder\Models\Notification;
use Fenos\Notifynder\Notifynder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Topaz\Core\Models\Menu;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Topaz\Core\Models\Resource;
use Topaz\Core\Models\Site;
use Topaz\Core\Models\User;

class TopazService {

    use SEOToolsTrait;

    protected $menu_items = [];
    protected $routes_types = [];
    protected $resources_types = [];
    protected $notifications_icons = [];

    protected $page_title = null;

    protected $url_cache = [];

    public static $domain_tag = '<%__SITE_DOMAIN__%>';

    protected $html_filters;

    public function __construct()
    {
        $this->html_filters = new Collection;
    }

    /**
     * @return SitesManager
     */
    public function sites()
    {
        return app('topaz.sites');
    }

    /**
     * @return TopazPermissions
     */
    public function permissions()
    {
        return app('topaz.permissions');
    }

    /**
     * @return TopazRoutingService
     */
    public function routing()
    {
        return app('topaz.routing');
    }

    public function getConfig($key, $site_id = null)
    {
        $this->sites()->getConfig($key, $site_id);
    }

    public function setConfig($key, $value, $site_id = null)
    {
        $this->sites()->setConfig($key, $value, $site_id);
    }

    public function registerAdminMenuItem($item)
    {
        $this->menu_items[] = $item;
    }

    public function registerAdminMenuSingleItem($icon, $title, $order, $active, $route, $params = [])
    {
        $this->registerAdminMenuItem(compact('icon', 'title', 'order', 'active', 'route', 'params'));
    }

    public function getAdminMenuItems()
    {
        usort($this->menu_items, function($a, $b) {
            if (!array_key_exists('order', $a))
                $a['order'] = 50;
            if (!array_key_exists('order', $b))
                $b['order'] = 50;
            return $a['order'] - $b['order'];
        });

        return $this->menu_items;
    }

    public function registerRouteType($group, $groupIcon, $name, $title, $icon, $service_name = null, $method = null)
    {
        $this->routes_types[] = compact('group', 'groupIcon', 'name', 'title', 'icon', 'service_name', 'method');
    }

    public function getRouteTypes()
    {
        return $this->routes_types;
    }

    public function getRoutesTypesForList()
    {
        $ind_types = [];
        $group_types = [];

        foreach ($this->routes_types as $type) {
            if ($type['group'] === null) {
                $ind_types[$type['name']] = $type['icon'] . '|' . $type['title'];
            } else {
                if (!array_key_exists($type['groupIcon'] . '|' . $type['group'], $group_types))
                    $group_types[$type['groupIcon'] . '|' . $type['group']] = [];
                $group_types[$type['groupIcon'] . '|' . $type['group']][$type['name']] = $type['icon'] . '|' . $type['title'];
            }
        }

        return array_merge($ind_types, $group_types);
    }

    public function getRouteType($name)
    {
        foreach ($this->routes_types as $type)
            if ($type['name'] == $name)
                return $type;
        return null;
    }

    public function registerResourceType($name)
    {
        $this->resources_types[] = $name;
    }

    public function getResourcesTypes()
    {
        return $this->resources_types;
    }

    public function getResourcesTypesForList()
    {
        $group_types = [];

        foreach ($this->resources_types as $type) {
            $obj = new $type;
            $groupTitle = $obj->getResourceIcon() . '|' . $obj->getResourceTitle();
            if (!array_key_exists($groupTitle, $group_types))
                $group_types[$groupTitle] = [];
            foreach ($obj->getResources() as $resource)
                $group_types[$groupTitle][$type . '|' . $resource->id] = $resource->getResourceIcon() . '|' . $resource->__toString();
        }

        return $group_types;
    }

    public function getMenu($menu_name)
    {
        return Menu::with('route', 'children')
            ->currentSite()
            ->where('menu', $menu_name)
            ->where('parent_id', 0)
            ->orderBy('order')->get();
    }

    /**
     * @return null
     */
    public function getPageTitle()
    {
        return $this->page_title;
    }

    /**
     * @param null $page_title
     */
    public function setPageTitle($page_title)
    {
        $this->page_title = $page_title;
        $this->seo()->setTitle($this->page_title . ' - ' . $this->sites()->getConfig('site_name'));
    }

    public function parse_domain($text)
    {
        return str_replace(url('/'), self::$domain_tag, $text);
    }

    public function unparse_domain($text)
    {
        return str_replace(self::$domain_tag, url('/'), $text);
    }

    public function getSeo()
    {
        return $this->seo();
    }

    public function url_cache($model, $id)
    {
        $key = $model . '::' . $id;
        if (array_key_exists($key, $this->url_cache)) {
            return $this->url_cache[$key];
        }

        $route = Resource::where('resource_type', $model)
            ->where('resource_id', $id)
            ->active()
            ->orderBy('created_at')
            ->first();

        if ($route === null) {
            $url = url('/');
        } else {
            $url = $route->url;
        }

        $this->url_cache[$key] = $url;
        return $url;
    }

    public function registerRole($name)
    {
        /** @var Defender $defender */
        $defender = app('defender');
        $defender->createRole($name);
    }

    public function registerPermission($name, $description = null)
    {
        /** @var Defender $defender */
        $defender = app('defender');
        $defender->createPermission($name, $description);
    }

    public function createNotificationType($name, $text)
    {
        /** @var NotifynderCategory $notifynder */
        $notifynder = app('notifynder.category');

        try {
            $notifynder->findByName($name);
        } catch (CategoryNotFoundException $e) {
            $notifynder->add($name, $text);
            return true;
        }

        return false;
    }

    public function notify($category, $from, $to, $url, $extra = [])
    {
        if ($from == 'me') {
            $from = app('auth')->admin()->get()->id;
        }
        if ($from instanceof User) {
            $from = $from->id;
        }
        if ($to instanceof User) {
            $to = $to->id;
        }
        if ($to == 'me') {
            $to = app('auth')->admin()->get()->id;
        }
        /** @var Notifynder $notifynder */
        $notifynder = app('notifynder');
        $notifynder->category($category)
                    ->from($from)
                    ->to($to)
                    ->url($url)
                    ->extra($extra)
                    ->send();
    }

    public function registerNotificationType($category, $icon, $color = 'dark')
    {
        return $this->notifications_icons[$category] = compact('icon', 'color');
    }

    public function getNotificationType($category)
    {
        if ($category instanceof Notification) {
            /** @var NotifynderCategory $categories */
            $categories = app('notifynder.category');

            $category = $categories->find($category->category_id)->name;
        }
        return $this->notifications_icons[$category];
    }

    public function getNotificationIcon($category)
    {
        return $this->getNotificationType($category)['icon'];
    }

    public function getNotificationColor($category)
    {
        return $this->getNotificationType($category)['color'];
    }

    /**
     * @return Notifynder
     */
    public function notifynder()
    {
        return app('notifynder');
    }

    public function getNotifications()
    {
        $user = app('auth')->admin()->get();
        $unread_notifications = count($this->notifynder()->getNotRead($user->id));

        $limit = max(10, $unread_notifications);

        $other_notifications = $this->notifynder()->getAll($user->id, $limit);

        return (object)compact('unread_notifications', 'other_notifications');
    }

    public function diffNow(Carbon $date)
    {
        return $date->diffForHumans(Carbon::now());
    }

    public function initSeo()
    {
        /** @var SitesManager $sites */
        $sites = app('topaz.sites');
        $title = $sites->getConfig('site_name');
        $description = $sites->getConfig('site_description');
        $robots = $sites->getConfig('robots_index') . ',' . $sites->getConfig('robots_follow');
        $this->seo()->setTitle($title);
        $this->seo()->setDescription($description);
        $this->seo()->metatags()->setKeywords(array_map(function($val) { return trim($val); }, explode(',', $sites->getConfig('site_keywords'))));
        $this->seo()->metatags()->addMeta('robots', $robots);
        $this->seo()->opengraph()->setTitle($title);
        $this->seo()->opengraph()->setDescription($description);
        $this->seo()->opengraph()->setUrl(\URL::full());
        $this->seo()->opengraph()->addProperty('type', 'website');
    }

    public function currentLocale()
    {
        return config('app.locale');
    }

    public function allLanguages()
    {
        return config('topaz_languages');
    }

    public function registerHtmlFilter(\Closure $parse, \Closure $unparse)
    {
        $this->html_filters->push(compact('parse', 'unparse'));
    }

    public function parseHtml($html)
    {
        foreach ($this->html_filters as $filter) {
            $html = $filter['parse']($html);
        }

        return $html;
    }
    public function unparseHtml($html)
    {
        foreach ($this->html_filters as $filter) {
            $html = $filter['unparse']($html);
        }

        return $html;
    }

} 