<?php


namespace Topaz\Core\CrudForm;


interface IField
{
    public function render();
}