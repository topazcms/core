<?php namespace Topaz\Core\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Media;
use Topaz\Core\Models\Page;

class GlobalMediaController extends Controller {

    public static function routes()
    {
        Route::get('/download/{filename}', ['as' => 'download', 'uses' => __CLASS__.'@download']);
        Route::get('/image/{filename}', ['as' => 'image', 'uses' => __CLASS__.'@image']);
        Route::get('/{filename}', ['as' => 'inline', 'uses' => __CLASS__.'@inline']);
    }

    public function download($filename)
    {
        return response()->download(storage_path(Media::$path.$filename));
    }

    public function inline($filename)
    {
        $path = storage_path(Media::$path.$filename);
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $path);
        return response()->make(file_get_contents($path), 200, [
            'Content-Type' => $mime,
            'Content-Disposition' => 'inline;' . $filename,
        ]);
    }

    public function image($filename, Request $req)
    {
        $location = storage_path(Media::$path.$filename);
        $img = \Image::make($location);

        if ($req->has('fit') && str_contains($req->get('fit'), 'x')) {
            $parts = explode('x', $req->get('fit'));
            $w = $parts[0];
            $h = $parts[1];
            $img->fit($w, $h);
        }

        return $img->response();
    }

}
