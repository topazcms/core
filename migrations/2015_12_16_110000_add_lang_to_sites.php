<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLangToSites extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('topaz_sites', function(Blueprint $table)
		{
			$table->string('lang')->default(config('app.locale'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('topaz_sites', function(Blueprint $table)
        {
            $table->dropColumn('lang');
        });
	}

}
