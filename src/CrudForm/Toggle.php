<?php


namespace Topaz\Core\CrudForm;


class Toggle extends Field
{
    public function __construct($name, $label = null, $help = null)
    {
        parent::__construct($name, $label, $help);
    }
}