@extends('topaz::crud.form.form_group')

@section($field->name . '__field')
    <div id="{{ $field->name }}_container"></div>
@endsection