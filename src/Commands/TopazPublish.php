<?php

namespace Topaz\Core\Commands;

use Artesaos\Defender\Defender;
use Illuminate\Console\Command;
use Topaz\Core\Models\User;
use Topaz\Core\TopazServiceProvider;

class TopazPublish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'topaz:publish {tag?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish from TopazCore';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $args = [
            '--provider' => TopazServiceProvider::class,
        ];

        if ($this->argument('tag') !== null) {
            $args = array_merge($args, ['--tag' => $this->argument('tag')]);
        }

        \Artisan::call('vendor:publish', $args);
    }
}
