@extends('topaz::app')

@section('title', $title)
@section('page_title', $title)

@section('breadcrumb')
    @foreach ($breadcrumb as $item)
        @include('topaz::breadcrumb_item', $item)
    @endforeach
    {{--@include('topaz::breadcrumb_item', ['title' => 'Accueil', 'url' => route('admin.index')])--}}
@endsection

@section('topbar_right')
    <ul class="nav nav-list nav-list-topbar pull-left mrn">
        <li>
            <a href="{{ route($route_prefix . '.index') }}">{{ ucwords($object_name_plural) }} {{ $object_name_male ? 'actifs' : 'actives' }}</a>
        </li>
        <li class="active">
            <a href="{{ route($route_prefix . '.index', ['trashed' => 1]) }}"><i class="fa fa-trash-o"></i> Corbeille</a>
        </li>
    </ul>
    <div class="pull-left mt10 mr15 ml15 text-primary" style="font-size:0.5em">
        |
    </div>
    <div class="btn-group pull-left">
        <a class="btn btn-sm btn-primary text-bold" href="{{ route('admin.pages.add') }}">
            <i class="fa fa-plus"></i>
            @if ($new_button === null)
                {{ $object_name_male ? 'Nouveau' : 'Nouvelle' }} {{ $object_name_singular }}
            @else
                {{ $new_button }}
            @endif
        </a>
    </div>
@append

@section('breadcrumb_tail', 'Pages')

@section('body')
    <div class="panel panel-primary panel-border top">
        <div class="panel-body pn">

            <div class="table-responsive">
                <table class="table table-hovered">
                    <thead>
                    <tr>
                        @foreach (array_keys($table) as $header)
                            <th>{{ $header }}</th>
                        @endforeach
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($objects as $object)
                        <tr>
                            @foreach ($table as $header => $prop)
                                <td>
                                    @if ($prop == '__toString')
                                        {!! (string)$object !!}
                                    @elseif (ends_with($prop, '()'))
                                        {!! (string)$object->$prop() !!}
                                    @else
                                        {!! (string)$object->$prop !!}
                                    @endif
                                </td>
                            @endforeach
                            <td style="width: 200px;">
                                <a href="{{ route($route_prefix . '.restore', $object) }}" class="btn btn-xs btn-success dark mb5">
                                    <i class="fa fa-reply"></i> Restaurer
                                </a>
                                <br>
                                <a href="{{ route($route_prefix . '.delete', $object) }}" class="btn btn-xs btn-danger">
                                    <i class="fa fa-times"></i> Supprimer définitivement
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="10" class=" text-center">
                                <div class="lead text-muted mt15">
                                    {{ $object_name_male ? 'Aucun' : 'Aucune' }} {{ $object_name_singular }} {{ $object_name_male ? 'supprimé' : 'supprimée' }}
                                </div>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>

            <div class="text-center">
                {!! $objects->render() !!}
            </div>

        </div>
    </div>
@stop