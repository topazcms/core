<?php namespace Topaz\Core\Services;

use Artesaos\Defender\Defender;
use Carbon\Carbon;
use Fenos\Notifynder\Contracts\NotifynderCategory;
use Fenos\Notifynder\Exceptions\CategoryNotFoundException;
use Fenos\Notifynder\Models\Notification;
use Fenos\Notifynder\Notifynder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Response;
use Topaz\Core\Models\Menu;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Topaz\Core\Models\Resource;
use Topaz\Core\Models\Site;
use Topaz\Core\Models\User;

class TopazRoutingService {

    /** @var TopazService */
    protected $topaz;

    /** @var SitesManager */
    protected $sites;

    /** @var TopazBreadcrumb */
    protected $breadcrumb;

    /**
     * @var RouteContext
     */
    protected $context = null;

    public function __construct()
    {
        $this->topaz = app('topaz');
        $this->sites = app('topaz.sites');
        $this->breadcrumb = app('topaz.breadcrumb');
    }

    public function dispatch(Request $request, $uri, $site_id = null)
    {
        if ($site_id === null) {
            $site_id = $this->sites->getCurrentSiteId();
        }

        $this->sites->setCurrentSiteId($site_id);

        $this->topaz->initSeo();

        $route_query = $this->newRouteQuery();

        $route_parameters = collect();

        // First : try to find a direct route
        if (empty($uri) || $uri == '/') {
            $route_query->where(function ($q) {
                $q->orWhere('route', '')->orWhere('route', '/');
            });
        } else {
            //preg_match_all("/{(.*)}/iUs", $uri, $matches)
            //$route_query->whereRaw("route REGEXP '^{$uri}$'"); // (-[0-9]*)?
            $route_query->where('route', $uri)->where('route', 'NOT LIKE', '%{%')->where('route', 'NOT LIKE', '%}%');
        }

        $route = $route_query->orderBy('updated_at', 'desc')->first();

        // Second : try to detect site using prefix
        if ($route === null) {
            $parts = explode('/', $uri);
            $prefix = array_shift($parts);
            $new_uri = implode('/', $parts);

            $site = $this->sites->getSiteOfPrefix($prefix);

            if ($site !== null) {
                return $this->dispatch($request, $new_uri, $site->id);
            }
        }

        // Third : try to find routes matching with params
        if ($route === null) {
            $routes = $this->newRouteQuery()->where('route', 'LIKE', '%{%')->where('route', 'LIKE', '%}%')->get();
            $param_tag = '%%' . str_random(6) . '%%';
            foreach ($routes as $param_route) {
                $regex_route = preg_replace("~\{(.*?)\}~s", $param_tag, $param_route->route);
                $regex_route = preg_quote($regex_route);
                $regex_route = str_replace('/', '\/', $regex_route);
                $regex_route = str_replace($param_tag, '(.*?)', $regex_route);
                $regex_route = '/^' . $regex_route . '$/';

                $matching = preg_match_all($regex_route, $uri, $matches);

                if ($matching) {
                    $route = $param_route;
                    preg_match_all($regex_route, $param_route->route, $uri_matches);
                    $combined = array_combine(
                        array_map(function($val) { return $val[0]; }, $uri_matches),
                        array_map(function($val) { return $val[0]; }, $matches)
                    );
                    unset($combined[$param_route->route]);
                    foreach ($combined as $key => $val) {
                        $new_key = substr($key, 1, -1);
                        $combined[$new_key] = $val;
                        unset($combined[$key]);
                    }
                    $route_parameters = collect($combined);
                }
            }
        }

        // Fourth : Nothing has been found, throw 404
        if ($route === null) {
            $maintenance = $this->testMaintenance();
            if ($maintenance !== null) {
                return $maintenance;
            }

            abort(404);
        }

        $current_site = $this->sites->getCurrentSite();

        app()->setLocale($current_site->lang);

        $this->context = new RouteContext($request, $route, $route_parameters);

        // Check maintenance
        $maintenance = $this->testMaintenance();
        if ($maintenance !== null) {
            return $maintenance;
        }

        // Check password
        if ($route->password !== null)
        {
            if ($request->has('revoke'))
            {
                $request->session()->forget('topaz_page_password.' . $route);
                return redirect(url($uri));
            }

            if ($request->method() == 'POST')
            {
                if (\Hash::check($request->get('password'), $route->password))
                {
                    $request->session()->set('topaz_page_password.' . $route, $route->password);
                }
                else
                {
                    return view('page_password', ['bad' => true]);
                }
            }
            else
            {
                if ($request->session()->get('topaz_page_password.' . $route, null) != $route->password)
                    return view('page_password');
            }
        }

        $request->session()->flash('is_protected', $route->password !== null);

        if ($route->type == 'resource')
        {
            $entity = $route->resource;
            if ($entity === null)
            {
                abort(404);
            }
            $content = $entity->getResourceResponse($this->context);
            $this->breadcrumb->generateAtLeastOneNode();
            return response($content, 200, [
                'Content-Language' => $current_site->lang
            ]);
        }
        elseif ($route->type == 'redirection')
        {
            return redirect()->to($route->parameter);
        }
        elseif ($route->type == 'mirror')
        {
            return $this->dispatch($request, $route->parameter, $route->site_id);
        }
        else
        {
            foreach ($this->topaz->getRouteTypes() as $route_type)
            {
                if ($route_type['name'] == $route->type) {
                    $service = app($route_type['service_name']);
                    if (method_exists($service, $route_type['method'])) {
                        $content = $service->{$route_type['method']}($this->context);
                        $this->breadcrumb->generateAtLeastOneNode();
                        return response($content, 200, [
                            'Content-Language' => $current_site->lang
                        ]);
                    }
                }
            }
        }

        $maintenance = $this->testMaintenance();
        if ($maintenance !== null) {
            return $maintenance;
        }

        abort(404);
        return response('Not Found', 404);
    }

    public function testMaintenance()
    {
        if ($this->sites->isMaintenance()) {
            if (app('auth')->admin()->check() && $this->sites->getConfig('maintenance_allow_admin')) {
                return null;
            }
            $view = $this->sites->getCurrentThemeView('_maintenance');
            if (!view()->exists($view)) {
                $view = $this->sites->getCurrentThemeView('maintenance');
                if (!view()->exists($view)) {
                    abort(503);
                }
            }
            $message = $this->sites->getConfig('maintenance_message');
            if (empty($message)) {
                $message = null;
            }
            return response()->view($view, ['message' => $message], 503);
        }

        return null;
    }

    /**
     * @return \Illuminate\Database\Query\Builder|Resource
     */
    protected function newRouteQuery()
    {
        return Resource::active()->currentSite();
    }

    /**
     * @return RouteContext|null
     */
    public function getContext()
    {
        return $this->context;
    }

} 