<?php namespace Topaz\Core\Controllers;

use Illuminate\Http\Request;
use Topaz\Core\Models\Resource;
use Topaz\Core\Services\TopazRoutingService;
use Topaz\Core\Services\TopazService;

class EntrypointController extends Controller {

    public function entrypoint($uri = "", TopazRoutingService $routing)
    {
        return $routing->dispatch($this->request, $uri);
    }

    public function sitemap()
    {
        $response = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $urls = [];

        $home = Resource::where('route', '/')->first();
        if ($home !== null)
        {
            $url = url('/');
            $urls[] = $url;
            $response .= '<url>';
            $response .= "<loc>$url</loc>";
            if ($home->type == 'resource')
            {
                if ($home->resource->updated_at !== null) {
                    $response .= "<lastmod>{$home->resource->updated_at->format('Y-m-d')}</lastmod>";
                }
            }
            $response .= '<priority>1</priority>';
            $response .= '</url>';
        }

        foreach (array_keys(config('topaz.menus')) as $menu_name)
        {
            $menu = $this->topaz->getMenu($menu_name);
            foreach ($menu as $item)
            {
                if ($item->route == null) {
                    continue;
                }
                $url = $item->route->url;
                if (empty($url) || in_array($url, $urls)) {
                    continue;
                }
                $urls[] = $url;

                $response .= '<url>';
                $response .= "<loc>$url</loc>";
                if ($item->route->type == 'resource')
                {
                    if ($item->route->resource->updated_at !== null) {
                        $response .= "<lastmod>{$item->route->resource->updated_at->format('Y-m-d')}</lastmod>";
                    }
                }
                $response .= '</url>';
            }
        }

        $response .= '</urlset>';

        return response($response)->header('Content-Type', 'text/xml');
    }

}
