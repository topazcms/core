@extends('topaz::crud.form.form_group')

@section($field->name . '__field')
    {!! Form::select($field->name,
        $field->getExtra('items'),
        old($field->name, $object->{$field->name}),
        ['class' => $field->getExtra('select2-class') . ' form-control']
    ) !!}
@endsection