<?php


namespace Topaz\Core\Services;


use Illuminate\Support\Collection;
use Topaz\Core\Models\ResourceableModel;

class TopazBreadcrumb
{
    /** @var Collection */
    protected $breadcrumb;

    public function __construct()
    {
        $this->breadcrumb = new Collection;
    }

    /**
     * @param string|ResourceableModel|Collection|array $name
     * @param string|null $url
     */
    public function append($name, $url = null)
    {
        if ($name instanceof Collection || is_array($name)) {
            foreach ($name as $elem) {
                $this->append($elem);
            }

            return $this;
        }

        if ($name instanceof ResourceableModel) {
            $url = $name->url;
            $name = $name->__toString();
        }

        $this->breadcrumb->push((object)compact('name', 'url'));
        return $this;
    }

    public function generateAtLeastOneNode()
    {
        if (!$this->breadcrumb->isEmpty()) {
            return;
        }

        $title = app('topaz')->getPageTitle();

        $this->append($title, \URL::full());
    }

    /**
     * @return Collection
     */
    public function getBreadcrumb()
    {
        if (!$this->breadcrumb->isEmpty()) {
            $this->breadcrumb->map(function ($obj) {
                $obj->active = false;
            });
            $this->breadcrumb->last()->active = true;
        }

        return $this->breadcrumb;
    }


}