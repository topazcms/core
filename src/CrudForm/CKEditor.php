<?php


namespace Topaz\Core\CrudForm;


class CKEditor extends Field
{
    public function __construct($name, $label, $help = null)
    {
        parent::__construct($name, $label, $help);

        $this->needStyle('topaz/admin_assets/vendor/plugins/magnific/magnific-popup.css');
        $this->needScript('topaz/admin_assets/vendor/plugins/magnific/jquery.magnific-popup.js');
        $this->needScript('topaz/admin_assets/vendor/plugins/ckeditor/ckeditor.js');
        $this->callBeforeJs('file_picker', [$name . '__pick_file', [
            'show_name' => "Texte à afficher"
        ]]);
        $this->callBeforeJs('image_picker', [$name . '__pick_image', [
            'show_name' => "Légende"
        ]]);
        $this->callBeforeJs('ckeditor', [$name]);
        $this->callBeforeJs('ckeditor_pickers', [$name, $name . '__pick_file', $name . '__pick_image']);
    }


}