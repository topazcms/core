<?php


namespace Topaz\Core\CrudForm;


abstract class Field
{
    public $name = null;
    public $label = null;
    public $placeholder = null;
    public $help = null;
    public $extras = [];

    public $view_name = null;
    public $call_before_js = [];
    public $needs_scripts = [];
    public $needs_styles = [];

    /**
     * Field constructor.
     * @param null $name
     * @param null $label
     * @param null $help
     */
    public function __construct($name, $label, $help = null)
    {
        $this->name = $name;
        $this->label = $label;
        $this->help = $help;
    }

    /**
     * @param null $name
     * @return Field
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param null $label
     * @return Field
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @param null $help
     * @return Field
     */
    public function setHelp($help)
    {
        $this->help = $help;
        return $this;
    }

    public function getViewName()
    {
        $refl = new \ReflectionClass(get_class($this));
        return strtolower($refl->getShortName());
    }

    /**
     * @param null $placeholder
     * @return Field
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return Field
     */
    public function addExtra($key, $value)
    {
        $this->extras[$key] = $value;
        return $this;
    }

    public function getExtra($key, $default = false)
    {
        if (array_key_exists($key, $this->extras)) {
            return $this->extras[$key];
        }

        return $default;
    }

    /**
     * @param string $function_name
     * @param array $args
     * @return $this
     */
    public function callBeforeJs($function_name, $args = [])
    {
        $this->call_before_js[$function_name] = $args;
        return $this;
    }

    /**
     * @param string $function_name
     * @param array $args
     * @return $this
     */
    public function needScript($script_location)
    {
        $this->needs_scripts[] = $script_location;
        return $this;
    }

    /**
     * @param string $function_name
     * @param array $args
     * @return $this
     */
    public function needStyle($style_location)
    {
        $this->needs_styles[] = $style_location;
        return $this;
    }


}