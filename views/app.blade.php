@extends('topaz::skeleton')
@inject('topaz', 'topaz')
@inject('sites', 'topaz.sites')

@section('body_class', 'sb-l-m')

@section('skeleton_body')
    <div id="main">
        <header class="navbar navbar-fixed-top bg-primary">
            <ul class="nav navbar-nav navbar-left">
                <li class="bg-primary dark mr10">
                    <a href="{{ url(config('topaz.admin_root')) }}" style="font-size:1.4em; font-weight: normal;">
                        <i class="fa fa-diamond"></i> <span style="font-weight: 700;">TOPAZ</span> {{ topaz_version() }} <span class="badge badge-primary">beta</span>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <li class="dropdown menu-merge">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="hidden-xs">
                        <i class="fa fa-globe ml5 mr5"></i>
                            <b>{{ $sites->getCurrentSite()->name }}</b>
                            <small style="opacity: 0.5;">{{ $sites->getConfig('site_name') }}</small>
                            <span class="caret caret-tp"></span>
                        </span>
                        <span class="visible-xs">
                            {{ $sites->getCurrentSite()->prefix }}
                            <span class="caret caret-tp"></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu pt5 pb5" role="menu">
                        @foreach ($sites->getSites() as $site)
                            <li @if ($site->id == $sites->getCurrentSiteId()) class="active" @endif>
                                <a href="{{ route('admin.set-site', $site->id) }}">
                                    <b>{{ $site->name }}</b>
                                    <br>
                                    {{ $sites->getConfig('site_name', $site->id) }}
                                </a>
                            </li>
                        @endforeach
                        @permission('sites.manage')
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('admin.settings.sites.index') }}">
                                <i class="fa fa-globe mr5"></i> Gérer les sites
                            </a>
                        </li>
                        @endpermission
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="navbar-btn btn-group">
                        <a href="{{ url('/') }}" target="_blank" class="btn btn-sm">
                            <span class="fa fa-external-link "></span>
                            <span class="ml5 hidden-xs">
                                Voir le site
                            </span>
                        </a>
                    </div>
                </li>
                <li class="dropdown menu-merge">
                    <?php $notifications = $topaz->getNotifications(); ?>
                    <div class="navbar-btn btn-group">
                        <button data-toggle="dropdown" class="btn btn-sm dropdown-toggle" aria-expanded="false">
                            <span class="fa fa-bell-o fs14 va-m"></span>
                            @if ($notifications->unread_notifications)
                                <span class="badge badge-danger">{{ $notifications->unread_notifications }}</span>
                            @endif
                        </button>
                        <div class="dropdown-menu dropdown-persist w450 animated animated-shorter fadeIn" role="menu">
                            <div class="panel mbn">
                                <div class="panel-menu">
                                    <span class="panel-icon"><i class="fa fa-clock-o"></i></span>
                                    <span class="panel-title fw600"> Activité récente</span>
                                </div>
                                <div class="panel-body panel-scroller scroller-navbar scroller-overlay scroller-pn pn scroller scroller-active"><div class="scroller-bar" style="height: 249px;"><div class="scroller-track" style="height: 249px; margin-bottom: 0px; margin-top: 0px;"><div class="scroller-handle" style="height: 123.2624254473161px; top: 0px;"></div></div></div><div class="scroller-content">
                                        <ol class="timeline-list">
                                            @forelse ($notifications->other_notifications as $notification)
                                                <li class="timeline-item {{ !$notification->read ?: 'bg-light' }}">
                                                    <div class="timeline-icon bg-{{ $topaz->getNotificationColor($notification) }} light">
                                                        <span class="fa fa-{{ $topaz->getNotificationIcon($notification) }}"></span>
                                                    </div>
                                                    <div class="timeline-desc">
                                                        @if (!$notification->read)
                                                            <span class="text-danger"><i class="fa fa-exclamation-circle"></i></span>
                                                        @endif
                                                        <a href="{{ route('admin.notification', $notification->id) }}">
                                                            {!! $notification->text !!}
                                                        </a>
                                                    </div>
                                                    <div class="timeline-date">{{ $topaz->diffNow($notification->created_at) }}</div>
                                                </li>
                                            @empty
                                                <li class="">
                                                    <div class="lead text-center mt20">
                                                        Aucune notification
                                                    </div>
                                                </li>
                                            @endforelse
                                        </ol>

                                    </div></div>
                                {{--<div class="panel-footer text-center p7">--}}
                                    {{--<a href="#" class="link-unstyled"> View All </a>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </li>
                <li class="dropdown menu-merge">
                    <a href="#" class="dropdown-toggle fw600" data-toggle="dropdown">
                        <span class="hidden-xs">
                            {{ $admin_user->displayname }}
                        </span>
                        <span class="visible-xs">
                            <i class="fa fa-user"></i> <i class="fa fa-caret-down"></i>
                        </span>
                        <span class="caret caret-tp hidden-xs"></span>
                    </a>
                    <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
                        <li class="list-group-item">
                            <a href="#" class="animated animated-short fadeInUp">
                                <span class="fa fa-user"></span> Mon Profil
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('admin.auth.logout') }}" class="animated animated-short fadeInUp">
                                <span class="text-danger">
                                    <i class="fa fa-power-off pr5"></i> Se déconnecter
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li id="toggle_sidemenu_t">
                    <span class="fa fa-caret-up"></span>
                </li>
            </ul>
        </header>
        <aside id="sidebar_left" class="sidebar-default affix">
            <!-- Start: Sidebar Left Content -->
            <div class="sidebar-left-content nano-content ">
                <!-- Start: Sidebar Menu -->
                <ul class="nav sidebar-menu">
                    @include('topaz::navbar_item', [
                        'icon' => 'home',
                        'title' => 'Accueil',
                        'route' => 'admin.index',
                        'active' => '',
                        'icontype' => 'glyphicon'
                    ])
                    @foreach ($topaz->getAdminMenuItems() as $item)
                        @include('topaz::navbar_item', $item)
                    @endforeach
                </ul>
            </div>
        </aside>
        <section id="content_wrapper">
            <header id="topbar" class="affix">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="crumb-active">
                            <a href="{{ URL::full() }}">@yield('page_title', '<span class="text-danger">Titre manquant !!</span>')</a>
                        </li>
                        <li class="crumb-icon">
                            <a href="{{ route('admin.index') }}">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        @yield('breadcrumb')
                        <li class="crumb-trail">@yield('breadcrumb_tail')</li>
                    </ol>
                </div>
                <div class="topbar-right">
                    @yield('topbar_right')
                </div>
            </header>
            <section id="content" class="">
                <div class="container">
                    @include('topaz::flashbag')
                    @yield('body')
                </div>

                <div class="text-center">
                    <hr>
                    &copy; {{ topaz_author() }} 2015
                </div>
            </section>
        </section>
    </div>
@stop

@section('skeleton_javascripts')
@endsection