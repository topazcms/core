<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaintenanceToSettings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('topaz_settings', function(Blueprint $table)
		{
			$table->boolean('maintenance')->default(false);
			$table->text('maintenance_message')->nullable();
			$table->boolean('maintenance_allow_admin')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('topaz_settings', function(Blueprint $table)
        {
            $table->dropColumn('maintenance');
            $table->dropColumn('maintenance_message');
            $table->dropColumn('maintenance_allow_admin');
        });
	}

}
