<?php

namespace Topaz\Core\Commands;

use Illuminate\Console\Command;
use Topaz\Core\Models\User;

class TopazGenerateSuperadmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'topaz:generate-superadmin {--username=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates superadmin user for Topaz Admin.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = 'superadmin';
        if ($this->option('username') !== null) {
            $username = $this->option('username');
        }
        if (User::where('username', $username)->count() > 0) {
            $this->error("Le superadmin existe deja !");
            return;
        }

        $email = $this->ask('Adresse e-mail', 'contact@irisdev.net');
        $password = $this->secret('Mot de passe');

        $user = new User([
            'firstname' => 'SuperAdmin',
            'lastname' => 'Topaz',
            'username' => $username,
            'password' => $password,
            'email' => $email,
        ]);

        $user->attachRole(app('defender')->findRole('superadmin'));

        $user->save();

        $this->info("Le compte a bien été créé !");
        $this->comment("Identifiant : $username");
        $this->comment("Mot de passe : " . str_repeat('*', strlen($password)));
    }
}
