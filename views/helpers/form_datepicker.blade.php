<script>
    $(document).ready(function() {
       $('#{{ $id }}').datetimepicker({
           format: '{{ $format or 'L' }}',
           locale: 'fr'
       });
    });
</script>