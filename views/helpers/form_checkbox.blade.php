<script>
    $(document).ready(function() {

        function refresh_button_{{ $id }}()
        {
            var input = $('#{{ $id }}');
            var button = $('#{{ $id }}_button');

            if (input.val() == 0) {
                button.removeClass('btn-{{ $color or 'primary' }} active')
                        .addClass('btn-default');
                button.find('i').removeClass('fa-check-square-o').addClass('fa-square-o');
            } else {
                button.addClass('btn-{{ $color or 'primary' }} active')
                        .removeClass('btn-default');
                button.find('i').addClass('fa-check-square-o').removeClass('fa-square-o');
            }
        }

        $('#{{ $id }}_button').on('click', function() {
            var input = $('#{{ $id }}');

            if (input.val() == 0) {
                input.val(1);
            } else {
                input.val(0);
            }

            refresh_button_{{ $id }}();
        });

        refresh_button_{{ $id }}();

    });
</script>