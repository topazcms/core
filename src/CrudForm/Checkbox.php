<?php


namespace Topaz\Core\CrudForm;


class Checkbox extends Field
{
    public function __construct($name, $label, $help = null, $color = 'primary')
    {
        parent::__construct($name, $label, $help);

        $this->callBeforeJs('form_checkbox', [$name, [
            'color' => $color
        ]]);
    }

}