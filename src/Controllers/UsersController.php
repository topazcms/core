<?php namespace Topaz\Core\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Topaz\Core\CrudForm\CKEditor;
use Topaz\Core\CrudForm\CrudForm;
use Topaz\Core\CrudForm\Datepicker;
use Topaz\Core\CrudForm\Image;
use Topaz\Core\CrudForm\Select2;
use Topaz\Core\CrudForm\Text;
use Topaz\Core\CrudForm\Checkbox;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\Settings;
use Topaz\Core\Models\Site;
use Topaz\Core\Models\User;
use Topaz\Core\Services\SitesManager;

class UsersController extends TopazCrudController {

    protected $modelName = User::class;
    protected $route_prefix = 'admin.settings.users';

    protected $object_name_singular = 'compte utilisateur';
    protected $object_name_plural = 'comptes utilisateurs';
    protected $object_name_male = true;

    protected $color = 'info';

    protected $index_table = [
        'Identifiant' => 'username',
        'Nom' => 'display_name',
        'Adresse e-mail' => 'email',
        'Création' => 'dates_column',
    ];

    protected $index_sorting = [
        'Identifiant' => 'username',
        'Nom' => 'firstname',
        'Adresse e-mail' => 'email',
        'Création' => 'created_at',
    ];

    protected $validation_rules = [
        'username' => 'required|max:255',
        'firstname' => 'required|max:255',
        'lastname' => 'required|max:255',
        'email' => 'required|email|max:255',
        'password' => 'required|max:255|min:8',
    ];

    protected $orderBy = ['created_at', 'asc'];
    protected $objectsPerPage = 20;

    public static function routes()
    {
        parent::crud_routes('settings/users', 'settings.users.', ['middleware' => 'permission:settings.users']);
    }

    protected static function custom_routes()
    {
        //Route::get('default/{site_id}', ['as' => 'default', 'uses' => get_called_class().'@setDefault']);
        //Route::get('enable/{site_id}', ['as' => 'enable', 'uses' => get_called_class().'@enable']);
        //Route::get('disable/{site_id}', ['as' => 'disable', 'uses' => get_called_class().'@disable']);
    }

    public function indexQuery($query)
    {
        return $query;
    }


    public function searchQuery($query, $term)
    {
        $like_term = "%$term%";
        return $query->where(function ($q) use ($like_term) {
            $q->orWhere('username', 'LIKE', $like_term);
            $q->orWhere('firstname', 'LIKE', $like_term);
            $q->orWhere('lastname', 'LIKE', $like_term);
            $q->orWhere('email', 'LIKE', $like_term);
        });
    }

    /**
     * @param $object
     * @return CrudForm
     */
    public function getForm($object, $add)
    {
        $form = new CrudForm;

        $form->appendMain(with(new Text('username', "Identifiant"))->big());
        $form->appendMain(with(new Text('lastname', "Nom")));
        $form->appendMain(with(new Text('firstname', "Prénom")));
        $form->appendMain(with(new Text('email', "Adresse e-mail")));
        $form->appendMain(with(new Text('password', "Mot de Passe"))->password());

        return $form;
    }

}
