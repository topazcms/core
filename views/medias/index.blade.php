@extends('topaz::app')

@section('title', 'Bibliothèque')
@section('page_title', 'Bibliothèque')

@section('breadcrumb_tail', 'Bibliothèque')

@inject('topaz', 'topaz')

@section('body')
    <div class="row mb20">
        <div class="col-md-6">
            <div class="btn-group">
                <a class="btn btn-primary text-bold openModal" href="#">
                    <i class="fa fa-upload"></i> Télécharger un nouveau fichier
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        @forelse($medias as $media)
            <div class="col-md-3 mb15">
                <a href="#" class="btn btn-default light btn-block text-left p20" style="overflow: hidden">
                    {{--<div class="row">--}}
                        {{--<div class="col-xs-2 text-center">--}}
                            {{--<i class="fa fa-{{ $media->type_icon }} fa-3x"></i>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-10">--}}
                            {{--<b>{{ $media->name }}</b>--}}
                            {{--<br>--}}
                            {{--{{ $media->type_title }}--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <table style="width:100%">
                        <tr>
                            <td style="width:45px;">
                                <i class="fa fa-{{ $media->type_icon }} fa-3x"></i>
                            </td>
                            <td class="pl5">
                                <b>{{ $media->name }}</b>
                                <br>
                                <span class="label label-primary">{{ $media->type_title }}</span>
                                <span class="label label-default">{{ $media->size }}</span>
                            </td>
                        </tr>
                    </table>
                </a>
            </div>
        @empty

        @endforelse
    </div>

    <div class="text-center">
        {!! $medias->render() !!}
    </div>

    <!-- Admin Form Popup -->
            <div id="modal" class="popup-basic horizontal-form mfp-with-anim mfp-hide" style="max-width: 700px">
              <div class="panel">
                <div class="panel-heading pb0">
                  <span class="panel-title">
                    <i class="fa fa-upload"></i> Télécharger un nouveau fichier
                  </span>
                </div>
                <!-- end .panel-heading section -->

                  <div class="panel-body p25">

                  <form action="{{ route('admin.medias.upload') }}" class="dropzone" id="upload-zone">
                    <input name="_token" value="{{ csrf_token() }}" type="hidden"/>
                  </form>

                      <div id="progression" class="mt35" style="display:none">
                          <div class="progress">
                              <div class="progress-bar progress-bar-success" id="progress_bar" style="width: 60%;">60%</div>
                          </div>
                      </div>

                    {{--<div class="form-group">--}}
                        {{--<label for="route"><i class="fa fa-code-fork ml5 mr5"></i> Route</label>--}}
                        {{--<div class="bs-component">--}}
                            {{--<input id="route" name="route" placeholder="Route" class="form-control" type="text" value="{{ Session::get('_old_input.route', '') }}">--}}
                        {{--</div>--}}
                    {{--</div>--}}

                  </div>
                  <!-- end .form-body section -->

                  {{--<div class="panel-footer">--}}
                      {{--<button type="submit" class="btn btn-primary" id="upload_close_btn">--}}
                          {{--<i class="fa fa-check-circle"></i> Terminé--}}
                      {{--</button>--}}
                  {{--</div>--}}
                  <!-- end .form-footer section -->
              </div>
              <!-- end: .panel -->
            </div>
            <!-- end: .admin-form -->
@stop

@section('stylesheets')
    @include('topaz::form_css')
    <link rel="stylesheet" href="{{ asset('topaz/admin_assets/vendor/plugins/magnific/magnific-popup.css') }}"/>
    <link rel="stylesheet" href="{{ asset('topaz/admin_assets/vendor/plugins/dropzone/css/dropzone.css') }}"/>
@endsection

@section('javascripts')
    @include('topaz::form_js')
    <script src="{{ asset('topaz/admin_assets/vendor/plugins/magnific/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('topaz/admin_assets/vendor/plugins/dropzone/dropzone.min.js') }}"></script>
    <script>

        Dropzone.options.uploadZone = {
            dictDefaultMessage: "Déplacez les fichiers ici pour les télécharger",
            dictFallbackMessage: "Votre navigateur ne supporte pas le téléchargement de fichiers par glisser/déposer.",
            dictFallbackText: "Veuillez utiliser le formulaire ci-dessous pour télécharger vos fichiers :",
            init: function() {
                this.on('sending', function(file) {
                    $('.mfp-close').hide();
                    $('#progress_bar').css('width', '0%').text('0%');
                });
                this.on('complete', function(file) {
                    $('.mfp-close').show();
                });
                this.on('sendingmultiple', function(file) {
                    $('.mfp-close').hide();
                    $('#progress_bar').css('width', pc+'%').text(pc+'%');
                });
                this.on('completemultiple', function(file) {
                    $('.mfp-close').show();
                });
                this.on('totaluploadprogress', function(uploadProgress) {
                    $('#progression').slideDown();
                    var pc = Math.round(uploadProgress);
                    $('#progress_bar').css('width', pc+'%').text(pc+'%');
                });
            }
        };



        $(document).ready(function() {

            $(document).on('click', '.mfp-close', function() {
                window.location.reload(true);
            });


            var openModal = $('.openModal');
            openModal.attr('data-effect', "mfp-zoomOut");

            openModal.on('click', function() {
                var btn = $(this);

                 $.magnificPopup.open({
                    removalDelay: 500, //delay removal by X to allow out-animation,
                    items: {
                        src: '#modal'
                    },
                     closeOnBgClick: false,
                    // overflowY: 'hidden', //
                    callbacks: {
                        beforeOpen: function(e) {
                            var Animation = btn.attr('data-effect');
                            this.st.mainClass = Animation;
                        }
                    },
                    midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
                });
            });

        });
    </script>
@endsection