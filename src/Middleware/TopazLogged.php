<?php

namespace Topaz\Core\Middleware;

use Closure;
use Illuminate\Auth\Guard;

class TopazLogged
{
    protected $auth;

    public function __construct()
    {
        $this->auth = app('auth')->admin();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest())
        {
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                return redirect()->route('admin.auth.login');
            }
        }

        return $next($request);
    }
}
