<?php

namespace Topaz\Core\Middleware;

use Closure;
use Illuminate\Auth\Guard;
use Topaz\Core\Services\SitesManager;

class TopazInitSites
{
    /** @var  SitesManager */
    protected $sites;

    public function __construct()
    {
        $this->sites = app('topaz.sites');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->sites->defineId();
        return $next($request);
    }
}
