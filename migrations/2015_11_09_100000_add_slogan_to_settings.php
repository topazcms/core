<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSloganToSettings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('topaz_settings', function(Blueprint $table)
		{
			$table->string('site_slogan')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('topaz_settings', function(Blueprint $table)
        {
            $table->dropColumn('site_slogan');
        });
	}

}
