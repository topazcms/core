<?php namespace Topaz\Core\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\User;

class OldPagesController extends Controller {

    public static function routes()
    {
        Route::pattern('page_id', '[0-9]+');

        Route::get('/', ['as' => 'index', 'uses' => __CLASS__.'@index']);

        Route::get('add', ['as' => 'add', 'uses' => __CLASS__.'@add']);
        Route::post('add', __CLASS__.'@create');

        Route::get('edit/{page_id}', ['as' => 'edit', 'uses' => __CLASS__.'@edit']);
        Route::post('edit/{page_id}', __CLASS__.'@update');

        Route::get('delete/{page_id}', ['as' => 'delete', 'uses' => __CLASS__.'@delete']);
        Route::get('restore/{page_id}', ['as' => 'restore', 'uses' => __CLASS__.'@restore']);
    }

    public function index()
    {
        $pages = Page::userRestricted()->with('author');
        $view = 'index';

        if ($this->request->has('trashed')) {
            $pages = $pages->onlyTrashed();
            $view = 'trash';
        }

        $pages = $pages->orderBy('updated_at', 'desc');
        $pages = $pages->paginate(20);

        return view('topaz::pages.' . $view, compact('pages'));
    }

    public function add()
    {
        $page = new Page;
        $page->author_id = $this->user->id;
        $authors = User::authors();
        return view('topaz::pages.form', compact('page', 'authors'));
    }

    public function create()
    {
        $this->validate($this->request, [
            'title' => 'required',
            'author_id' => 'required|exists:topaz_users,id',
        ]);

        $page = new Page($this->request->all());
        $page->save();

        $this->request->session()->flash('success', "La page <b>{$page}</b> a bien été créée");
        return redirect()->route('admin.pages.edit', $page);
    }

    public function edit($page_id)
    {
        $page = Page::findOrFail($page_id);
        $authors = User::authors();
        return view('topaz::pages.form', compact('page', 'authors'));
    }

    public function update($page_id)
    {
        $this->validate($this->request, [
            'title' => 'required',
            'author_id' => 'required|exists:topaz_users,id',
        ]);

        $page = Page::findOrFail($page_id);
        $page->fill($this->request->all());
        $page->save();

        $this->request->session()->flash('success', "La page <b>{$page}</b> a bien été modifiée");
        return redirect()->route('admin.pages.edit', $page);
    }

    public function delete($page_id)
    {
        $page = Page::whereId($page_id)->withTrashed()->firstOrFail();

        $message = "La page <b>$page</b> a bien été mise à la corbeille.";

        if ($page->trashed()) {
            $page->clearRoutes();
            $page->forceDelete();
            $message = "La page <b>$page</b> a bien été définitivement supprimée.";
        } else {
            $page->disableRoutes();
            $page->delete();
        }

        $this->request->session()->flash('success', $message);
        return redirect()->route('admin.pages.index');
    }

    public function restore($page_id)
    {
        $page = Page::whereId($page_id)->withTrashed()->firstOrFail();

        if (!$page->trashed()) {
            $this->request->session()->flash('warning', "La page <b>$page</b> est déjà active. Vous ne pouvez pas la restaurer.");
            return redirect()->back();
        }

        $page->restore();
        $page->enableRoutes();

        $this->request->session()->flash('success', "La page <b>$page</b> a bien été restaurée.");
        return redirect()->route('admin.pages.index');
    }

}
