<?php

namespace Topaz\Core;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Topaz\Core\Commands\TopazGenerateSuperadmin;
use Topaz\Core\Middleware\TopazLogged;
use Topaz\Core\Middleware\TopazMinify;
use Topaz\Core\Models\Menu;
use Topaz\Core\Models\Page;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class TopazCatchallServiceProvider extends ServiceProvider
{
    use SEOToolsTrait;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!$this->app->routesAreCached()) {
            require __DIR__ . '/routes_after.php';
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
