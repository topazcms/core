<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRobotsToSettings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('topaz_settings', function(Blueprint $table)
		{
			$table->string('robots_index')->default('index');
			$table->string('robots_follow')->default('follow');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('topaz_settings', function(Blueprint $table)
        {
            $table->dropColumn('robots_index');
            $table->dropColumn('robots_follow');
        });
	}

}
